<?php


namespace app\commands;

use yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\LastPublPosts;
use app\models\SubscribeLists;
use yii\helpers\Url;

class PublController extends Controller
{
	const CONFIRM_SUBSRIBE_EMAIL = 1;
	/**
	 * Консольная команда для получения последних публикаций и запись в БД
	 * @return array|false|int
	 * @throws yii\db\Exception
	 */
	public function actionIndex()
	{
		$lastPosts = Yii::$app->db->createCommand('
			SELECT c.`id`, c.`editedon`, c.`publishedon`, c.content, 
			longtitle, REPLACE(c.uri, ".html", "") `uri`
			FROM `site_content` c
			LEFT JOIN last_publ_posts l ON c.editedon = l.publishedon
			WHERE c.`published` = 1 AND c.`deleted` = 0
			AND c.`editedon` = c.`publishedon` AND c.`menutitle` <> ""
			AND (c.`publishedon` > l.`publishedon` OR l.content_id IS NULL)
			ORDER BY c.`publishedon` DESC
		')->queryAll();

		if (empty($lastPosts)) {
			return false;
		}

		foreach ($lastPosts as $lastPost) {
			$content = preg_replace('/\[\[\$form(\?*)(.*)\]\]/uxi', '', $lastPost['content']);
			preg_match('/<img(.*)>/uxi', $content, $match);
			preg_match('/src="([^"]+)"/uxi', $match[0], $matchImage);
			$image = $matchImage[1];
//			var_dump($matchImage);die();
			$textContent = substr($content, 0, strpos($content, $match[0]));
			$publ = LastPublPosts::findOne(['id' => $lastPost['id']]);
			if (empty($publ)) {
				$publ = new LastPublPosts();
			}

			$publ->content_id = $lastPost['id'];
			$publ->publishedon = $lastPost['editedon'];
			$publ->title = $lastPost['longtitle'];
			$publ->images = $image;
			$publ->content = $textContent;
			$publ->uri = $lastPost['uri'];
			if (!$publ->save()) {
				return $publ->errors;
			}

			echo 'RESULT: ' . $publ->id ."\n";
		}

		return ExitCode::OK;
	}

	/**
	 * Получение последних статей больше дня отправки на 1 день и
	 * отправка этих статей подписавшимся
	 * @return false
	 */
	public function actionSendPost()
	{
		$emails = SubscribeLists::find()->select([
			'email'
		])->where([
			'confirm' => self::CONFIRM_SUBSRIBE_EMAIL
		])->column();

		if (empty($emails)) {
			echo "NOT SUBSCRIBE EMAIL \n";
			return false;
		}

		$lastPosts = LastPublPosts::getLastPublPostByYesterday(10, 0);
		if (empty($lastPosts)) {
			echo "NOT LAST PUBLICATION POSTS \n";
			return false;
		}

		foreach ($emails as $email) {
			$mailer = Yii::$app->mailer->compose('last-publi-post-send', [
				'lastPosts' => $lastPosts,
				'linkMainUrl' => 'https://alandalus.ru',
			])->setTo($email)->setFrom([
				Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']
			]);
			$mailer->setReplyTo([Yii::$app->params['adminEmail']]);
			$mailer->setSubject('Последние публикации');
//			$mailer->setHtmlBody('$lastPosts');
			$mailer->send();
			echo "SEND LAST PUBLICATION POSTS \n";
		}

	}

}