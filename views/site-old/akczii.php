<?php

use yii\helpers\Url;
use app\assets\AppAsset;


$this->registerCssFile('css/style-slide.css', ['depends' => AppAsset::className()]);
//$this->registerCssFile('css/elastislide.css', ['depends' => AppAsset::className()]);
$images_arr = [
	'5325' => '/assets/images/akcii/banner excurcion.png',
	'5324' => '/assets/images/akcii/banner rental_150.png',
	'5323' => '/assets/images/akcii/banner bus_150.png',
	'5319' => '/assets/images/akcii/banner boat.png',
	'5317' => '/assets/images/akcii/banner car_150.png',
];
$this->registerCss(<<<CSS
	.da-slide-current h2, .da-slide-current p, .da-slide-current .da-link {
		left: 0;
		opacity: 1;
		width: 100%;
		right: 0;
		text-align: center;
	}
	.da-slide h2 {
		top: 0;
		white-space: unset;
	}
	.da-slider {
		min-width: 100%;
	}
	.da-slide h2, .da-slide p, .da-slide .da-link, .da-slide .da-img {
		position: unset;	
	}
	.da-slide p {
		height: auto;
		top: 0;
	}
	.da-slide .da-img {
		/*text-align: center;*/
		width: 100%;
		top: 0;
		height: auto;
    	line-height: 0;
		/*left: 110%;*/
	}/**/
	@media (max-width: 425px) {
		.da-slider {
			height: 665px;
		}
	}

CSS
);
//var_dump($akczii);

?>

<?php if(!empty($akczii)) : ?>
<div class="row">
	<div id="da-slider" class="da-slider">
		<?php foreach ($akczii as $item) : ?>
		<div class="da-slide">
			<div class="col-lg-12">
				<h2><?=$item->pagetitle;?></h2>
			</div>
			<div class="col-lg-12">
				<div class="col-sm-6">
					<p><?=$item->description;?></p>
					<a href="<?=Url::to([$item->uri]);?>" class="da-link button button-primary">Подробнее</a>
				</div>
				<div class="col-sm-6">
					<div class="da-img"><?php if (!empty($images_arr[$item->id])) : ?><img src="<?=$images_arr[$item->id]?>" alt="image01" /><?php endif; ?></div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		<nav class="da-arrows">
			<span class="da-arrows-prev"></span>
			<span class="da-arrows-next"></span>
		</nav>
	</div>
</div>
<?php endif; ?>


<?php
$this->registerJsFile('/js/modernizr.custom.28468.js', ['depends' => \yii\web\YiiAsset::className()]);
$this->registerJsFile('/js/jquery.cslider.js', ['depends' => \yii\web\YiiAsset::className()]);
$this->registerJs(<<<JS
	$(function() {
		$('#da-slider').cslider({
			current		: 0, 	
			bgincrement	: 50,	
			autoplay	: true,
			interval	: 4000  
		});
	});
JS
);
?>
