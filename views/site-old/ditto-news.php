<?php

use yii\helpers\Url;

?>

<?php if(!empty($dittonews)) : ?>
	<?php foreach ($dittonews as $item) : ?>
		<div class="post-box post-box-wide d-block text-left">
			<div class="post-box-caption-custom">
				<div class="post-box-title h5 text-ubold"><a class="text-black" href="<?=Url::to([$item->uri])?>"><?=$item->pagetitle?></a></div>
				<p class="text-small text-silver-chalice"><?=$item->introtext?></p>
				<a class="button button-primary button-width-110" href="<?=Url::to([$item->uri])?>">Читать дальше...</a>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>