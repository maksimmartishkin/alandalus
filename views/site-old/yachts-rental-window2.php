<?php

use app\models\YachtRentalForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;
//use softark\mbcaptcha\Captcha;

$model = new YachtRentalForm();
if ($model->load(Yii::$app->request->post()) && $model->retail(Yii::$app->params['adminEmail'])) {
	Yii::$app->session->setFlash('contactFormSubmitted');

	return Yii::$app->getResponse()->refresh();
}

$this->title = 'Аренда яхт';

?>
<section class="section parallax-container bg-black section-height-mac" data-parallax-img="<?php /*/images/background/p.jpg*/?>">
	<div class="parallax-content">
		<div class="bg-overlay-darker">
			<div class="container section-34 section-md-60 section-lg-115">
				<h1 class="d-none d-lg-inline-block text-white"><?=$this->title?></h1>
			</div>
		</div>
	</div>
</section>

<section class="section-80 bg-wild-wand text-md-left">
	<div class="container">
		<div class="row justify-content-sm-center row-50">
			<div class="col-lg-12">

				<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

					<div class="alert alert-success">
						Ваша заявка отправлена. В ближайшее время с Вами свяжутся.
					</div>

				<?php else: ?>
					<div class="offset-sm-1 offset-md-2 offset-lg-3">
						<div class="col-md-8 col-lg-4 col-xl-8 form-question-block">
							<h5 class="text-ubold text-center">Форма обратной связи для аренды яхт</h5>
							<?php $form = ActiveForm::begin([
								'id' => 'yacht-rental-form',
								'enableClientValidation' => true,
								'options' => [
									//					'class' => 'rd-mailform text-left',
									//				'data-form-output' => 'form-output-global',
									//					'data-form-type' => 'contact'
								],
							]); ?>

							<input name="formid" type="hidden" value="contact-form">
							<input type="hidden" name="site_url" value="http://alandalus.ru/">
							<input type="hidden" name="page_url" value="yachts-rental-window2.html">
							<input type="hidden" name="page_name" value="Yachts rental window2">

							<?= $form->field($model, 'name', [
								'template' => '
									<div class="form-wrap form-wrap-xs">
										{label}
										{input}
										<span class="form-validation">{error}</span>
									</div>
								'
							])->textInput(['class' => 'form-input'])
								->label($model->attributeLabels()['name'], ['class' => 'form-label']); ?>

							<?= $form->field($model, 'phone', [
								'template' => '
									<div class="form-wrap form-wrap-xs">
										{label}
										{input}
										<span class="form-validation">{error}</span>
									</div>
								'
							])->textInput(['class' => 'form-input'])
								->label($model->attributeLabels()['phone'], ['class' => 'form-label']); ?>

							<?= $form->field($model, 'email', [
								'options' => [
									'class' => 'form-wrap form-wrap-xs'
								],
								'template' => '
									<div class="form-wrap form-wrap-xs">
										{label}
										{input}
										<span class="form-validation">{error}</span>
									</div>
								'
							])->input('email', ['class' => 'form-input'])
								->label($model->attributeLabels()['email'], ['class' => 'form-label']); ?>

							<!--				--><?//= $form->field($model, 'subject')->hiddenInput(['value' => 'form-input']); ?>

							<?= $form->field($model, 'count_guest', [
								'template' => '
									<div class="form-wrap form-wrap-xs">
										{label}
										{input}
										<span class="form-validation">{error}</span>
									</div>
								'
							])->input('number', ['class' => 'form-input', 'min' => 1])
								->label($model->attributeLabels()['count_guest'], ['class' => 'form-label']); ?>

							<?= $form->field($model, 'date', [
								'template' => '
									<div class="form-wrap form-wrap-xs">
										{label}
										{input}
										<span class="form-validation">{error}</span>
									</div>
								'
							])->input('date', ['class' => 'form-input'])
								->label(false); ?>

							<?= $form->field($model, 'body', [
								'template' => '
									<div class="form-wrap form-wrap-xs">
										{label}
										{input}
										<span class="form-validation">{error}</span>
									</div>
								'
							])->textarea(['rows' => 6, 'class' => 'form-input', 'style' => 'height:120px;'])
								->label($model->attributeLabels()['body'], ['class' => 'form-label']); ?>

<!--							--><?//= $form->field($model, 'verifyCode', [
//								'options' => [
//									'class' => 'form-wrap form-wrap-xs js-captcha-form'
//								],
//								'template' => '
//									<div class="form-wrap form-wrap-xs">
//										{label}
//										{input}
//										<span class="form-validation">{error}</span>
//									</div>
//								'
//							])->widget(Captcha::className(), [
//								'template' => '
//									<div class="form-wrap form-wrap-xs">
//										{image}
//										{input}
//									</div>
//								',
//								'captchaAction' => '/site/captcha',
//							])->label(false); ?>




							<?= $form->field($model, 'reCaptcha')->widget(
								\himiklab\yii2\recaptcha\ReCaptcha::className(),
								[
										'siteKey' => RECAPTCHA_SITE_KEY_V2
								]
							)->label(false); ?>

							<div class="form-button text-center text-md-center">
								<?= Html::submitButton('Отправить', ['class' => 'button button-width-110 button-primary', 'name' => 'contact-button']) ?>
							</div>

							<?php ActiveForm::end(); ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>