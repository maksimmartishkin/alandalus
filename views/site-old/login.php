<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section parallax-container bg-black" data-parallax-img="images/backgrounds/background-26-1920x900.jpg">
	<div class="parallax-content">
		<div class="container section-80 section-md-top-135 section-md-bottom-145">
			<div class="row justify-content-sm-center">
				<div class="col-sm-9 col-md-7 col-lg-5 col-xl-4">
					<!-- Box-->
					<div class="box box-lg d-block bg-default inset-xl-left-60 inset-xl-right-60">
						<h5 class="text-ubold text-md-center"><?= Html::encode($this->title) ?></h5>
						<?php $form = ActiveForm::begin([
							'id' => 'login-form',
							'enableClientValidation' => true,
//							'layout' => 'horizontal',
							'options' => [
//								'class' => 'rd-mailform',
							],
//							'fieldConfig' => [
//								'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//								'labelOptions' => ['class' => 'col-lg-1 control-label'],
//							],
						]); ?>
						<?= $form->field($model, 'username', [
							'options' => [
								'class' => 'form-wrap form-wrap-xs'
							],
						])->textInput(['autofocus' => true, 'class' => 'form-input', 'data-constraints' => '@Required'])->label('Логин', ['class' => 'form-label']) ?>
<!--							<div class="form-wrap form-wrap-xs">-->
<!--								<label class="form-label" for="login-email">Your Email</label>-->
<!--								<input class="form-input" id="login-email" type="email" name="login-email" data-constraints="@Email @Required">-->
<!--							</div>-->
						<?= $form->field($model, 'password', [
							'options' => [
								'class' => 'form-wrap form-wrap-xs form-offset-bottom-none'
							],
						])->passwordInput(['class' => 'form-input', 'data-constraints' => '@Required'])
							->label('Пароль', ['class' => 'form-label'])?>
<!--							<div class="form-wrap form-wrap-xs form-offset-bottom-none">-->
<!--								<label class="form-label" for="login-password">Password</label>-->
<!--								<input class="form-input" id="login-password" type="password" name="login-password" data-constraints="@Required">-->
<!--							</div>-->
							<div class="form-wrap-checkbox d-sm-flex justify-content-between">
								<div class="pull-sm-left">
									<p class="text-extra-small"><a class="text-primary" href="forgot-password.html">Forgot Your Password?</a></p>
								</div>
								<div class="pull-sm-right form-wrap">
									<label class="checkbox-inline checkbox-inline-right">
										<input class="checkbox-custom" name="remember" value="checkbox-1" type="checkbox"><span class="text-extra-small text-black inset-right-10">Remember Me</span>
									</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-button">
								<?= Html::submitButton('<span>Войти</span><span class="icon icon-xxs mdi mdi-chevron-right" style="float:none; margin-top: -1px;"></span>', ['class' => 'button button-block button-icon button-icon-right button-primary', 'name' => 'login-button']) ?>
							</div>
						<?php ActiveForm::end(); ?>
						<p class="text-extra-small">Don`t have an account? <a class="text-primary" href="signup.html">Sign Up</a> now!</p>
<!--						<div class="section-hidden section-hidden-2">-->
<!--							<p class="divider-both-lines text-silver-chalice font-italic">or use your social account</p>-->
<!--						</div>-->
						<!-- List Inline-->
<!--						<ul class="list-inline list-inline-13">-->
<!--							<li class="text-center"><a class="icon icon-xxs icon-circle icon-circle-lg icon-filled-twitter fa fa-twitter text-white" href="#"></a></li>-->
<!--							<li class="text-center"><a class="icon icon-xxs icon-circle icon-circle-lg icon-filled-facebook fa fa-facebook text-white" href="#"></a></li>-->
<!--							<li class="text-center"><a class="icon icon-xxs icon-circle icon-circle-lg icon-filled-google-plus fa fa-google-plus text-white" href="#"></a></li>-->
<!--						</ul>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="site-login">
    <h1></h1>

    <p>Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>


        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

    <div class="col-lg-offset-1" style="color:#999;">
        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>
        To modify the username/password, please check out the code <code>app\models\User::$users</code>.
    </div>
</div>
