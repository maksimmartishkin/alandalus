<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Pages;

$action_link = explode('/', Url::to());
$active_link = Url::to();
//$action_link = array_pop($action_ar);
//$side_menu = Pages::find()->where(['LIKE', 'link', $action_link])->all();
//echo '<pre>';print_r($side_menu);die();

$this->registerCssFile('/plugins/tree/nogrid.css');
$id_cache = $active_link;
//if ($this->beginCache($id_cache, ['duration' => 3600])) :

?>

<aside class="blog-aside box box-xs d-block bg-default">
	<?php echo Yii::$app->controller->renderPartial("@app/views/site/search-for-site-form", ['text_request' => !empty($text_request)?$text_request:false]);?>
	<?php if(!empty($link) && ($link == 'nedvizimost-ispania')) : ?>
		<hr class="hr bg-gallery">
		<?php echo Yii::$app->controller->renderPartial("@app/views/site/bazasearch_h");?>
	<?php endif; ?>
	<hr class="hr bg-gallery">
	<div class="blog-aside-item">
		<?php if(!empty($link) && !empty($side_menu)) : ?>
			<div onclick="tree_toggle(arguments[0])" class="js-tree-side-menu">
				<ul class="Container">
					<?php foreach ($side_menu as $sideMenu) : ?>
						<?php
						$activeClassSideMenu = (!empty($sideMenu['link']) && $active_link == $sideMenu['link']) ? 'active' : '';
						$expandSideMenu = 'ExpandClosed';
						if (!empty($sideMenu['child']) && !empty($sideMenu['link']) && strpos($active_link, $sideMenu['link']) !== false) {
							$expandSideMenu = 'ExpandOpen';
						}
						?>
						<li class="Node IsRoot <?= $expandSideMenu ?> <?= $activeClassSideMenu ?>">
							<?php if (!empty($sideMenu['child'])) : ?>
								<div class="Expand"></div>
							<?php endif; ?>
							<div class="Content">
								<?= Html::a((!empty($sideMenu['title']) ? $sideMenu['title'] : ''), (!empty($sideMenu['link']) ? $sideMenu['link'] : '#')); ?>
								<?php if (!empty($sideMenu['child'])) : ?>
									<ul class="Container">
										<?php foreach ($sideMenu['child'] as $subMenu) : ?>
											<?php
											$activeClassSubMenu = (!empty($subMenu['link']) && $active_link == $subMenu['link']) ? 'active' : '';
											$expandSubMenu = 'ExpandClosed';
											if (!empty($subMenu['child']) && !empty($subMenu['link']) && strpos($active_link, $subMenu['link']) !== false) {
												$expandSubMenu = 'ExpandOpen';
											}
											?>
											<li class="Node IsRoot <?= $expandSubMenu ?> <?= $activeClassSubMenu ?>">
												<?php if (!empty($subMenu['child'])) : ?>
													<div class="Expand"></div>
												<?php endif; ?>
												<div class="Content">
													<?= Html::a((!empty($subMenu['title']) ? $subMenu['title'] : ''), (!empty($subMenu['link']) ? $subMenu['link'] : '#')); ?>
													<?php if (!empty($subMenu['child'])) : ?>
														<ul class="Container">
															<?php foreach ($subMenu['child'] as $subSubMenu) : ?>
																<?php
																$activeClassSubSubMenu = (!empty($subSubMenu['link']) && $active_link == $subSubMenu['link']) ? 'active' : '';
																$expandSubSubMenu = 'ExpandClosed';
																if (!empty($subSubMenu['child']) && !empty($subSubMenu['link']) && strpos($active_link, $subSubMenu['link']) !== false) {
																	$expandSubSubMenu = 'ExpandOpen';
																}
																?>
																<li class="Node IsRoot <?= $expandSubSubMenu ?> <?= $activeClassSubSubMenu ?>">
																	<?php if (!empty($subSubMenu['child'])) : ?>
																		<div class="Expand"></div>
																	<?php endif; ?>
																	<div class="Content">
																		<?= Html::a((!empty($subSubMenu['title']) ? $subSubMenu['title'] : ''), (!empty($subSubMenu['link']) ? $subSubMenu['link'] : '#')); ?>
																	</div>
																</li>
															<?php endforeach; ?>
														</ul>
													<?php endif; ?>
												</div>
											</li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
	</div>

</aside>
<?php
//	$this->endCache();
//endif;
$this->registerJsFile('/plugins/tree/tree_toggle.js',
	[
		'depends' => [\yii\web\JqueryAsset::className()]
	]
);
?>