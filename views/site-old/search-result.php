<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

//var_dump($query_result);die();
$this->title = 'Результаты поиска';
$this->params['breadcrumbs'][] = $this->title;

$menu_lists = [
	'search-result' => [
		'nedvizimost-ispania' => [
			'title' => 'Недвижимость',
			'link' => 'nedvizimost-ispania',
		],
		'otdyh-v-ispanii' => [
			'title' => 'Отдых',
			'link' => 'otdyh-v-ispanii',
		],
		'arenda-yaxt' => [
			'title' => 'Яхты',
			'link' => 'arenda-yaxt',
		],
		'arenda-avtomobilya-v-ispanii' => [
			'title' => 'Автомобили',
			'link' => 'arenda-avtomobilya-v-ispanii',
		],
		'akczii' => [
			'title' => 'Акции',
			'link' => 'akczii',
		],
		'andalucia' => [
			'title' => 'Об Андалусии',
			'link' => 'andalucia',
		],
		'poleznaya-informacziya' => [
			'title' => 'Информация',
			'link' => 'poleznaya-informacziya',
		],
		'kontaktyi' => [
			'title' => 'Контакты',
			'link' => 'kontaktyi',
		],
	],
];

?>

<!--<section class="section parallax-container bg-black section-height-mac context-dark" data-parallax-img="images/backgrounds/background-30-1920x900.jpg">-->
<!--	<div class="parallax-content">-->
<!--		<div class="bg-overlay-darker">-->
<!--			<div class="container section-34 section-md-100 section-lg-top-170 section-lg-bottom-165">-->
<!--				<h1 class="d-none d-lg-inline-block">Search Results</h1>-->
<!--				<h6 class="font-italic">Here are the most relevant search results according to your request</h6>-->
<!--				<div class="row row-sm justify-content-sm-center">-->
<!--					<div class="col-md-8 col-xl-6">-->
<!--						--><?php //$form = ActiveForm::begin([
//							'options' => [
////			'action' => 'search-query-for-site',
//								'class' => 'form-blog-search form-blog-search-type-2 form-search rd-search'
//							],
//						]); ?>
<!--						--><?//= Html::submitButton('<span class="fa fa-search"></span>', ['class' => 'form-search-submit']) ?>
<!--						--><?//= $form->field($model, 'text_request', [
//							'options' => [
//								'class' => 'form-wrap form-wrap-xs'
//							],
//						])->textInput(['autocomplete' => 'off', 'class' => 'form-search-input input-sm form-input input-sm'])
//							->label('Введите свой запрос', ['class' => 'form-label form-search-label form-label-sm']); ?>
<!--						--><?php //ActiveForm::end(); ?>
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->
<section class="section parallax-container bg-black section-height-mac" data-parallax-img="/images/background/background-23-1920x900.jpg">
	<div class="parallax-content">
		<div class="bg-overlay-darker">
			<div class="container section-34 section-md-60 section-lg-115">
				<h1 class="d-none d-lg-inline-block text-white"><?=$this->title?></h1>
			</div>
		</div>
	</div>
</section>

<section class="section-80 bg-wild-wand text-md-left">
	<div class="container">
		<div class="row justify-content-sm-center row-50">
			<div class="col-md-11 col-lg-9 order-lg-1">
				<!-- Box-->
				<div class="box box-lg box-single-post bg-default d-block">
					<h4 class="text-ubold"><?=$this->title?></h4>
					<div class="text-silver-chalice text-left">

						<?php if(!empty($query_result)) : ?>
							<?php foreach ($query_result as $key => $result) : ?>
								<?php
									$number_iterator = !empty($pages)?($pages->page * $pages->pageSize + $key + 1):$key + 1;
//									var_dump($number_iterator, $pages);
								?>
								<div class="post-box post-box-wide d-block text-left">
									<div class="post-box-caption">
										<div class="post-box-title h5 text-ubold"><?=$number_iterator?>. <a class="text-black" href="<?=Url::to([$result->uri])?>" title="<?=$result->pagetitle?>"><?=$result->pagetitle?></a></div>
										<p class="text-small text-silver-chalice"><?=mb_strimwidth(strip_tags($result->content), 0, 300, '...');?></p>
									</div>
								</div>
<!--								<div class="sisea-result">-->
<!--									<h3>--><?//=$key+1?><!--. <a href="--><?//=Url::to([$result->link])?><!--" title="--><?//=$result->title?><!--">--><?//=$result->title?><!--</a></h3>-->
<!--									<div class="extract">-->
<!--										--><?//=mb_strimwidth(strip_tags($result->content), 0, 300, '...');?>
<!--									</div>-->
<!--								</div>-->
								<!--					--><?php //var_dump($result); ?>
							<?php endforeach; ?>
						<?php else : ?>
							<p class="alert alert-warning" role="alert">
								По вашему запросу ничего не найдено. Попробуйте ввести похожие по смыслу слова, чтобы получить лучший результат.
							</p>
						<?php endif; ?>



					</div>
					<hr class="hr bg-alto">
					<?php
					if(!empty($pages)) {
						echo LinkPager::widget([
							'pagination' => $pages,
							'hideOnSinglePage' => true,
//							'prevPageLabel' => '<i class="news-pagination__faback fa fa-angle-double-left" aria-hidden="true"></i> Назад ',
//							'prevPageCssClass' => 'news-pagination__item back',
//							'nextPageLabel' => 'Вперед <i class="news-pagination__fanext news-pagination__fa fa fa-angle-double-right" aria-hidden="true"></i>',
//							'nextPageCssClass' => 'news-pagination__item next',
							'maxButtonCount'=>5,
//							'pageCssClass' => ['class' => 'news-pagination__item'],
							'options' => ['class' => 'pagination-classic'],
//							'linkOptions' => ['class' => 'news-pagination__link'],
						]);
					}
					?>



					<div class="d-inline-block inset-md-left-10">
						<!-- List Inline-->
						<ul class="list-inline list-inline-modern list-inline-11 bg-wild-wand">
							<li class="text-center"><span class="icon icon-sm icon-circle icon-filled-primary"><img class="img-responsive center-block" src="images/icons/icon-18-18x15.png" width="18" height="15" alt=""></span></li>
							<li class="text-center"><a class="icon fa fa-facebook-f text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-twitter text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-youtube text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-linkedin text-gray" href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-11 col-lg-3 text-lg-left">
				<?php echo Yii::$app->controller->renderPartial("/site/aside-menu-search-result", ['side_menu' => $side_menu, 'link' =>'search-result', 'text_request' => !empty($text_request)?$text_request:false]);?>
			</div>

		</div>
	</div>
</section>
<section class="section-80 bg-wild-wand">
	<div class="container">
		<div class="rd-search-results">


		</div>
	</div>
</section>
