<?php
use app\assets\AppAsset;

?>
<?php if(!empty($album_list)) : ?>
<?php $this->registerCss(<<<CSS
	.gv_galleryWrap { width: 100% !important; height: auto !important; }
	.gv_gallery { width: 100% !important; height: 440px !important; }
	.gv_panelWrap { width: 100% !important; height: 400px !important;}
		
	/* GALLERY PANELS */
	.gv_panel { width: 100% !important;  }
	.gv_panel img { width: 100% !important;  }
	.gv_filmstripWrap { width: 600px!important; }
	.gv_navWrap { z-index: 2; }
	
	@media (max-width: 575.98px) {
		.gv_galleryWrap {
			height: 42vh !important;
		}
		.gv_gallery {
			width: 100% !important;
			height: 40vh !important;
		}
		.gv_panel {
			width: 100vw !important;
			height: 35vh !important;
		}
		.gv_panelWrap {
			height: auto !important;
		}
		.gv_panel img {
			height: 33vh !important;
		}
	}
CSS
);
$this->registerCssFile('/plugins/gallery/css/jquery.galleryview-3.0-dev.css', ['depends' => AppAsset::className()]);
?>
<?php
	$id_cache = $album_list;
	if ($this->beginCache($id_cache, ['duration' => 86400])) :
?>
		<div class="js-gallery-view-722 gallery-view-722"></div>
		<!--		<ul id="myGallery">-->
		<!--			--><?php //foreach ($album_list as $key => $value) :
		?>
		<!--			<li><img src="/assets/images/--><?//=$value['filename']
		?><!--" alt="--><?//=$value['name']
		?><!--" />-->
		<!--				--><?php //endforeach;
		?>
		<!--		</ul>-->
		<?php
		$this->endCache();
	endif;
?>

<?php
//		$id_baza=$table_nedvizimost['id_baza']?$table_nedvizimost['id_baza']:false;
	if (!empty($album_list)) :
		$this->registerJs(<<<JS
	setTimeout(
    	$.ajax({
		url: "/images-gallery?album=$album_list",
		dataType: 'html',
		success: function(data){
		    // conso
			$('.js-gallery-view-722').html(data);
			$('#myGallery').galleryView({
				frame_opacity: 0.8,
			});
		}
	}), 1000);

JS
		);
	endif;
$this->registerJsFile('/plugins/gallery/js/jquery.timers-1.2.js', ['depends' => \yii\web\YiiAsset::className()]);
$this->registerJsFile('/plugins/gallery/js/jquery.galleryview-3.0-dev.js', ['depends' =>
	\yii\web\YiiAsset::className()]);
?>

<?php endif; ?>
