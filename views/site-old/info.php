<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Полезная информация';
$this->params['breadcrumbs'][] = $this->title;

?>


<section class="section parallax-container bg-black section-height-mac context-dark" data-parallax-img="images/backgrounds/background-23-1920x900.jpg">
	<div class="parallax-content">
		<div class="bg-overlay-darker">
			<div class="container section-34 section-md-100 section-lg-top-170 section-lg-bottom-165">
				<h1 class="d-none d-lg-inline-block"><?=$this->title?></h1>
				<h6 class="font-italic">TVOYA ISPANIA S.L.</h6>
			</div>
		</div>
	</div>
</section>

<!-- Our Blog-->
<section class="section-80 bg-wild-wand">
	<div class="container">
		<div class="row row-50 justify-content-sm-center">
			<div class="col-md-12 order-md-1 col-xl-5 text-md-left">
<!--				<h3>--><?//=$this->title?><!--</h3>-->
				<p class="text-offset-1">
					В данном разделе нашего сайта мы стараемся размещать информацию об Испании, которая может быть полезна как тем, кто собирается на курортный отдых, так и тем, кто намеревается приобрести недвижимость в этой прекрасной стране.

				</p>
				<?php /*<a class="button button-primary button-offset-1" href="tours-grid.html">view tours</a>*/?>
			</div>
			<div class="col-md-11 col-lg-9 order-lg-1">
				<?php /*<div class="row row-20 justify-content-sm-between">
					<div class="col-md-6 col-md-3 text-md-left">
						<div class="d-inline-block inset-md-left-20 inset-lg-left-0">
							<div class="pull-left inset-right-10">
								<p class="text-extra-small text-uppercase text-black">Sort By:</p>
							</div>
							<div class="pull-right shadow-drop-xs d-inline-block select-xs">
								<!--Select 2-->
								<select class="form-input select-filter" data-minimum-results-for-search="Infinity" data-constraints="@Required">
									<option value="2">Newest</option>
									<option value="3">Eldest</option>
								</select>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="col-md-6 col-md-3 text-md-right">
						<div class="d-inline-block inset-md-right-20 inset-lg-right-0">
							<div class="pull-left inset-right-10">
								<p class="text-extra-small text-uppercase text-black">View:</p>
							</div>
							<div class="pull-right">
								<!-- List Inline-->
								<ul class="list-inline list-primary-filled text-center list-top-panel">
									<li class="active"><a class="shadow-drop-lg" href="blog-list.html"><span class="icon icon-sm icon-square mdi mdi-view-stream"></span></a></li>
									<li><a class="shadow-drop-lg" href="blog-list-variant-2.html"><span class="icon icon-sm icon-square mdi mdi-format-list-bulleted"></span></a></li>
									<li><a class="shadow-drop-lg" href="blog-grid.html"><span class="icon icon-sm icon-square mdi mdi-view-module"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>*/?>
				<!-- Post Box-->
				<div class="post-box post-box-wide d-block text-left">
					<div class="post-box-img-wrap"><a href="blog-single-post-sidebar-left.html"><img src="images/blog/post-02-1170x440.jpg" width="1170" height="440" alt=""/></a></div>
					<div class="post-box-caption">
						<div class="post-box-title h5 text-ubold"><a class="text-black" href="blog-single-post-sidebar-left.html">Перевод с Испанского</a></div>
						<?php /*<ul class="list-inline post-box-meta list-inline-dashed list-inline-dashed-sm text-extra-small text-silver-chalice">
							<li class="text-uppercase"><img src="images/icons/icon-16-16x15.png" width="16" height="15" alt=""/><span class="text-middle inset-left-10">JUNE 20, 2019</span></li>
							<li class="text-bottom p text-uppercase"><img src="images/icons/icon-17-16x15.png" width="16" height="15" alt=""/><span class="inset-left-10">by <a href="testimonials.html">EMMA STONE</a></span></li>
						</ul>*/?>
						<p class="text-silver-chalice">Предлагаем письменный и устный перевод с Испанского на Русский и наоборот. К вашим услугам:
							- технический перевод и перевод документации;
							- перевод контрактов и подготовка документов для государственных служб Испании;
							- услуги переводчика в Испании;
							- сопровождение и работа переводчика на деловых встречах в Испании.</p><a class="button button-primary button-width-110" href="poleznaya-informacziya/perevod-s-ispanskogo.html">Читать дальше...</a>
					</div>
				</div>
				<!-- Post Box-->
				<div class="post-box post-box-wide d-block text-left">
					<div class="post-box-img-wrap"><a href="blog-single-post-sidebar-left.html"><img src="images/blog/post-02-1170x440.jpg" width="1170" height="440" alt=""/></a></div>
					<div class="post-box-caption">
						<div class="post-box-title h5 text-ubold"><a class="text-black" href="blog-single-post-sidebar-left.html">Загранпаспорт</a></div>
						<?php /*<ul class="list-inline post-box-meta list-inline-dashed list-inline-dashed-sm text-extra-small text-silver-chalice">
							<li class="text-uppercase"><img src="images/icons/icon-16-16x15.png" width="16" height="15" alt=""/><span class="text-middle inset-left-10">JUNE 20, 2019</span></li>
							<li class="text-bottom p text-uppercase"><img src="images/icons/icon-17-16x15.png" width="16" height="15" alt=""/><span class="inset-left-10">by <a href="testimonials.html">EMMA STONE</a></span></li>
						</ul>*/?>
						<p class="text-silver-chalice">Процедура обмена и получения загранпаспортов в Испании для Россиян. Как обменять и получить загранпаспорт не выезжая из Испании.</p><a class="button button-primary button-width-110" href="blog-single-post-sidebar-left.html">Читать дальше...</a>
					</div>
				</div>
				<!-- Post Box-->
				<div class="post-box post-box-wide d-block text-left">
					<div class="post-box-img-wrap"><a href="blog-single-post-sidebar-left.html"><img src="images/blog/post-03-1170x440.jpg" width="1170" height="440" alt=""/></a></div>
					<div class="post-box-caption">
						<div class="post-box-title h5 text-ubold"><a class="text-black" href="blog-single-post-sidebar-left.html">Planning a Trip to a Place Where You’ve Never Been</a></div>
						<ul class="list-inline post-box-meta list-inline-dashed list-inline-dashed-sm text-extra-small text-silver-chalice">
							<li class="text-uppercase"><img src="images/icons/icon-16-16x15.png" width="16" height="15" alt=""/><span class="text-middle inset-left-10">JUNE 20, 2019</span></li>
							<li class="text-bottom p text-uppercase"><img src="images/icons/icon-17-16x15.png" width="16" height="15" alt=""/><span class="inset-left-10">by <a href="testimonials.html">EMMA STONE</a></span></li>
						</ul>
						<p class="text-small text-silver-chalice">While I have no intention of ever planning trips day-by-day, I never like to go somewhere blind — it’s a sure-fire way to get ripped off, eat the wrong thing, get sick etc. Knowledge is power, and given that so much information about is available online, I feel that it is a good way to discover something new and educate yourself.</p><a class="button button-primary button-width-110" href="blog-single-post-sidebar-left.html">Read More</a>
					</div>
				</div>
				<!-- Post Box-->
				<div class="post-box post-box-wide d-block text-left">
					<div class="post-box-img-wrap"><a href="blog-single-post-sidebar-left.html"><img src="images/blog/post-07-1170x440.jpg" width="1170" height="440" alt=""/></a></div>
					<div class="post-box-caption">
						<div class="post-box-title h5 text-ubold"><a class="text-black" href="blog-single-post-sidebar-left.html">How to Choose Your Ideal Travel Destination</a></div>
						<ul class="list-inline post-box-meta list-inline-dashed list-inline-dashed-sm text-extra-small text-silver-chalice">
							<li class="text-uppercase"><img src="images/icons/icon-16-16x15.png" width="16" height="15" alt=""/><span class="text-middle inset-left-10">JUNE 20, 2019</span></li>
							<li class="text-bottom p text-uppercase"><img src="images/icons/icon-17-16x15.png" width="16" height="15" alt=""/><span class="inset-left-10">by <a href="testimonials.html">EMMA STONE</a></span></li>
						</ul>
						<p class="text-small text-silver-chalice">You have vacation days coming up? Finally! You have been dreaming of taking holidays for long. Now it is here, finally, you are ready to escape the office… but where should you go? It does not happen every day, so you want to find the perfect holiday destination… Welcome to our guide of your next ideal travel destination.</p><a class="button button-primary button-width-110" href="blog-single-post-sidebar-left.html">Read More</a>
					</div>
				</div>
				<!-- Classic Pagination-->
				<ul class="pagination-classic">
					<li class="active"><span>1</span></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li>...</li>
					<li><a href="#">12</a></li>
				</ul>
			</div>
			<div class="col-md-11 col-lg-3 text-lg-left">
				<!-- Aside-->
				<aside class="blog-aside box box-xs d-block bg-default">
					<div class="blog-aside-item">
						<p class="text-black text-ubold text-uppercase text-spacing-200">Search</p>
						<!-- RD Search Form-->
						<form class="form-blog-search form-blog-search-type-2 form-search rd-search" action="search-results.html" method="GET">
							<button class="form-search-submit" type="submit"><span class="fa fa-search"></span></button>
							<div class="form-wrap form-wrap-xs">
								<label class="form-label form-search-label form-label-sm" for="blog-sidebar-form-search-widget">Enter your request</label>
								<input class="form-search-input input-sm form-input input-sm" id="blog-sidebar-form-search-widget" type="text" name="s" autocomplete="off">
							</div>
						</form>
					</div>
					<hr class="hr bg-gallery">
					<div class="blog-aside-item">
						<p class="text-black text-ubold text-uppercase text-spacing-200">Categories</p>
						<!-- List-->
						<ul class="list list-1 list-modern">
							<li class="text-small"><a class="text-silver-chalice" href="#"><span class="pull-left">Family (26)</span><span class="pull-right text-ubold icon mdi mdi-chevron-right"></span><span class="clearfix"></span></a></li>
							<li class="text-small"><a class="text-silver-chalice" href="#"><span class="pull-left">Adventure (66)</span><span class="pull-right text-ubold icon mdi mdi-chevron-right"></span><span class="clearfix"></span></a></li>
							<li class="text-small"><a class="text-silver-chalice" href="#"><span class="pull-left">Romantic (59)</span><span class="pull-right text-ubold icon mdi mdi-chevron-right"></span><span class="clearfix"></span></a></li>
							<li class="text-small"><a class="text-silver-chalice" href="#"><span class="pull-left">Wildlife (55)</span><span class="pull-right text-ubold icon mdi mdi-chevron-right"></span><span class="clearfix"></span></a></li>
							<li class="text-small"><a class="text-silver-chalice" href="#"><span class="pull-left">Beach (89)</span><span class="pull-right text-ubold icon mdi mdi-chevron-right"></span><span class="clearfix"></span></a></li>
							<li class="text-small"><a class="text-silver-chalice" href="#"><span class="pull-left">Honeymoon (27)</span><span class="pull-right text-ubold icon mdi mdi-chevron-right"></span><span class="clearfix"></span></a></li>
							<li class="text-small"><a class="text-silver-chalice" href="#"><span class="pull-left">Island (45)</span><span class="pull-right text-ubold icon mdi mdi-chevron-right"></span><span class="clearfix"></span></a></li>
						</ul>
						<hr class="hr bg-gallery">
						<div class="blog-aside-item">
							<p class="text-black text-ubold text-uppercase text-spacing-200">Tags</p>
							<div class="group group-xs button-tags text-left"><a class="button button-sm button-gray" href="#">Travel</a><a class="button button-sm button-gray" href="#">Adventure</a><a class="button button-sm button-gray" href="#">Relax</a><a class="button button-sm button-gray" href="#">Brasil</a><a class="button button-sm button-gray" href="#">Trip</a><a class="button button-sm button-gray" href="#">Honeymoon</a><a class="button button-sm button-gray" href="#">Promotions</a><a class="button button-sm button-gray" href="#">North America</a></div>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
