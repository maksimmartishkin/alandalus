<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use yii\widgets\Breadcrumbs;

$this->title = !empty($content) && !empty($content['longtitle'])?$content['longtitle']:'';
if(!empty($content['lt4']) && !empty($content['uri4'])) {
	$this->params['breadcrumbs'][] = ['label'=>$content['lt4'], 'url'=>Url::to([$content['uri4']])];
}
if(!empty($content['lt3']) && !empty($content['uri3'])) {
	$this->params['breadcrumbs'][] = ['label'=>$content['lt3'], 'url'=>Url::to([$content['uri3']])];
}
if(!empty($content['lt2']) && !empty($content['uri2'])) {
	$this->params['breadcrumbs'][] = ['label'=>$content['lt2'], 'url'=>Url::to([$content['uri2']])];
}
$this->params['breadcrumbs'][] = $this->title;
//$isMap = false;
$isFormQuestionNedvizimost = false;
$map_lat = false;
$map_long = false;
$location_detail_baza = false;
//var_dump($this->params, $content);die();
//$this->registerCssFile('css/demo.css', ['depends' => AppAsset::className()]);
//$this->registerCssFile('css/style-galery.css', ['depends' => AppAsset::className()]);
//$this->registerCssFile('css/elastislide.css', ['depends' => AppAsset::className()]);
?>
<?php $this->registerCss(<<<CSS
	#thumbs { padding-top: 10px; overflow: hidden; }
	#thumbs img, #largeImage {
		border: 1px solid gray;
		padding: 4px;
		background-color: white;
		cursor: pointer;
	}
	#thumbs img {
		float: left;
		margin-right: 6px;
		width: 100px;
	}
	#description {
		background: black;
		color: white;
		position: absolute;
		bottom: 0;
		padding: 10px 20px;
		width: 525px;
		margin: 5px;
	}
	#panel { position: relative; }
CSS
);
?>
<section class="section parallax-container bg-black section-height-mac" data-parallax-img="/images/background/shapka_n.png">
	<div class="parallax-content">
		<div class="bg-overlay-darker">
			<div class="container section-34 section-md-60 section-lg-115">
				<h1 class="d-none d-lg-inline-block text-white"><?php /*echo $this->title */?></h1>
			</div>
		</div>
	</div>
</section>


<section class="section-100 bg-wild-wand text-md-left">
	<div class="container">
		<div class="row justify-content-sm-center row-50">
			<div class="col-md-11 col-lg-3 text-lg-left">
				<?php echo Yii::$app->controller->renderPartial("/site/aside-menu-new", ['procat' => true, 'side_menu' => !empty($side_menu)?$side_menu:false, 'link' => !empty($link)?$link:false]);?>
			</div>
			<div class="col-md-11 col-lg-9 order-lg-1">
				<!-- Box-->
				<div class="box box-lg box-single-post bg-default d-block">
					<?php /*= Breadcrumbs::widget([
						'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					]) */?>
					<?php if($content['id'] && !Yii::$app->user->isGuest) : ?>
						<?= Html::a('Редактировать страницу', [Url::to(['/admin/content/update-site']), 'id' => $content['id']], ['class' => 'button button-primary editable-content']) ?>
					<?php endif; ?>
					<h4 class="text-ubold"><?=$this->title?></h4>
					<div class="text-silver-chalice text-justify">
					<?php if (!empty($table_nedvizimost)) : ?>
						<?php
							$map_lat = !empty($table_nedvizimost['map_lat'])?$table_nedvizimost['map_lat']:false;
							$map_long = !empty($table_nedvizimost['map_long'])?$table_nedvizimost['map_long']:false;
							$location_detail_baza = !empty($table_nedvizimost['location_detail_baza'])?$table_nedvizimost['location_detail_baza']:'';
							$isFormQuestionNedvizimost = true;
						?>
						<?php echo Yii::$app->controller->renderPartial("@app/views/site/table_nedvizimost", ['table_nedvizimost' => $table_nedvizimost]);?>
					<?php endif; ?>
					<?php
					if($content['content']) :
//						$id_cache = md5(Url::to().'content');
//						if ($this->beginCache($id_cache, ['duration' => 3600])) :
							echo $content['content'];
//							$this->endCache();
//						endif;
					endif;
					?>
					<?php if($map_lat && $map_long) : ?>
						<ul class="gmap" data-lat="<?=$map_lat?>" data-lng="<?=$map_long?>">
							<li data-lat="<?=$map_lat?>" data-lng="<?=$map_long?>"><?=$location_detail_baza?></li>
						</ul>
						<?php echo Yii::$app->controller->renderPartial("@app/views/site/googleMapsScript");?>
					<?php endif; ?>
					<?php if($isFormQuestionNedvizimost) : ?>
						<?php echo Yii::$app->controller->renderPartial("@app/views/site/form-question", ['form_subject' => $this->title]);?>
					<?php endif; ?>


					</div>
					<hr class="hr bg-gallery">


					<div class="d-inline-block inset-xs-center-12">
						<h1>Понравилось? Поделись с друзьями!</h1>
						<div class="share42init" data-url="<?=Url::base(true)?><?=Url::to()?>" data-title="<?=$this->title?>"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<?php
//$this->registerJsFile('/js/init.js', ['depends' => \yii\web\YiiAsset::className()]);
//$this->registerJsFile('/js/jquery.tmpl.min.js', ['depends' => \yii\web\YiiAsset::className()]);
//$this->registerJsFile('/js/jquery.easing.1.3.js', ['depends' => \yii\web\YiiAsset::className()]);
//$this->registerJsFile('/js/jquery.elastislide.js', ['depends' => \yii\web\YiiAsset::className()]);
//$this->registerJsFile('/js/gallery.js', ['depends' => \yii\web\YiiAsset::className()]);
?>
<?php
//	$this->endCache();
//endif;
?>
<?php
//yii\helpers\Html::script('
//<div class="rg-image-wrapper">
//		{{if itemsCount > 1}}
//			<div class="rg-image-nav">
//				<a href="#" class="rg-image-nav-prev">Previous Image</a>
//				<a href="#" class="rg-image-nav-next">Next Image</a>
//			</div>
//		{{/if}}
//		<div class="rg-image"></div>
//		<div class="rg-loading"></div>
//		<div class="rg-caption-wrapper">
//			<div class="rg-caption" style="display:none;">
//				<p></p>
//			</div>
//		</div>
//	</div>
//', ['type' => 'text/x-jquery-tmpl', 'id' => '']);
?>
<?php $this->registerJs(<<<JS
	$('#thumbs').delegate('img','click', function(){
		$('#largeImage').attr('src',$(this).attr('src').replace('thumb','large'));
		$('#description').html($(this).attr('alt'));
	});

JS
); ?>