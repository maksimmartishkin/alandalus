<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;
use developit\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="section parallax-container bg-black section-height-mac" data-parallax-img="/images/background/background-23-1920x900.jpg">
	<div class="parallax-content">
		<div class="bg-overlay-darker">
			<div class="container section-34 section-md-60 section-lg-115">
				<h1 class="d-none d-lg-inline-block text-white"><?=$this->title?></h1>
			</div>
		</div>
	</div>
</section>

<!-- Contact Us-->
<section class="section-80">
	<div class="container">
		<div class="row row-50 justify-content-sm-center justify-content-lg-between text-md-left">
			<div class="col-md-10 col-lg-5">
				<h5 class="text-ubold">О Компании</h5>
				<p class="text-offset-2 text-left">
					TVOYA ISPANIA S.L. - это испанская компания, которая, прежде всего, нацелена на работу с российскими клиентами, а также с клиентами из стран СНГ.
				</p>
				<p class="text-offset-2 text-left">
					Создавая проект Alandalus.ru, мы ставили своей задачей подготовить максимум полезной информации для тех, кто собирается приобрести недвижимость в Испании или же просто приезжает в эту гостеприимную страну на отдых. Среди сотрудников нашей компании есть как россияне, прожившие значительное время в Испании, так и коренные испанцы. Это означает, что мы прекрасно знаем специфику рынка недвижимости в Испании изнутри, знаем испанское законодательство, а также хорошо знакомы с культурой и бытом этой страны, что позволяет нам профессионально работать как в сфере недвижимости, так и в сфере организации индивидуального отдыха. Наши юристы являются коренными испанцами с большим опытом работы, владеющими, при этом, русским языком. Это позволяет нам оказывать любую юридическую поддержку на самом высоком уровне. Поскольку наша компания зарегистрирована и осуществляет свою деятельность в Испании,то мы действуем в рамках европейского законодательства. Соответственно, и права наших клиентов защищаются европейской юридической системой. Это обеспечивает гарантии безопасности при проведении сделок, а также защиту приобретенной недвижимости.
				</p>
				<p class="text-offset-2 text-left">
					Мы выбрали название "TVOYA ISPANIA" не случайно. Главная наша цель - не просто предложить Вам недвижимость в Испании, или организовать отдых в этой стране, а сделать для Вас Испанию вторым домом, где Вы будете чувствовать себя уютно и комфортно!
				</p>
			</div>
			<div class="col-md-10 col-lg-7 col-xl-3">
				<h5 class="text-ubold">Контакты</h5>
				<!-- Contact Info-->
				<address class="contact-info text-left">
					<p class="d-block contact-info-address"><a class="text-gray" href="#"><span class="unit flex-row unit-spacing-xs"><span class="unit-left"><img class="img-responsive center-block" src="/images/icons/icon-01-16x21.png" width="16" height="21" alt="Адрес в Испании" title="Адрес в Испании"></span><span class="unit-body"><span>CL ACEBO S/N, URB. MYRAMAR DEL SOL Bloq.3, Esc.C, 1B, 29649 MIJAS, MALAGA, Spain</span></span></span></a></p>
					<p class="d-block"><a class="text-gray" href="tel:#"><span class="unit align-items-center flex-row unit-spacing-xs"><span class="unit-left"><img class="img-responsive center-block" src="/images/icons/icon-02-19x19.png" width="19" height="19" alt="Телефон в Испании" title="Телефон в Испании"></span><span class="unit-body"><span>+34 603 306 511 (доступен также в Viber и WatsUp)</span></span></span></a></p>
					<p class="d-block"><a class="text-gray" href="mailto:#"><span class="unit align-items-center flex-row unit-spacing-xs"><span class="unit-left"><img class="img-responsive center-block" src="/images/icons/icon-04-20x13.png" width="20" height="13" alt="Электронная почта" title="Электронная почта"></span><span class="unit-body"><span>al-andalus@yandex.ru</span></span></span></a></p>
					<p class="d-block"><a class="text-gray" href="tel:#"><span class="unit align-items-center flex-row unit-spacing-xs"><span class="unit-left"><strong>NIF (ИНН):</strong></span><span class="unit-body"><span>B93235976</span></span></span></a></p>
					<?php /*<p class="d-block text-small"><a class="text-gray" href="#"><span class="unit align-items-center flex-row unit-spacing-xs"><span class="unit-left"><img class="img-responsive center-block" src="images/icons/icon-05-19x19.png" width="19" height="19" alt=""></span><span class="unit-body"><span>demolink.org</span></span></span></a></p>*/?>
				</address>
			</div>
			<div class="col-md-10 col-lg-7 col-xl-3">
				<h5 class="text-ubold text-center">Обратная связь</h5>
				<?php $form = ActiveForm::begin([
					'id' => 'contact-form',
					//				'action' => 'form-question',
					'enableClientValidation' => true,
					//				'enableAjaxValidation' => true,
					'options' => [
						//					'class' => 'rd-mailform text-left',
						//				'data-form-output' => 'form-output-global',
						//					'data-form-type' => 'contact'
					],
				]); ?>

				<?= $form->field($model, 'name', [
//					'options' => [
//						'class' => 'form-wrap form-wrap-xs'
//					],
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->textInput(['class' => 'form-input'])
					->label($model->attributeLabels()['name'], ['class' => 'form-label']); ?>

				<?= $form->field($model, 'email', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs'
					],
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->input('email', ['class' => 'form-input'])
					->label($model->attributeLabels()['email'], ['class' => 'form-label']); ?>

				<!--				--><?//= $form->field($model, 'subject')->hiddenInput(['value' => 'form-input']); ?>

				<?= $form->field($model, 'phone', [
//					'options' => [
//						'class' => 'form-wrap form-wrap-xs'
//					],
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->textInput(['class' => 'form-input'])
					->label($model->attributeLabels()['phone'], ['class' => 'form-label']); ?>

				<?= $form->field($model, 'body', [
//					'options' => [
//						'class' => 'form-wrap form-wrap-xs'
//					],
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->textarea(['rows' => 6, 'class' => 'form-input', 'style' => 'height:120px;'])
					->label($model->attributeLabels()['body'], ['class' => 'form-label']); ?>

				<?php /*= $form->field($model, 'verifyCode', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs'
					],
					'template' => '
								<div class="form-wrap form-wrap-xs">
									{label}
									{input}
									<span class="form-validation">{error}</span>
								</div>
							'
				])->widget(Captcha::className(), [
					'template' => '
								<div class="form-wrap form-wrap-xs">
									{image}
									{input}
								</div>
							
							',
				])
					//->textInput(['class' => 'form-input', 'data-constraints' => '@Required'])
					->label(false)
				*/?>
				<!--				<div class="form-wrap form-wrap-xs"><div class="col-lg-3">{image}</div><div class="col-lg-6 form-input" data-constraints="@Required">{input}</div></div>-->

				<?= $form->field($model, 'reCaptchav2')->widget(
					\himiklab\yii2\recaptcha\ReCaptcha::className(),
					[
						'siteKey' => RECAPTCHA_SITE_KEY_V2
					]
				)->label(false); ?>


				<div class="form-button text-center text-md-center">
					<?= Html::submitButton('Отправить', ['class' => 'button button-width-110 button-primary', 'name' => 'contact-button']) ?>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</section>
<?php /*<!-- RD Google Map-->
<section>
	<div class="google-map-container" data-center="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-zoom="5" data-icon="images/gmap_marker.png" data-icon-active="images/gmap_marker_active.png" data-styles="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:60}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:40},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;administrative.province&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;lightness&quot;:30}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ef8c25&quot;},{&quot;lightness&quot;:40}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b6c54c&quot;},{&quot;lightness&quot;:40},{&quot;saturation&quot;:-40}]},{}]">
		<div class="google-map"></div>
		<ul class="google-map-markers">
			<li data-location="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-description="9870 St Vincent Place, Glasgow"></li>
		</ul>
	</div>
</section>*/?>
