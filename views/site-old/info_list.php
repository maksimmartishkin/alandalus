<?php

use yii\helpers\Url;

?>

<?php if(!empty($info_list)) : ?>
	<?php foreach ($info_list as $info) : ?>
		<div class="post-box post-box-wide d-block text-left">
			<?php /*<div class="post-box-img-wrap"><a href="blog-single-post-sidebar-left.html"><img src="images/blog/post-01-1170x440.jpg" width="1170" height="440" alt=""/></a></div>*/?>
			<div class="post-box-caption-custom">
				<div class="post-box-title h5 text-ubold"><a class="text-black" href="<?=Url::to([$info->uri])?>"><?=$info->pagetitle?></a></div>
				<?php /*<ul class="list-inline post-box-meta list-inline-dashed list-inline-dashed-sm text-extra-small text-silver-chalice">
					<li class="text-uppercase"><img src="images/icons/icon-16-16x15.png" width="16" height="15" alt=""/><span class="text-middle inset-left-10">JUNE 20, 2019</span></li>
					<li class="text-bottom p text-uppercase"><img src="images/icons/icon-17-16x15.png" width="16" height="15" alt=""/><span class="inset-left-10">by <a href="testimonials.html">EMMA STONE</a></span></li>
				</ul>*/?>
				<p class="text-small text-silver-chalice"><?=$info->introtext?></p>
				<a class="button button-primary button-width-110" href="<?=Url::to([$info->uri])?>">Читать дальше...</a>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>