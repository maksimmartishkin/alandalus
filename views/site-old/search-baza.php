<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;

//var_dump($query_result);die();
$this->title = 'Поиск по базе недвижимости в Испании';
$this->params['breadcrumbs'][] = $this->title;

$menu_lists = [
	'nedvizimost-ispania' => [
		'title' => 'Недвижимость',
		'link' => 'nedvizimost-ispania',
	],
	'otdyh-v-ispanii' => [
		'title' => 'Отдых',
		'link' => 'otdyh-v-ispanii',
	],
	'arenda-yaxt' => [
		'title' => 'Яхты',
		'link' => 'arenda-yaxt',
	],
	'arenda-avtomobilya-v-ispanii' => [
		'title' => 'Автомобили',
		'link' => 'arenda-avtomobilya-v-ispanii',
	],
	'akczii' => [
		'title' => 'Акции',
		'link' => 'akczii',
	],
	'andalucia' => [
		'title' => 'Об Андалусии',
		'link' => 'andalucia',
	],
	'poleznaya-informacziya' => [
		'title' => 'Информация',
		'link' => 'poleznaya-informacziya',
	],
	'kontaktyi' => [
		'title' => 'Контакты',
		'link' => 'kontaktyi',
	],
];


//global $modx;


//echo $modx->runSnippet('getPage', array('element' => 'getResources', 'parents' => $parents, 'debug' => '1', 'tpl' => 'sale', 'limit' => '8', 'sortBy' => 'publishedon', 'showHidden' => '1', 'includeTVs' => '1', 'tvFilters' => $filter, 'processTVs' => '1', 'tvPrefix' => '', ’cache’ => 0,));



?>

<section class="section parallax-container bg-black section-height-mac" data-parallax-img="/images/background/background-23-1920x900.jpg">
	<div class="parallax-content">
		<div class="bg-overlay-darker">
			<div class="container section-34 section-md-60 section-lg-115">
				<h1 class="d-none d-lg-inline-block text-white"><?=$this->title?></h1>
			</div>
		</div>
	</div>
</section>

<section class="section-80 bg-wild-wand text-md-left">
	<div class="container">
		<div class="row justify-content-sm-center row-50">
			<div class="col-md-11 col-lg-9 order-lg-1">
				<!-- Box-->
				<div class="box box-lg box-single-post bg-default d-block">
					<?= Breadcrumbs::widget([
						'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					]) ?>
					<h4 class="text-ubold"><?=$this->title?></h4>
					<div class="text-silver-chalice text-left">

						<?php if(!empty($query_result)) : ?>
<!--							--><?php //foreach ($query_result as $key => $result) : ?>
								<?php echo Yii::$app->controller->renderPartial("@app/views/site/baza_ditto", ['baza' => $query_result]);?>

<!--							--><?php //endforeach; ?>
						<?php else : ?>
							<p class="alert alert-warning" role="alert">
								По вашему запросу ничего не найдено. Попробуйте ввести похожие по смыслу слова, чтобы получить лучший результат.
							</p>
						<?php endif; ?>



					</div>
					<hr class="hr bg-alto">
					<?php
//					if(!empty($pages)) {
//						echo LinkPager::widget([
//							'pagination' => $pages,
//							'hideOnSinglePage' => true,
////							'prevPageLabel' => '<i class="news-pagination__faback fa fa-angle-double-left" aria-hidden="true"></i> Назад ',
////							'prevPageCssClass' => 'news-pagination__item back',
////							'nextPageLabel' => 'Вперед <i class="news-pagination__fanext news-pagination__fa fa fa-angle-double-right" aria-hidden="true"></i>',
////							'nextPageCssClass' => 'news-pagination__item next',
//							'maxButtonCount'=>5,
////							'pageCssClass' => ['class' => 'news-pagination__item'],
//							'options' => ['class' => 'pagination-classic'],
////							'linkOptions' => ['class' => 'news-pagination__link'],
//						]);
//					}
					?>



					<div class="d-inline-block inset-md-left-10">
						<!-- List Inline-->
						<ul class="list-inline list-inline-modern list-inline-11 bg-wild-wand">
							<li class="text-center"><span class="icon icon-sm icon-circle icon-filled-primary"><img class="img-responsive center-block" src="images/icons/icon-18-18x15.png" width="18" height="15" alt=""></span></li>
							<li class="text-center"><a class="icon fa fa-facebook-f text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-twitter text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-youtube text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-linkedin text-gray" href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-11 col-lg-3 text-lg-left">
				<?php echo Yii::$app->controller->renderPartial("@app/views/site/aside-menu-search-result", ['side_menu' => $menu_lists, 'link' =>'search-baza', 'text_request' => false]);?>
			</div>

		</div>
	</div>
</section>
<section class="section-80 bg-wild-wand">
	<div class="container">
		<div class="rd-search-results">


		</div>
	</div>
</section>
