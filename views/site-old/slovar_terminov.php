<?php

use yii\helpers\Url;

?>

<?php if(!empty($slovar_terminov)) : ?>
	<?php foreach ($slovar_terminov as $slovar) : ?>
		<div class="post-box post-box-wide d-block text-left">
			<div class="post-box-caption-custom">
				<div class="post-box-title h5 text-ubold"><a class="text-black" href="<?=Url::to([$slovar->uri])?>"><?=$slovar->pagetitle?></a></div>
				<p class="text-small text-silver-chalice"><?=$slovar->introtext?></p>
				<a class="button button-primary button-width-110" href="<?=Url::to([$slovar->uri])?>">Читать дальше...</a>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>