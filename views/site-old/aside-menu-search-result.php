<?php

use yii\helpers\Html;
use yii\helpers\Url;

//var_dump($side_menu);
?>
<aside class="blog-aside box box-xs d-block bg-default">
		<?php echo Yii::$app->controller->renderPartial("@app/views/site/search-for-site-form", ['text_request' => false]);?>
	<?php if(!empty($link) && ($link == 'search-baza')): ?>
		<hr class="hr bg-gallery">
		<?php echo Yii::$app->controller->renderPartial("@app/views/site/bazasearch_h", ['form_data' => Yii::$app->request->bodyParams]);?>
	<?php endif; ?>
	<hr class="hr bg-gallery">
	<div class="blog-aside-item">
		<?php if(!empty($side_menu)) : ?>
			<ul class="list-marked-icon">
				<?php foreach ($side_menu as $item) : ?>
					<li><a href="<?=!empty($item['link'])?Url::to([$item['link']]):'#';?>" title="<?=!empty($item['title'])?$item['title']:''?>"><?=!empty($item['title'])?$item['title']:''?></a>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<hr class="hr bg-gallery">
	</div>

</aside>
