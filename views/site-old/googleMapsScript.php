<?php



$this->registerCss(<<<CSS
	.gmap {
		width: 720px;
		height: 405px;
		padding: 5px;
		box-shadow: inset 0.5px 0.5px 0.5px 0.5px #ab8b6a;
		background: none repeat scroll 0 0 rgba(197, 177, 144, 0.3);
		border: 1px ridge #80593a;
	}
CSS
);
$this->registerJsFile('https://maps.google.com/maps/api/js?sensor=false', ['depends' => \yii\web\YiiAsset::className()]);
$this->registerJsFile('/js/gmaps.js', ['depends' => \yii\web\YiiAsset::className()]);

?>

<?php $this->registerJs(<<<JS
    (function($){
        $(document).ready(function(){
            var count = 1;
            $(".gmap").each(function(){
                var markers = [];
                $("li", $(this)).each(function(){
                    markers.push({
                        lat: $(this).attr('data-lat'),
                        lng: $(this).attr('data-lng'),
                        title: $(this).html(),
                    });
                    $(this).remove();
                });

                $(this).append("<li id='gmap_"+count+"' style='height:100%;'/>")

                var zoom = 10;
                if($(this).attr('zoom')){
                    zoom = parseInt($(this).attr('zoom'));
                }

                var map = new GMaps({
                    div: '#gmap_'+count,
                    zoom: zoom,
                    lat: $(this).attr('data-lat'),
                    lng: $(this).attr('data-lng')
                });

                $(markers).each(function(key, value){
                    map.addMarker(value);
                });

                count++;
            });
        });
    })(jQuery);
JS
); ?>