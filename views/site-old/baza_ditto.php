<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

//var_dump($baza);die();

?>

<?php if(!empty($baza)) : ?>
	<?php if(!empty($baza['data'])) : ?>
		<?php foreach ($baza['data'] as $value) : ?>
			<h3 class="hidden-lg hidden-md hidden-sm"> <a href="<?=Url::to([$value['uri']])?>"> <?=$value['longtitle']?></a></h3>
			<div class="row" style="display: inline-flex;margin-top: 15px;">
				<div class="col-sm-6 center">
					<a href="<?=Url::to([$value['uri']])?>">
						<?php if(!empty($value['value']['img_baza1']['value'])) : ?>
							<?php if($img_baza1 = explode('|', $value['value']['img_baza1']['value'])): ?>
								<img src="<?=Url::to([$img_baza1[0]])?>" class="img_white" border="0">
							<?php endif; ?>
						<?php elseif(!empty($value['value']['images_baza']['value'])) : ?>
							<?php if($images_baza = explode('|', $value['value']['images_baza']['value'])): ?>
								<img src="<?=Url::to([$images_baza[0]])?>" class="img_white" border="0">
							<?php endif; ?>
<!--							--><?php //var_dump($value['value']['images_baza']); ?>

						<?php endif; ?>
<!--						--><?php //var_dump($img_baza1); ?>
						<img src="<?//=Url::to([$img_baza1[0]])?>" class="img_white" border="0">
					</a>
				</div>
				<div class="col-sm-6">
					<div class="row" style="display: block;">
						<h3 class="hidden-xs"> <a href="<?=Url::to([$value['uri']])?>"> <?=$value['longtitle']?></a></h3>
						<div class="col-xs-12">
							<?=!empty($value['value']['town_baza']['value'])?$value['value']['town_baza']['value'].', ':''?>

							<?=!empty($value['value']['province_baza']['value'])?$value['value']['province_baza']['value']:''?>
							<?=!empty($value['value']['location_detail_baza']['value'])?'('.$value['value']['location_detail_baza']['value'].')':''?>
						</div>
						<?php if(!empty($value['value']['built_baza'])) : ?>
							<div class="col-xs-7"><strong>Площадь:</strong></div>
							<div class="col-xs-5"><?=$value['value']['built_baza']['value']?> м<sup>2</sup></div>
						<?php endif; ?>
						<?php if(!empty($value['value']['plot_baza']) && $value['value']['plot_baza']['value'] > 0) : ?>
							<div class="col-xs-7"><strong>Участок:</strong></div>
							<div class="col-xs-5"><?=$value['value']['plot_baza']['value']?> м<sup>2</sup></div>
						<?php endif; ?>

						<?php if(!empty($value['value']['baths_baza'])) : ?>
							<div class="col-xs-7"><strong>Ванных комнат:</strong></div>
							<div class="col-xs-5"><?=$value['value']['baths_baza']['value']?></div>
						<?php endif; ?>

						<?php if(!empty($value['value']['beds_baza_do'])) : ?>
							<div class="col-xs-7"><strong>Спален:</strong></div>
							<div class="col-xs-5"><?=$value['value']['beds_baza_do']['value']?></div>

						<?php endif; ?>

						<?php if(!empty($value['value']['pool_baza'])) : ?>
							<div class="col-xs-7"><strong>Бассейн:</strong></div>
							<div class="col-xs-5"><?=$value['value']['pool_baza']['value']?></div>
						<?php endif; ?>
						<?php if(!empty($value['value']['price_baza'])) : ?>
							<div class="col-xs-7">
								<strong>
									<?php if(!empty($value['value']['price_freq_baza']) && $value['value']['price_freq_baza']['value'] == 'sale') : ?>
										Цена
									<?php elseif(!empty($value['value']['price_freq_baza']) && $value['value']['price_freq_baza']['value'] == 'week') : ?>
										Стоимость аренды в неделю:
									<?php endif; ?>
								</strong>
							</div>
							<?php if(isset($value['value']['price_baza']['value'])) : ?>
								<div class="price col-xs-5">
									<?php if(preg_match("/^([0-9]+)/", $value['value']['price_baza']['value'], $match)): ?>
										<?=number_format($value['value']['price_baza']['value'], 0, '.', ' ')?> €
									<?php else: ?>
										<?=$value['value']['price_baza']['value']?> €
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<div class="col-xs-12 text-right"><a href="<?=Url::to([$value['uri']])?>" class="button button-primary">Подробнее</a></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<hr class="hr bg-gallery" style="margin-top: 25px;">
		<?php endforeach; ?>
	<?php endif; ?>
<!--	--><?php //echo '<pre>';print_r($baza); die();?>
	<?php if(!empty($baza['pages'])) : ?>
			<?php
				echo LinkPager::widget([
					'pagination' => $baza['pages'],
					'hideOnSinglePage' => true,
	//			'prevPageLabel' => '<i class="news-pagination__faback fa fa-angle-double-left" aria-hidden="true"></i> Назад',
	//			'prevPageCssClass' => 'news-pagination__item back',
//					'disableCurrentPageButton' => false,
	//			'nextPageLabel' => 'Вперед <i class="news-pagination__fanext news-pagination__fa fa fa-angle-double-right" aria-hidden="true"></i>',
	//			'nextPageCssClass' => 'news-pagination__item next',
					'maxButtonCount'=>5,
	//			'pageCssClass' => ['class' => 'news-pagination__item'],
//					'disabledPageCssClass' => ['class' => 'news-pagination__item'],
					'options' => [
						'class' => 'pagination-classic'],
	//				'linkOptions' => ['class' => 'news-pagination__link'],
				]);
			?>
	<?php endif; ?>

<?php endif; ?>