<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $content->title;
$this->params['breadcrumbs'][] = $this->title;
//var_dump(strpos($content->content, "[['$form']]") !== false, $content->content);die();
?>


<section class="section parallax-container bg-black section-height-mac" data-parallax-img="/images/background/background-23-1920x900.jpg">
	<div class="parallax-content">
		<div class="bg-overlay-darker">
			<div class="container section-34 section-md-60 section-lg-115">
				<h1 class="d-none d-lg-inline-block text-white"><?=$this->title?></h1>
			</div>
		</div>
	</div>
</section>


<section class="section-80 bg-wild-wand text-md-left">
	<div class="container">
		<div class="row justify-content-sm-center row-50">
			<div class="col-md-11 col-lg-9 order-lg-1">
				<!-- Box-->
				<div class="box box-lg box-single-post bg-default d-block">
					<h4 class="text-ubold"><?=$this->title?></h4>
					<div class="text-silver-chalice text-left">

					<?php
					if($content->content) {
						echo $content->content;
					}
					?>



					</div>
					<hr class="hr bg-alto">



					<div class="d-inline-block inset-md-left-10">
						<!-- List Inline-->
						<ul class="list-inline list-inline-modern list-inline-11 bg-wild-wand">
							<li class="text-center"><span class="icon icon-sm icon-circle icon-filled-primary"><img class="img-responsive center-block" src="/images/icons/icon-18-18x15.png" width="18" height="15" alt=""></span></li>
							<li class="text-center"><a class="icon fa fa-facebook-f text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-twitter text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-youtube text-gray" href="#"></a></li>
							<li class="text-center"><a class="icon fa fa-linkedin text-gray" href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-11 col-lg-3 text-lg-left">
				<?php echo Yii::$app->controller->renderPartial("/site/aside-menu-new", ['procat' => true, 'side_menu' => !empty($side_menu)?$side_menu:false, 'link' => !empty($link)?$link:false]);?>
			</div>

		</div>
	</div>
</section>
