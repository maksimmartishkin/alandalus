<?php

use app\models\ContactForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$model = new ContactForm();
$model->subject = isset($form_subject)?$form_subject:'';
if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//	var_dump(Yii::$app->request->post());
	Yii::$app->session->setFlash('contactFormSubmitted');

	return Yii::$app->getResponse()->refresh();
}

?>
<div class="row">
	<div class="col-lg-12">
		<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

				<div class="alert alert-success">
					Ваша заявка отправлена. В ближайшее время с Вами свяжутся.
				</div>

				<?php /*<p>
					Note that if you turn on the Yii debugger, you should be able
					to view the mail message on the mail panel of the debugger.
					<?php if (Yii::$app->mailer->useFileTransport): ?>
						Because the application is in development mode, the email is not sent but saved as
						a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
																											Please configure the <code>useFileTransport</code> property of the <code>mail</code>
						application component to be false to enable email sending.
					<?php endif; ?>
				</p>*/?>

		<?php else: ?>
			<div class="form-question-block">
				<h5 class="text-ubold text-center">Обратная связь</h5>
				<?php $form = ActiveForm::begin([
					'id' => 'contact-form',
					//				'action' => 'form-question',
					'enableClientValidation' => true,
					//				'enableAjaxValidation' => true,
					'options' => [
						//					'class' => 'rd-mailform text-left',
						//				'data-form-output' => 'form-output-global',
						//					'data-form-type' => 'contact'
					],
				]); ?>

				<?= $form->field($model, 'name', [
					//					'options' => [
					//						'class' => 'form-wrap form-wrap-xs'
					//					],
					'template' => '
							<div class="form-wrap form-wrap-xs">
								{label}
								{input}
								<span class="form-validation">{error}</span>
							</div>
						'
				])->textInput(['class' => 'form-input'])
					->label($model->attributeLabels()['name'], ['class' => 'form-label']); ?>

				<?= $form->field($model, 'email', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs'
					],
					'template' => '
							<div class="form-wrap form-wrap-xs">
								{label}
								{input}
								<span class="form-validation">{error}</span>
							</div>
						'
				])->input('email', ['class' => 'form-input'])
					->label($model->attributeLabels()['email'], ['class' => 'form-label']); ?>

				<!--				--><?//= $form->field($model, 'subject')->hiddenInput(['value' => 'form-input']); ?>

				<?= $form->field($model, 'phone', [
					//					'options' => [
					//						'class' => 'form-wrap form-wrap-xs'
					//					],
					'template' => '
							<div class="form-wrap form-wrap-xs">
								{label}
								{input}
								<span class="form-validation">{error}</span>
							</div>
						'
				])->textInput(['class' => 'form-input'])
					->label($model->attributeLabels()['phone'], ['class' => 'form-label']); ?>

				<?= $form->field($model, 'body', [
					//					'options' => [
					//						'class' => 'form-wrap form-wrap-xs'
					//					],
					'template' => '
							<div class="form-wrap form-wrap-xs">
								{label}
								{input}
								<span class="form-validation">{error}</span>
							</div>
						'
				])->textarea(['rows' => 6, 'class' => 'form-input', 'style' => 'height:120px;'])
					->label($model->attributeLabels()['body'], ['class' => 'form-label']); ?>

				<?php /*= $form->field($model, 'verifyCode', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs'
					],
					'template' => '
							<div class="form-wrap form-wrap-xs">
								{label}
								{input}
								<span class="form-validation">{error}</span>
							</div>
						'
				])->widget(Captcha::className(), [
					'template' => '
							<div class="form-wrap form-wrap-xs">
								{image}
								{input}
							</div>
						
						',
				])
					//->textInput(['class' => 'form-input', 'data-constraints' => '@Required'])
					->label(false); */?>
				<!--				<div class="form-wrap form-wrap-xs"><div class="col-lg-3">{image}</div><div class="col-lg-6 form-input" data-constraints="@Required">{input}</div></div>-->

 				<?= $form->field($model, 'reCaptchav2')->widget(
					\himiklab\yii2\recaptcha\ReCaptcha::className(),
					[
						'siteKey' => RECAPTCHA_SITE_KEY_V2
					]
				)->label(false); ?>


				<div class="form-button text-center text-md-center">
					<?= Html::submitButton('Отправить', ['class' => 'button button-width-110 button-primary', 'name' => 'contact-button']) ?>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		<?php endif; ?>
	</div>
</div>

