<?php

use app\models\SiteContent;
use yii\helpers\Url;

if ($menuListsCache = Yii::$app->cache->get('main-menu')) {
	$menuLists = $menuListsCache;
} else {
	$menuLists = SiteContent::getMainMenu();
	Yii::$app->cache->set('main-menu', $menuLists);
}


$action_link = explode('/', Url::to());
$active_link = Url::to();

$links = array_filter($action_link);
$current_action = array_shift($links);
?>

	<nav class="rd-navbar rd-navbar-light rd-navbar-light-lightmenu rd-navbar-right-side rd-navbar-right-side-wrap-mobile"
		 data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-static"
		 data-lg-device-layout="rd-navbar-static" data-lg-layout="rd-navbar-static"
		 data-xl-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static"
		 data-xxl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-lg-stick-up-offset="1px"
		 data-xl-stick-up-offset="1px" data-xxl-stick-up-offset="1px">
		<div class="rd-navbar-inner">
			<div class="rd-navbar-panel">
				<button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap">
					<span></span>
				</button>
				<div class="rd-navbar-brand rd-navbar-brand-desktop">
					<a class="brand-name" href="<?= Url::to(['/']) ?>">
						<img width="148" height="30" src="/images/logo-dark-250x76.png" alt="">
					</a>
				</div>
				<div class="rd-navbar-brand rd-navbar-brand-mobile">
					<a class="brand-name" href="<?= Url::to(['/']) ?>">
						<img width="148" height="30"
							 src="/images/logo-dark-250x76.png" alt="">
					</a>
				</div>
				<button class="rd-navbar-collapse" data-rd-navbar-toggle=".rd-navbar-right-side-wrap">
					<span></span>
				</button>
			</div>
			<div class="rd-navbar-nav-wrap">
				<ul class="rd-navbar-nav">
					<?php foreach ($menuLists as $href => $menu) : ?>
						<li class="menu-list-width <?=$href?> <?= ($current_action == $href || ($current_action == 'contact' && $href == 'kontaktyi')) ? 'active' : '' ?>">
							<a href="<?= Url::to(["/$href"]); ?>"><?= $menu ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="rd-navbar-right-side-wrap">
				<p>
					<a class="text-black" href="tel:#">
						<i class="fa fa-phone icon-navbar-menu-phone" aria-hidden="true"></i>
						<span class="h5 text-big text-ubold text-middle">+34 603-306-511</span><br />
					</a>
					<a class="text-black" href="tg://+34603306511">
						<img class="icon-messagers-block-custom margin-right-20"
							 src="/images/icons/icon-telegram-1.png" />
					</a>
					<a class="text-black" href="whatsapp://+34603306511">
						<img class="icon-messagers-block-custom margin-right-20"
							 src="/images/icons/icon-whatsapp-1.png" />
					</a>
					<a class="text-black" href="viber://add?number=34603306511">
						<img class="icon-messagers-block-custom"
							 src="/images/icons/icon-viber-1.png">
					</a>
				</p>
				<?php /*
				<p>
			<div class="social whatsapp"><a><a class="text-black" href="whatsapp://+34 603-306-511"><i class="fa fa-whatsapp"></i></a></div>


			<a class="text-black" href="tel:#">

				</a></p>
			<!--					<img class="d-inline-block icon-blocks" src="/images/icons/Whatsapp_37229.png" alt="">-->
			<!--					<img class="d-inline-block icon-blocks" src="/images/icons/telegram_icon-icons.com_72055.png" alt="">-->
			<!--					<img class="d-inline-block icon-blocks" src="/images/icons/viber_14147.png" alt="">-->
			<!--					<i class="fa fa-2x fa-whatsapp icon-blocks" aria-hidden="true"></i><i class="fa fa-telegram" aria-hidden="true"></i>-->*/ ?>
			</div>
		</div>
	</nav>

<?php /*<!--<nav class="rd-navbar rd-navbar-transparent rd-navbar-white-stuck" data-sm-device-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="1px" data-xl-stick-up-offset="1px" data-xxl-stick-up-offset="1px" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static">-->
<!--	<div class="rd-navbar-inner">-->
		<!-- RD Navbar Panel-->
<!--		<div class="rd-navbar-panel">-->
			<!-- RD Navbar Toggle-->
<!--			<button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>-->
			<!-- RD Navbar Brand-->
<!--			<div class="rd-navbar-brand rd-navbar-brand-desktop"><a class="brand-name" href="--><?//=Url::to(['/']); ?><!--"><img width="250" height="76" src="/images/logo-dark-250x76.png" alt=""></a></div>-->
			<!-- RD Navbar Brand-->
			<div class="rd-navbar-brand rd-navbar-brand-mobile"><a class="brand-name" href="<?=Url::to(['/']); ?>"><img width="148" height="30" src="/images/logo-dark-250x76.png" alt=""></a></div>-->
<!--		</div>-->
<!--		<div class="rd-navbar-nav-wrap">-->
			<!-- RD Navbar Nav-->
<!--			<ul class="rd-navbar-nav">-->
				<?php foreach ($menu_lists as $href => $menu) : ?>
					<li class="<?=$current_action==$href?'active':''?>"><a href="<?=Url::to(["/$href"]);?>"><?=$menu?></a></li>
				<?php endforeach; ?>
<!--				<li class=""><a href="--><?//=Url::to(['/']);?><!--">Отдых</a></li>-->
<!--				<li class=""><a href="--><?//=Url::to(['/']);?><!--">Яхты</a></li>-->
<!--				<li class=""><a href="--><?//=Url::to(['/']);?><!--">Автомобили</a></li>-->
<!--				<li class=""><a href="--><?//=Url::to(['/']);?><!--">Акции</a></li>-->
<!--				<li class=""><a href="--><?//=Url::to(['/']);?><!--">Об Андалусии</a></li>-->
<!--				<li class=""><a href="--><?//=Url::to(['/']);?><!--">Информация</a></li>-->
<!--				<li class=""><a href="--><?//=Url::to(['/']);?><!--">Контакты</a></li>-->
				<!--							<li><a href="tours-grid.html">Tours</a>-->
				<!-- RD Navbar Dropdown-->
				<!--								<ul class="rd-navbar-dropdown">-->
				<!--									<li><a href="tours-grid.html">Tours Grid</a></li>-->
				<!--									<li><a href="tours-grid-variant-2.html">Tours Grid v2</a></li>-->
				<!--									<li><a href="tours-list.html">Tours List</a></li>-->
				<!--									<li><a href="tours-single.html">Tours Single</a></li>-->
				<!--								</ul>-->
				<!--							</li>-->
				<!--							<li><a href="about.html">About</a>-->
				<!-- RD Navbar Dropdown-->
				<!--								<ul class="rd-navbar-dropdown">-->
				<!--									<li><a href="our-team.html">Our Team</a></li>-->
				<!--									<li><a href="careers.html">Careers</a></li>-->
				<!--									<li><a href="faq.html">FAQ</a></li>-->
				<!--									<li><a href="testimonials.html">Testimonials</a></li>-->
				<!--								</ul>-->
				<!--							</li>-->
				<!--							<li><a href="#">Pages</a>-->
				<!-- RD Navbar Megamenu-->
				<!--								<div class="rd-navbar-megamenu">-->
				<!--									<div class="row">-->
				<!--										<div class="col-xl-4">-->
				<!--											<p class="rd-megamenu-header text-big text-black text-ubold">Pages 1</p>-->
				<!--											<ul class="rd-megamenu-list">-->
				<!--												<li><a href="press.html">Press</a></li>-->
				<!--												<li><a href="services.html">Services</a></li>-->
				<!--												<li><a href="pricing.html">Pricing</a></li>-->
				<!--												<li><a href="destinations.html">Destinations</a></li>-->
				<!--												<li><a href="signup.html">Sign Up</a></li>-->
				<!--												<li><a href="signup-variant-2.html">Sign Up v2</a></li>-->
				<!--											</ul>-->
				<!--										</div>-->
				<!--										<div class="col-xl-4">-->
				<!--											<p class="rd-megamenu-header text-big text-black text-ubold">Pages 2</p>-->
				<!--											<ul class="rd-megamenu-list">-->
				<!--												<li><a href="login.html">Login</a></li>-->
				<!--												<li><a href="forgot-password.html">Forgot Password</a></li>-->
				<!--												<li><a href="privacy.html">Privacy Policy</a></li>-->
				<!--												<li><a href="terms-of-use.html">Terms Of Use</a></li>-->
				<!--												<li><a href="sitemap.html">Sitemap</a></li>-->
				<!--												<li><a href="search-results.html">Search Results</a></li>-->
				<!--											</ul>-->
				<!--										</div>-->
				<!--										<div class="col-xl-4">-->
				<!--											<p class="rd-megamenu-header text-big text-black text-ubold">Pages 3</p>-->
				<!--											<ul class="rd-megamenu-list">-->
				<!--												<li><a href="404.html">404</a></li>-->
				<!--												<li><a href="503.html">503</a></li>-->
				<!--												<li><a href="comingsoon.html">Coming Soon</a></li>-->
				<!--												<li><a href="maintenance.html">Maintenance</a></li>-->
				<!--												<li><a href="underconstruction.html">Under Construction</a></li>-->
				<!--											</ul>-->
				<!--										</div>-->
				<!--									</div>-->
				<!--								</div>-->
				<!--							</li>-->
				<!--							<li><a href="gallery-cobbles.html">Gallery</a>-->
				<!-- RD Navbar Dropdown-->
				<!--								<ul class="rd-navbar-dropdown">-->
				<!--									<li><a href="gallery-cobbles.html">Gallery Cobbles</a></li>-->
				<!--									<li><a href="gallery-fullwidth.html">Gallery Fullwidth</a></li>-->
				<!--									<li><a href="gallery-grid.html">Gallery Grid</a></li>-->
				<!--									<li><a href="gallery-masonry.html">Gallery Masonry</a></li>-->
				<!--								</ul>-->
				<!--							</li>-->
				<!--							<li><a href="blog-grid.html">Blog</a>-->
				<!-- RD Navbar Dropdown-->
				<!--								<ul class="rd-navbar-dropdown">-->
				<!--									<li><a href="blog-grid.html">Blog Grid</a></li>-->
				<!--									<li><a href="blog-grid-sidebar-left.html">Blog Grid Sidebar</a></li>-->
				<!--									<li><a href="blog-list.html">Blog List</a></li>-->
				<!--									<li><a href="blog-list-sidebar-left.html">Blog List Sidebar</a></li>-->
				<!--									<li><a href="blog-list-variant-2.html">Blog List v2</a></li>-->
				<!--									<li><a href="blog-list-variant-2-sidebar-left.html">Blog List v2 Sidebar</a></li>-->
				<!--									<li><a href="blog-masonry.html">Blog Masonry</a></li>-->
				<!--									<li><a href="blog-modern.html">Blog Modern</a></li>-->
				<!--									<li><a href="blog-single-post.html">Blog Single Post</a></li>-->
				<!--									<li><a href="blog-single-post-sidebar-left.html">Blog Single Post Sidebar</a></li>-->
				<!--								</ul>-->
				<!--							</li>-->
				<!--							<li><a href="contacts.html">Contacts</a>-->
				<!-- RD Navbar Dropdown-->
				<!--								<ul class="rd-navbar-dropdown">-->
				<!--									<li><a href="contacts.html">Contacts</a></li>-->
				<!--									<li><a href="contacts-variant-2.html">Contacts v2</a></li>-->
				<!--								</ul>-->
				<!--							</li>-->
<!--			</ul>-->
<!--		</div>-->
<!--	</div>-->
<!--</nav>-->
*/ ?>