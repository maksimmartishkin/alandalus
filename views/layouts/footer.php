<?php

use yii\helpers\Url;
use app\models\SiteContent;

if ($aboutCache = Yii::$app->cache->get('about_text')) {
	$about = $aboutCache;
} else {
	$about = SiteContent::getAbout();
	Yii::$app->cache->set('about_text', $about);
}

?>

<footer class="page-footer footer-default section-top-50 text-left parallax-container footer-parallax-container-section"
		data-parallax-img="/images/backgrounds/background-42.1.png" data-wow-delay=".2s">
	<div class="page-footer-container">
		<div class="container">
			<div class="row row-top-0 row-20 justify-content-sm-center">
				<div class="col-md-12 col-lg-12">
					<div class="row">
						<div class="col-sm-6 col-md-4 col-lg-4">
							<ul class="list-style-none text-extra-small text-lists-footer">
								<li><a class="text-white" href="<?= Url::to(['/']) ?>">Главная</a></li>
								<li><a class="text-white" href="<?= Url::to(['/nedvizimost-ispania/prodaza']) ?>">Продажа недвижимости</a>
								</li>
								<li><a class="text-white" href="<?= Url::to(['/nedvizimost-ispania/arenda']) ?>">Аренда недвижимости</a>
								</li>
								<li><a class="text-white" href="<?= Url::to(['/arenda-avtomobilya-v-ispanii']) ?>">Прокат автомобилей</a>
								</li>
								<li><a class="text-white" href="<?= Url::to(['/arenda-yaxt']) ?>">Аренда яхт и рыбалка</a></li>
								<li><a class="text-white" href="<?= Url::to(['/andalucia']) ?>">Об Андалусии</a></li>
								<li><a class="text-white" href="<?= Url::to(['/poleznaya-informacziya']) ?>">Полезная информация</a></li>
								<li><a class="text-white" href="<?= Url::to(['/']) ?>">Карта сайта</a></li>
							</ul>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-4">
							<p class="text-big text-ubold">
								<a class="text-white" href="<?= Url::to(['/o-kompanii']) ?>">О компании</a>
							</p>
							<p class="text-white">
								<?= isset($about['description']) ? $about['description'] : ''; ?>
							</p>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4">
							<p class="text-big text-white text-ubold">Контакты</p>
							<div class="row row-15 justify-content-sm-center">
								<div class="col-md-12">
									<ul class="list-style-none text-extra-small text-lists-footer contact-info-address">
										<li>
											<a class="text-white" href="//goo.gl/maps/L6rLSAboGwX1mAsj7" target="_blank">
										<span class="unit flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-2x fa-globe maps-color" aria-hidden="true"></i>
											</span>
											<span class="unit-body">
												<span>CL ACEBO S/N, URB. MYRAMAR DEL SOL Bloq.3, Esc.C, 1B, 29649 MIJAS, MALAGA, Spain
												</span>
											</span>
										</span>
											</a>
										</li>
										<li>
											<a class="text-white" href="tel:+34603306511">
										<span class="unit align-items-center flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-3x fa-mobile" aria-hidden="true"></i>
											</span>
											<span
													class="unit-body">
												<span>+34 603-306-511</span>
											</span>
										</span>
											</a>
										</li>
										<li>
											<a class="text-white" href="mailto:al-andalus@yandex.ru">
										<span class="unit align-items-center flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-2x fa-envelope-square" aria-hidden="true"></i>
											</span>
											<span class="unit-body">
												<span>al-andalus@yandex.ru</span>
											</span>
										</span>
											</a>
										</li>
										<li>
											<a class="text-white"
											   href="//www.youtube.com/channel/UC4Jr_4GJVFLyZsvk556wmIg/about"
											   target="_blank">
										<span class="unit align-items-center flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-2x fa-youtube-play youtube-play-color"
												   aria-hidden="true"></i>
											</span>
											<span class="unit-body">
												<span>Наш канал YouTube</span>
											</span>
										</span>
											</a>
										</li>
									</ul>

									<?php /*<p class="d-block text-small contact-info-address">
										<a class="text-white" href="//goo.gl/maps/L6rLSAboGwX1mAsj7" target="_blank">
										<span class="unit flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-2x fa-globe maps-color" aria-hidden="true"></i>
											</span>
											<span class="unit-body">
												<span>CL ACEBO S/N, URB. MYRAMAR DEL SOL Bloq.3, Esc.C, 1B, 29649 MIJAS, MALAGA, Spain
												</span>
											</span>
										</span>
										</a>
										<a class="text-white" href="tel:+34603306511">
										<span class="unit align-items-center flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-3x fa-mobile" aria-hidden="true"></i>
											</span>
											<span
													class="unit-body">
												<span>+34 603-306-511</span>
											</span>
										</span>
										</a>
										<a class="text-white" href="mailto:al-andalus@yandex.ru">
										<span class="unit align-items-center flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-2x fa-envelope-square" aria-hidden="true"></i>
											</span>
											<span class="unit-body">
												<span>al-andalus@yandex.ru</span>
											</span>
										</span>
										</a>
										<a class="text-white"
										   href="//www.youtube.com/channel/UC4Jr_4GJVFLyZsvk556wmIg/about"
										   target="_blank">
										<span class="unit align-items-center flex-row unit-spacing-xs">
											<span class="unit-left">
												<i class="fa fa-2x fa-youtube-play youtube-play-color"
												   aria-hidden="true"></i>
											</span>
											<span class="unit-body">
												<span>Наш канал YouTube</span>
											</span>
										</span>
										</a>
									</p>*/?>
								</div>
							</div>
							<address class="contact-info text-left">
							</address>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-top-0 justify-content-sm-center justify-content-md-between">
				<div class="col-sm-12 col-md-12 col-lg-12 order-lg-3">
					<p class="text-big text-white text-ubold">
						Позвонить +34 603 306 511 или написать нам
					</p>
					<div class="row row-0 justify-content-sm-center justify-content-md-between">
						<div class="col-3 col-sm-3 col-md-3 col-lg-3">
							<a class="button button-block button-primary button-messagengers" href="tel:+34603306511">
								<i class="fa fa-phone icon-messagers-block-custom icon-messagers-phone" aria-hidden="true"></i>
								<span class="hidden-xs hidden-sm">Телефон</span>
							</a>
						</div>
						<div class="col-3 col-sm-3 col-md-3 col-lg-3">
							<a class="button button-block button-primary button-messagengers" href="tg://+34603306511">
								<img class="icon-messagers-block-custom"
									 src="/images/icons/icon-telegram-2.png" />
								<span class="hidden-xs hidden-sm">Telegram</span>
							</a>
						</div>
						<div class="col-3 col-sm-3 col-md-3 col-lg-3">
							<a class="button button-block button-primary button-messagengers"
							   href="whatsapp://+34603306511">
								<img class="icon-messagers-block-custom"
									 src="/images/icons/icon-whatsapp-2.png" />
								<span class="hidden-xs hidden-sm">WhatsApp</span>
							</a>
						</div>
						<div class="col-3 col-sm-3 col-md-3 col-lg-3">
							<a class="button button-block button-primary button-messagengers"
							   href="viber://add?number=34603306511">
								<img class="icon-messagers-block-custom"
									 src="/images/icons/icon-viber-2.png">
								<span class="hidden-xs hidden-sm">Viber</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<hr class="">
			<div class="row row-20 justify-content-sm-center justify-content-md-between">
				<div class="col-md-12 col-lg-12 text-md-center">
					<p class="text-small text-white">
						Недвижимость и отдых в Испании
						<a href="<?= Url::to(['/']) ?>"> Alandalus.ru</a>. &#169;
						<span class="copyright-year"></span> Все права защищены.
					<br />
						Не санкционированное использование материалов запрещено.
					</p>
				</div>
				<div class="col-md-5 col-lg-3 text-md-right"></div>
			</div>
		</div>
	</div>
</footer>




