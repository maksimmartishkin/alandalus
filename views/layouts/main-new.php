<?php

/* @var $this \yii\web\View */

/* @var $content string */

//use app\widgets\Alert;
use app\assets\AppAsset;
use yii\helpers\Html;

//use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="preloader">
	<div class="preloader-body">
		<div class="cssload-container">
			<div class="cssload-speeding-wheel"></div>
		</div>
		<p>Загрузка...</p>
	</div>
</div>


<div class="page text-center">
	<header class="page-header slider-menu-position">
		<div class="rd-navbar-wrap">
			<?= Yii::$app->controller->renderPartial("@app/views/layouts/navbar-menu", []); ?>
		</div>
	</header>

	<?php
	switch (Yii::$app->controller->action->id) :
		case 'index':
			echo $content;
			break;
		case 'yachts-rental-window2':
		case 'contact':
		case 'error':
//		case 'index':
			?>
			<section class="section parallax-container bg-black section-height-mac"
					 data-parallax-img="/images/background/shapka_n.png">
				<div class="parallax-content">
					<div class="bg-overlay-darker">
						<div class="container section-34 section-md-60 section-lg-115">
							<h1 class="d-none d-lg-inline-block text-white"><?php /*echo $this->title */ ?></h1>
						</div>
					</div>
				</div>
			</section>
			<?php
			echo $content;
			break;
		default:
			?>
			<section class="section parallax-container bg-black section-height-mac"
					 data-parallax-img="/images/background/shapka_n.png">
				<div class="parallax-content">
					<div class="bg-overlay-darker">
						<div class="container section-34 section-md-60 section-lg-115">
							<h1 class="d-none d-lg-inline-block text-white"><?php /*echo $this->title */ ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section class="section-top-100 bg-wild-wand text-md-left">
				<div class="container">
					<div class="row justify-content-sm-center row-50" style="display: block">
						<div class="col-md-11 col-lg-3 text-lg-left">
							<?php echo Yii::$app->controller->renderPartial(
								"@app/views/site/aside-menu", []
							);
							?>
						</div>
						<div class="col-md-11 col-lg-9 order-lg-1">
							<?= $content ?>
						</div>

					</div>
				</div>
			</section>

		<?php endswitch; ?>

	<?= Yii::$app->controller->renderPartial("@app/views/layouts/footer", []); ?>

</div>

<script src="//www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
    _uacct = "UA-29470616-1"
    urchinTracker();
</script>

<!-- Yandex.Metrika counter -->
<div style="display:none;">
	<script type="text/javascript" defer="defer" async>
        (function (w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter12870595 = new Ya.Metrika({id: 12870595, enableAll: true, webvisor: true});
                } catch (e) {
                }
            });
        })(window, "yandex_metrika_callbacks");
	</script>
</div>
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer" async></script>
<noscript>
	<div><img src="//mc.yandex.ru/watch/12870595" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global Mailform Output-->
<!--<div class="snackbars" id="form-output-global"> </div>-->
<!-- Java script-->

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript' defer async>
    (function () {
        var widget_id = '131185';
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.defer = true;
        s.src = '//code.jivosite.com/script/widget/' + widget_id;
        var ss = document.getElementsByTagName('script')[0];
        ss.parentNode.insertBefore(s, ss);
    })();</script>
<!-- {/literal} END JIVOSITE CODE -->
<?php
$this->registerJs(<<<JS
$('img.lazy').lazyload({"failurelimit":10,"effect":"fadeIn"});
JS
);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
