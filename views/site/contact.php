<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//use yii\captcha\Captcha;
use developit\captcha\Captcha;
use app\models\SiteContent;

if ($aboutCache = Yii::$app->cache->get('about_text')) {
	$about = $aboutCache;
} else {
	$about = SiteContent::getAbout();
	Yii::$app->cache->set('about_text', $about);
}

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="section-80">
	<div class="container">
		<div class="row row-50 justify-content-sm-center justify-content-lg-between text-md-left">
			<div class="col-md-10 col-lg-5">
				<h5 class="text-ubold">О Компании</h5>
				<?= isset($about['content']) ? $about['content'] : ''?>
			</div>
			<div class="col-md-10 col-lg-7 col-xl-3">
				<h5 class="text-ubold">Контакты</h5>
				<address class="contact-info text-left">
					<p class="d-block contact-info-address"><a class="text-gray" href="#">
							<span class="unit flex-row unit-spacing-xs">
								<span class="unit-left">
									<img class="img-responsive center-block"
										 src="/images/icons/icon-01-16x21.png"
										 width="16" height="21" alt="Адрес в Испании"
										 title="Адрес в Испании">
								</span>
								<span class="unit-body">
									<span>
										CL ACEBO S/N, URB. MYRAMAR DEL SOL Bloq.3, Esc.C, 1B, 29649 MIJAS, MALAGA, Spain
									</span>
								</span>
							</span>
						</a>
					</p>
					<p class="d-block">
						<a class="text-gray" href="tel:#">
							<span class="unit align-items-center flex-row unit-spacing-xs">
								<span class="unit-left">
									<img class="img-responsive center-block"
										 src="/images/icons/icon-02-19x19.png" width="19"
										 height="19" alt="Телефон в Испании"
										 title="Телефон в Испании">
								</span>
								<span class="unit-body">
									<span>
										+34 603 306 511 (доступен также в Viber и WatsUp)
									</span>
								</span>
							</span>
						</a>
					</p>
					<p class="d-block">
						<a class="text-gray" href="mailto:#">
							<span class="unit align-items-center flex-row unit-spacing-xs">
								<span class="unit-left">
									<img class="img-responsive center-block"
										 src="/images/icons/icon-04-20x13.png" width="20"
										 height="13" alt="Электронная почта"
										 title="Электронная почта">
								</span>
								<span class="unit-body">
									<span>al-andalus@yandex.ru</span>
								</span>
							</span>
						</a>
					</p>
					<p class="d-block">
						<a class="text-gray" href="tel:#">
							<span class="unit align-items-center flex-row unit-spacing-xs">
								<span class="unit-left">
									<strong>NIF (ИНН):</strong>
								</span>
								<span class="unit-body">
									<span>B93235976</span>
								</span>
							</span>
						</a>
					</p>
					<?php /*<p class="d-block text-small"><a class="text-gray" href="#"><span class="unit align-items-center flex-row unit-spacing-xs"><span class="unit-left"><img class="img-responsive center-block" src="images/icons/icon-05-19x19.png" width="19" height="19" alt=""></span><span class="unit-body"><span>demolink.org</span></span></span></a></p>*/ ?>
				</address>
			</div>
			<div class="col-md-10 col-lg-7 col-xl-3">
				<h5 class="text-ubold text-center">Обратная связь</h5>
				<?php $form = ActiveForm::begin([
					'id' => 'contact-form',
					'enableClientValidation' => true,
					'options' => [],
				]); ?>

				<?= $form->field($model, 'name', [
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->textInput(
					[
						'class' => 'form-input'
					]
				)->label($model->attributeLabels()['name'],
					[
						'class' => 'form-label'
					]
				); ?>

				<?= $form->field($model, 'email', [
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->input('email',
					[
						'class' => 'form-input'
					]
				)->label($model->attributeLabels()['email'],
					[
						'class' => 'form-label'
					]
				); ?>

				<? //= $form->field($model, 'subject')->hiddenInput(['value' => 'form-input']); ?>

				<?= $form->field($model, 'phone', [
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->textInput(
					[
						'class' => 'form-input'
					]
				)->label($model->attributeLabels()['phone'],
					[
						'class' => 'form-label'
					]
				); ?>

				<?= $form->field($model, 'body', [
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->textarea(
					[
						'rows' => 6,
						'class' => 'form-input',
						'style' => 'height:120px;'
					]
				)->label($model->attributeLabels()['body'],
					[
						'class' => 'form-label'
					]
				); ?>

				<?= $form->field($model, 'reCaptchav2')->widget(
					\himiklab\yii2\recaptcha\ReCaptcha::className(),
					[
						'siteKey' => RECAPTCHA_SITE_KEY_V2
					]
				)->label(false); ?>


				<div class="form-button text-center text-md-center">
					<?= Html::submitButton('Отправить',
						[
							'class' => 'button button-width-110 button-primary',
							'name' => 'contact-button'
						]
					) ?>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</section>