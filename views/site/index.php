<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Alandalus.ru - все для жизни и отдыха в Испании';
$id_cache = 'alandalus_main_index';
$this->registerMetaTag([
	'name' => 'keyword',
	'content' => 'Испания,alandalus,услуги,сайт о Испании',
]);

$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Alandalus.ru - это портал с услугами и информацией об Испании. На нашем сайте вы можете купить или арендовать недвижимость, оформить прокат автомобиля или яхты, заказать индивидуальную экскурсию, подготовку свадьбы или корпоратива, а также многое другое.',
]);
?>
<?php //if ($this->beginCache($id_cache, ['duration' => defined('CACHE_DURATION') ? CACHE_DURATION : 86400])) : ?>
<?php
if (!empty($content['content'])) :
	$id_cache = Url::to() . 'content';
	if ($this->beginCache($id_cache, ['duration' => defined('CACHE_DURATION') ? CACHE_DURATION : 86400])) {
		echo $content['content'];
		$this->endCache();
	}
endif;
?>
<?php
//$this->endCache();
//endif;
?>
