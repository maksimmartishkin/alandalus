<?php

$reviews = !empty($reviews) ? $reviews : [];
?>

<!-- In the Socials-->
<section class="section-70 section-md-bottom-80">
	<div class="container">
		<div class="row row-60 justify-content-sm-center text-left">
			<div class="col-md-12">
				<?php foreach ($reviews as $review) : ?>
				<div class="box-socials unit flex-sm-row unit-spacing-md">
					<div class="unit-left"><img class="rounded-circle img-responsive img-responsive center-block" src="/images/icons/face-review.svg" width="70" height="70" alt=""></div>
					<div class="unit-body">
						<p class="text-ubold text-spacing-200 text-black text-uppercase text-small">
							<?= !empty($review->name) ? $review->name : ''; ?>
						</p>
						<p class="text-small font-italic text-silver-chalice">
							<?= !empty($review->body) ? $review->body : ''; ?>
						</p>
					</div>
				</div>
				<hr class="hr bg-gallery">
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>
