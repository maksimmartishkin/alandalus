<?php


$this->registerCss(<<<CSS
	.gmap {
		display: none;
		width: 100%;
		height: 405px;
		padding: 5px;
		box-shadow: inset 0.5px 0.5px 0.5px 0.5px #ab8b6a;
		background: none repeat scroll 0 0 rgba(197, 177, 144, 0.3);
		border: 1px ridge #80593a;
	}
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
CSS
);
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?apikey=4a83c893-1f10-47c9-8b1a-f3fe58377770&lang=ru_RU', ['depends' => \yii\web\YiiAsset::className()]);

?>

<?php $this->registerJs(<<<JS
	let lat = $(".gmap").find('span').attr('data-lat');
	let lng = $(".gmap").find('span').attr('data-lng');
	$('.gmap').before('<div id="map" style="width: 100%; height: 400px"></div>');
	var zoom = 12;
	if($(".gmap").find('span').attr('zoom')){
	   zoom = parseInt($(".gmap").find('span').attr('zoom'));
	}
	
	ymaps.ready(init);
	function init(){
		var myMap = new ymaps.Map("map", {
			center: [lat, lng],
			zoom: zoom
		});
	}
JS
); ?>

