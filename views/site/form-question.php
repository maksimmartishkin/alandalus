<?php

use app\models\ContactForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$model = new ContactForm();
$model->subject = isset($form_subject) ? $form_subject : '';
if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
	Yii::$app->session->setFlash('contactFormSubmitted');

	return Yii::$app->getResponse()->refresh();
}

?>
<div class="row">
	<div class="col-lg-12">
		<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

			<div class="alert alert-success">
				Ваша заявка отправлена. В ближайшее время с Вами свяжутся.
			</div>

		<?php else: ?>
			<div class="form-question-block">
				<h5 class="text-ubold text-center">Обратная связь</h5>
				<?php $form = ActiveForm::begin([
					'id' => 'contact-form',
					'enableClientValidation' => true,
				]); ?>

				<?= $form->field($model, 'email', [
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->input('email',
					[
						'class' => 'form-input'
					]
				)->label($model->attributeLabels()['email'],
					[
						'class' => 'form-label'
					]
				); ?>

				<? //= $form->field($model, 'subject')->hiddenInput(['value' => 'form-input']); ?>

				<?= $form->field($model, 'phone', [
					'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
				])->textInput(
					[
						'class' => 'form-input'
					]
				)->label($model->attributeLabels()['phone'],
					[
						'class' => 'form-label'
					]
				); ?>


				<?= $form->field($model, 'reCaptchav2')->widget(
					\himiklab\yii2\recaptcha\ReCaptcha::className(),
					[
						'siteKey' => RECAPTCHA_SITE_KEY_V2
					]
				)->label(false); ?>

				<div class="form-button text-center text-md-center">
					<?= Html::submitButton('Отправить',
						[
							'class' => 'button button-width-110 button-primary',
							'name' => 'contact-button'
						]
					) ?>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		<?php endif; ?>
	</div>
</div>

