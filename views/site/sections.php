<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = !empty($content) && !empty($content['longtitle']) ? $content['longtitle'] : '';

$this->registerMetaTag([
	'name' => 'keyword',
	'content' => !empty($content) && !empty($content['introtext']) ? $content['introtext'] : ''
]);

$this->registerMetaTag([
	'name' => 'description',
	'content' => !empty($content) && !empty($content['description']) ? $content['description'] : ''
]);


$isFormQuestionNedvizimost = false;
$map_lat = false;
$map_long = false;
$location_detail_baza = false;
?>
<?php $this->registerCss(<<<CSS
	#thumbs { padding-top: 10px; overflow: hidden; }
	#thumbs img, #largeImage {
		border: 1px solid gray;
		padding: 4px;
		background-color: white;
		cursor: pointer;
	}
	#thumbs img {
		float: left;
		margin-right: 6px;
		width: 100px;
	}
	#description {
		background: black;
		color: white;
		position: absolute;
		bottom: 0;
		padding: 10px 20px;
		width: 525px;
		margin: 5px;
	}
	#panel { position: relative; }
CSS
);
?>
	<div class="box box-lg box-single-post bg-default d-block">
		<?php /*= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				]) */ ?>
		<?php if (isset($content['id']) && !Yii::$app->user->isGuest) : ?>
			<?= Html::a('Редактировать страницу',
				[
					Url::to([
							'/admin/content/update-site'
						]
					),
					'id' => $content['id']
				],
				[
					'class' => 'button button-primary editable-content'
				]
			) ?>
		<?php endif; ?>
		<h1 class="text-ubold"><?= $this->title ?></h1>
		<div class="text-silver-chalice text-justify">
			<?php if (!empty($table_nedvizimost)) : ?>
				<?php
				$map_lat = !empty($table_nedvizimost['map_lat']) ? $table_nedvizimost['map_lat'] : false;
				$map_long = !empty($table_nedvizimost['map_long']) ? $table_nedvizimost['map_long'] : false;
				$location_detail_baza = !empty($table_nedvizimost['location_detail_baza']) ? $table_nedvizimost['location_detail_baza'] : '';
				$isFormQuestionNedvizimost = true;
				?>
				<?php echo Yii::$app->controller->renderPartial("table_nedvizimost", ['table_nedvizimost' => $table_nedvizimost]); ?>
			<?php endif; ?>
			<?php
			if (!empty($content['content'])) :
				$id_cache = Url::to() . 'content';
				if ($this->beginCache($id_cache, ['duration' => defined('CACHE_DURATION') ? CACHE_DURATION : 86400])) {
					echo $content['content'];
					$this->endCache();
				}
			endif;
			?>
			<?php if ($map_lat && $map_long) : ?>
				<ul class="gmap" data-lat="<?= $map_lat ?>" data-lng="<?= $map_long ?>">
					<li data-lat="<?= $map_lat ?>" data-lng="<?= $map_long ?>"><?= $location_detail_baza ?></li>
				</ul>
				<?php echo Yii::$app->controller->renderPartial("@app/views/main/googleMapsScript"); ?>
			<?php endif; ?>
			<?php if ($isFormQuestionNedvizimost) : ?>
				<?php echo Yii::$app->controller->renderPartial("form-question",
					[
						'form_subject' => $this->title
					]
				); ?>
			<?php endif; ?>


		</div>
		<hr class="hr bg-gallery">


		<div class="d-inline-block inset-xs-center-12">
			<h3>Понравилось? Поделись с друзьями!</h3>
			<div class="share42init" data-url="<?= Url::base(true) ?><?= Url::to() ?>"
				 data-title="<?= $this->title ?>"></div>
		</div>
	</div>
<?php /*$this->registerJs(<<<JS
	$('#thumbs').delegate('img','click', function(){
		$('#largeImage').attr('src',$(this).attr('src').replace('thumb','large'));
		$('#description').html($(this).attr('alt'));
	});

	var buttonFormsModal =  $('.modal-forms-button');
	var navbarHeightTop = Math.floor($('.rd-navbar').height());
	var buttonFormsModalHeightTop =  buttonFormsModal.offset().top;
	
	$(window).scroll(function(){
		var scrollTop = $(document).scrollTop();
		if ((scrollTop + (navbarHeightTop - 100)) > buttonFormsModalHeightTop) {
			buttonFormsModal.addClass('button-fixed'); 
			buttonFormsModal.removeClass('button-no-fixed'); 
		} else {
			buttonFormsModal.removeClass('button-fixed'); 
			buttonFormsModal.addClass('button-no-fixed'); 
		}
	});
JS
); */?>