<?php

$idCacheAssets = 'swiper-slide-main-assets';
$idCacheAssets = 'swiper-slide-main-imaget';
$dirSliders = Yii::getAlias('@webroot') . '/assets/images/main-page-slider';
$dirAssets = '/assets/images/main-page-slider/';

$files = [];
if (($swiperSliderCacheAssets = Yii::$app->cache->get($idCacheAssets)) || ($swiperSliderCacheImages = Yii::$app->cache->get($idCacheAssets))) {
	$files = $swiperSliderCacheAssets ? $swiperSliderCacheAssets : ($swiperSliderCacheImages ? $swiperSliderCacheImages : []);
} else {
	if (is_dir($dirSliders)) {
		$filesInDir = scandir($dirSliders);
		foreach ($filesInDir as $file) {
			if ($file == '.' || $file == '..') {
				continue;
			}

			$files[] = $dirAssets . $file;
			Yii::$app->cache->set($idCacheAssets, $files);
		}
	} else {
		$files = [
			'/images/background/5.jpg',
			'/images/background/6.jpg',
			'/images/background/7.jpg',
			'/images/background/8.jpg',
			'/images/background/9.jpg',
			'/images/background/10.jpg',
			'/images/background/11.jpg',
			'/images/background/12.jpg',
			'/images/background/13.jpg',
			'/images/background/14.jpg',
		];
		Yii::$app->cache->set($idCacheAssets, $files);
	}
}

if (empty($files)) {
	return false;
}

$filesRand = array_rand($files, 5);
?>

<?php if ($this->beginCache('swiper-container-slider', ['duration' => 3600])) : ?>

	<section class="swiper-container swiper-slider" data-height="" data-min-height="400px" data-simulate-touch="false"
			 data-slide-effect="fade">
		<div class="swiper-wrapper">
			<?php if (!empty($filesRand) && is_array($filesRand)) : ?>
				<?php foreach ($filesRand as $fileKey) : ?>
					<?php if (!empty($fileKey)) : ?>
						<div class="swiper-slide" data-slide-bg="<?= $files[$fileKey] ?>"></div>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<div class="swiper-caption-absolute">
			<div class="container">
				<div class="row justify-content-sm-center">
					<div class="col-xl-10">
						<h1 class="text-white"><?php /*Enjoy Your Dream Vacation*/ ?></h1>
						<p class="h6 text-white"><?php /*Travel to the any corner of the world, without going around in circles.*/ ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-prev">
			<span class="icon icon-xxs icon-circle icon-filled-white mdi mdi-chevron-left text-gray"></span>
		</div>
		<div class="swiper-button-next">
			<span class="icon icon-xxs icon-circle icon-filled-white mdi mdi-chevron-right text-gray"></span>
		</div>
	</section>

	<?php
	$this->endCache();
endif;
?>
