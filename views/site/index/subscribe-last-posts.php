<?php

use yii\bootstrap\ActiveForm;

//$model = !empty($model) ? $model : new LastPublForm();

?>

<section class="section parallax-container bg-black wow fadeIn section-block-subscribe-posts"
		 data-parallax-img="<?= !empty($dataBlocks['images']) ? $dataBlocks['images'] : '/images/backgrounds/background-41-970x528.png' ?>"
		 data-wow-delay=".2s">
	<div class="parallax-content">
		<div class="">
			<div class="container section-80 section-md-top-70 container-block-subscribe-posts">
				<div class="row row-30 row-sm justify-content-sm-center justify-content-lg-start text-sm-left">
					<div class="col-md-12 col-lg-12">
						<div class="box d-lg-block bg-default box-block-subscribe-posts">
							<div class="unit flex-column flex-sm-row flex-lg-column unit-spacing-box-icon">
								<div class="unit-body">
									<div class="row row-15 justify-content-sm-center align-items-sm-center">
										<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
											<div class="col-md-12 col-lg-12 text-lg-center col-custom">
												<p class="text-block-title-subscribe-posts text-black">
													<?= !empty($dataBlocks['title']) ? $dataBlocks['title'] : 'Подпишитесь на нас' ?>
												</p>
												<p class="text-block-extra-subscribe-posts text-spacing-500">
													<?= !empty($dataBlocks['text']) ? $dataBlocks['text'] : 'Подпишитесь на нашу рассылку и всегда узнавайте первые все новости и акции' ?>
												</p>
											</div>
											<div class="col-md-12 col-lg-12">
												<div class="alert alert-success">
													Вы успешно подписались.
												</div>
											</div>
										<?php else: ?>
											<?php if (!empty($model)) : ?>
												<?php $form = ActiveForm::begin([
													'id' => 'contact-form',
													'enableClientValidation' => true,
												]); ?>
												<div class="col-md-12 col-lg-12 text-lg-center col-custom">
													<p class="text-block-title-subscribe-posts text-black">
														<?= !empty($dataBlocks['title']) ? $dataBlocks['title'] : 'Подпишитесь на нас' ?>
													</p>
													<p class="text-block-extra-subscribe-posts text-spacing-500">
														<?= !empty($dataBlocks['text']) ? $dataBlocks['text'] : 'Подпишитесь на нашу рассылку и всегда узнавайте первые все новости и акции' ?>
													</p>
												</div>
												<div class="col-md-12 col-lg-12">
													<div class="form-wrap form-wrap-xs form-offset-bottom-none">
														<?= $form->field($model, 'email', [
															'template' => '
															<div class="form-wrap form-wrap-xs">
																{label}
																{input}
																<span class="form-validation">{error}</span>
															</div>
														'
														])->input('email', [
															'class' => 'form-input form-input-custom form-control',
														])->label($model->attributeLabels()['email'], [
															'class' => 'form-label'
														]); ?>
													</div>
												</div>
												<div class="col-md-12 col-lg-12">
													<button class="button button-block button-primary"
															type="submit">
														Подписаться
													</button>
												</div>
												<?php ActiveForm::end(); ?>
											<?php endif; ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

