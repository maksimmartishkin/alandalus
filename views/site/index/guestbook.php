<?php

use yii\helpers\Url;

?>
<!--backgroun-guestbook.png-->

<h1 class="header-reviews hidden-md hidden-lg"><?= !empty($blocksData['title']) ? $blocksData['title'] : '' ?></h1>
<h3><?= !empty($blocksData['texts']) ? $blocksData['texts'] : '' ?></h3>
<section class="parallax-container section-md-bottom-100 section-block-guestbook-posts"
		 data-parallax-img="<?= !empty($blocksData['images']) ? $blocksData['images'] : '/images/background/background-05-1920x900.jpg' ?>">
	<div class="container">
		<div class="row justify-content-sm-left">
			<div class="col-lg-6 col-xl-6 block-guestbook-posts">
				<div class="row">
					<div class="col-8 col-lg-8 col-xl-8 text-left">
						<h1 class="header-reviews hidden-xs hidden-sm"><?= !empty($blocksData['title']) ? $blocksData['title'] : '' ?></h1>
						<h3><?= !empty($blocksData['texts']) ? $blocksData['texts'] : '' ?></h3>
					</div>
					<div class="col-md-3 col-lg-3 col-xl-3 text-right block-button-guestbook hidden-xs hidden-sm">
						<a class="button button-default" href="<?= Url::to(['/guestbook']); ?>">Все отзывы</a>
					</div>
					<div class="col-12">
						<?php if (!empty($reviews)) : ?>
							<div class="owl-carousel owl-dots-white owl-navs-white owl-carusel-inset-left-right owl-dots-lg-reveal owl-navs-lg-veil"
								 data-items="1" data-md-items="1" data-xl-items="1"
								 data-stage-padding="5" data-loop="false" data-margin="30" data-mouse-drag="false"
								 data-dots="true" data-nav="true">
								<?php foreach ($reviews as $review) : ?>
									<blockquote class="quote quote-boxed">
										<div class="quote-boxed-img-wrap img-face-review-wrap">
											<img class="rounded-circle img-responsive" src="/images/icons/face-review.svg"
												 width="60" height="60" alt="">
										</div>
										<div class="quote-boxed-body">
											<h6 class="text-small text-ubold text-spacing-200 text-left">
												<?= isset($review->name) ? $review->name : '' ?>
											</h6>
											<p class="text-small text-silver-chalice">
												<?php if (isset($review->name)) : ?>
													<q><?= mb_substr($review->body, 0, 140, 'UTF-8'); ?>
														<?= (iconv_strlen($review->body) > 140) ? '...' : ''; ?>
													</q>
												<?php endif; ?>
											</p>
											<button class="button button-primary button-reviews" type="button"
													data-toggle="modal"
													data-target="#reviewBody<?= isset($review->id) ? $review->id : '' ?>">
												Подробнее
											</button>
										</div>
									</blockquote>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-12 block-button-guestbook hidden-md hidden-lg">
						<a class="button button-default" href="<?= Url::to(['/guestbook']); ?>">Все отзывы</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if (!empty($reviews)) : ?>
	<?php foreach ($reviews as $review) : ?>
		<div class="modal modal-custom modal-team-member fade text-md-left"
			 id="reviewBody<?= isset($review->id) ? $review->id : '' ?>" tabindex="-1"
			 role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span></button>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row row-30 justify-content-sm-center align-items-sm-center">
								<div class="col-md-2">
									<div class="bg-image-2">
										<img
												class="rounded-circle img-responsive center-block"
												src="/images/icons/face-review.svg"
												width="100" height="100" alt="">
									</div>
								</div>
								<div class="col-md-10">
									<div class="modal-body-column-content">
										<div class="team-member">
											<div class="team-member-img-wrap d-md-none">
											</div>
											<div class="team-member-title text-small text-ubold text-uppercase text-spacing-200 text-black"><?= isset($review->name) ? $review->name : '' ?></div>
											<div class="team-member-scroll-section">
												<p class="text-small font-italic text-silver-chalice text-left">
													<?= isset($review->name) ? $review->body : ''; ?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>
