<?php

use yii\helpers\Url;
use app\models\LastPublPosts;

$this->registerJs('var get_last_posts_request = "' . Url::to(['/get-last-posts-request']) . '";', \yii\web\View::POS_HEAD);

?>


<?php if (!empty($lastPosts)) : ?>
	<section class="section-70 section-md-bottom-80 wow fadeIn block-last-publ-posts">
		<div class="container">
			<div class="row row-50 justify-content-sm-center">
				<h1 class="header-last-publ-posts"><?= !empty($blocksData['titleBlock']) ? $blocksData['titleBlock'] : 'Последние публикации'; ?></h1>
				<div class="col-md-12 col-lg-12 js-block-last-publication">
					<div class="row row-30 row-offset-1 justify-content-sm-center justify-content-md-between">
						<?php foreach ($lastPosts as $key => $lastPost) : ?>
							<?php if ($key == 0) : ?>
								<div class="col-sm-12 col-md-12 col-xl-12">
									<section class="section parallax-container bg-black wow fadeIn section-block-publ-posts
									<?php if (!empty($request)) : ?>
			section-block-publ-posts-<?= !empty($lastPost->images) ? md5($lastPost->images) : '' ?>
									<?php endif; ?>
					"
											 data-parallax-img="<?= !empty($lastPost->images) ? $lastPost->images : '' ?>"
											 data-wow-delay=".2s">
										<?php if (!empty($request)) : ?>
										<style>
											@media (max-width: 768px) {
                                                .section-block-publ-posts-<?= !empty($lastPost->images) ? md5($lastPost->images) : '' ?> {
                                                    background-image: url('<?= !empty($lastPost->images) ? $lastPost->images : '' ?>') !important;
                                                }
                                            }
										</style>
											<div class="material-parallax parallax">
												<img src="<?= !empty($lastPost->images) ? $lastPost->images : '' ?>" alt="" style="display: block; transform: translate3d(-50%, 0px, 0px);">
											</div>
										<?php endif; ?>
										<div class="parallax-content">
											<div class="">
												<div class="container section-80 section-md-top-70 container-block-publ-posts">
													<div class="row row-30 row-sm justify-content-sm-center justify-content-lg-start text-sm-left">
														<div class="col-md-12 col-lg-12">
															<div class="box d-lg-block bg-default box-block-publ-posts">
																<div class="post-box-title h5">
																	<?= !empty($lastPost->title) ? $lastPost->title : '' ?>
																</div>
																<p class="text-small text-silver-chalice">
																	<?= !empty($lastPost->content) ? LastPublPosts::cutPublPost($lastPost->content, 350) : '' ?>
																</p>
																<a class="last_publ_post_more"
																   href="<?= !empty($lastPost->uri) ? $lastPost->uri : '' ?>">
																	<i class="fa fa-bars" aria-hidden="true"></i>&nbsp;
																	Читать
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
							<?php else : ?>
								<div class="col-sm-12 col-md-12 col-xl-12">
									<!-- Post Box-->
									<div class="post-box post-box-wide publ-last-post-box post-blog-left text-left">
										<div class="post-box-img-wrap">
											<img src="<?= !empty($lastPost->images) ? $lastPost->images : '' ?>"
												 width="270" height="315" alt="" />
										</div>
										<div class="post-box-caption">
											<div class="post-box-title h5">
												<?= !empty($lastPost->title) ? $lastPost->title : '' ?>
											</div>
											<div class="row row-top-0">
												<div class="col-12">
													<?= !empty($lastPost->content) ? LastPublPosts::cutPublPost($lastPost->content, 300) : '' ?>
												</div>
												<div class="col-12">
													<a class="last_publ_post_more" href="<?= !empty($lastPost->uri) ? $lastPost->uri : '' ?>">
														<i class="fa fa-bars" aria-hidden="true"></i>&nbsp;
														Читать
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-md-12 col-lg-12 js-spinner-last-publication last-publication-hidden">
					<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>
				</div>
				<div class="col-sm-12 col-md-12 col-xl-12 block-last-publication-button">
					<button class="js-get-last-publication-posts button-last-publication-posts">
						<span class="js-last-publication-offset last-publication-hidden">4</span>
						Больше статей <img src="/images/icons/arrow.png" />
					</button>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>