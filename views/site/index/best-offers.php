<?php

$fadeEffect = [0 => 'fadeInLeft', 1 => 'bounceIn', 2 => 'fadeInRight'];
$countStaticOffers = 6;
?>


<section class="section-70 section-md-bottom-80 wow fadeIn">
	<div class="container">
		<div class="row row-30 section-50 justify-content-sm-center">
			<h1 class="header-best-offers"><?= isset($blocksData['titleBlock']) ? $blocksData['titleBlock'] : '10 лет мы предоставляем услуги по организации отдыха в Испании'; ?></h1>
			<h5 class="text-best-offers"><?= !empty($blocksData['textBlock']) ? $blocksData['textBlock'] : 'Наши лучшие предложения'; ?></h5>
			<?php if (!empty($blocksData['titles']) && !empty($blocksData['texts']) && !empty($blocksData['images']) && !empty($blocksData['links'])) : ?>
				<?php $countBlockOffers = count(array_filter($blocksData['titles'])); ?>
				<?php for ($i = 0; $i < $countBlockOffers; $i++) : ?>
				<?php
					$classBlockOffersHidden = '';
					if ($i >= $countStaticOffers) {
						$classBlockOffersHidden = 'js-block-offers-hidden hidden';
					}
					?>
					<div class="col-md-5 col-lg-4 <?=$classBlockOffersHidden?>">
						<div class="box-offer wow <?= $fadeEffect[$i % 3] ?>" data-wow-delay=".2s">
							<div class="box-offer-img-wrap">
								<img src="/images/loader-images.gif"
									 data-original="<?= isset($blocksData['images'][$i]) ? $blocksData['images'][$i] : ''; ?>"
									 class="img-responsive center-block lazy" width="370" height="250">
								</a>
							</div>
							<div class="box-offer-caption text-left">
								<div class="pull-left">
									<div class="box-offer-title text-ubold">
										<span class="text-green">
											<?= isset($blocksData['titles'][$i]) ? $blocksData['titles'][$i] : ''; ?>
										</span>
									</div>
								</div>
								<div class="pull-right">
									<div class="box-offer-price text-black"></div>
								</div>
								<div class="clearfix"></div>
								<ul class="list-inline list-inline-13 list-inline-marked text-silver-chalice text-small">
									<li>
										<?= isset($blocksData['texts'][$i]) ? $blocksData['texts'][$i] : ''; ?>
									</li>
								</ul>
							</div>
							<a class="button button-primary button-best-offers-custom"
							   href="<?= isset($blocksData['links'][$i]) ? $blocksData['links'][$i] : ''; ?>">Подробнее</a>
						</div>
					</div>
				<?php endfor; ?>
			<?php endif; ?>
			<div class="col-sm-12 col-md-12 col-xl-12">
				<button class="js-best-offers-again-show button-best-offers-again-show">
					Показать все услуги
				</button>
			</div>
		</div>
</section>

