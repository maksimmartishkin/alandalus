<?php
use yii\helpers\Url;

$idCacheAssets = 'swiper-slide-main-imaget';


if (!empty($files)) :
?>
<div class="swiper-container swiper-slider-container-custom swiper-slider" data-height="" data-min-height="400px" data-simulate-touch="false"
	 data-slide-effect="fade">
	<div class="swiper-wrapper">
		<?php if (!empty($filesRand) && is_array($filesRand)) : ?>
			<?php foreach ($filesRand as $fileKey) : ?>
			<?php //var_dump($fileKey); ?>
				<?php if (!empty($fileKey)) : ?>
					<div class="swiper-slide" data-slide-bg="<?= $files[$fileKey] ?>">
						<div class="swiper-slide-caption">
							<div class="container">
								<div class="row justify-content-sm-center justify-content-lg-start text-lg-left">
									<div class="col-md-10 col-lg-6 col-xl-6 col-xxl-4">
										<div class="inset-sm-left-50 inset-xxl-left-0 section-80 block-swiper-slide-caption-custom">
											<h1 data-caption-animate="fadeInUp" data-caption-delay="100">
												<?=isset($titles[$fileKey]) ? $titles[$fileKey] : ''?>
											</h1>
											<p class="h6" data-caption-animate="fadeInUp"
											   data-caption-delay="400">
												<?= isset($captions[$fileKey]) ? $captions[$fileKey] : '' ?>
											</p>
											<?php if (isset($links[$fileKey])) : ?>
											<a class="button button-primary" href="<?=Url::to([$links[$fileKey]])?>"
													data-caption-animate="fadeInUp" data-caption-delay="450">Узнать подробнее</a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<!-- Swiper Pagination-->
	<div class="swiper-pagination"></div>
	<!-- Swiper Navigation-->
	<div class="swiper-button-prev swiper-button-prev-variant-2 d-none d-sm-inline-block"><span
				class="icon icon-xxs icon-circle icon-filled-white mdi mdi-chevron-left text-gray"></span></div>
	<div class="swiper-button-next swiper-button-next-variant-2 d-none d-sm-inline-block"><span
				class="icon icon-xxs icon-circle icon-filled-white mdi mdi-chevron-right text-gray"></span></div>
</div>
<?php endif; ?>

<?php /*<section class="swiper-container swiper-slider" data-height="" data-min-height="400px"
		 data-simulate-touch="false"
		 data-slide-effect="fade">
	<div class="swiper-wrapper">
		<?php if (!empty($filesRand) && is_array($filesRand)) : ?>
		<?php foreach ($filesRand as $fileKey) : ?>
		<?php if (!empty($fileKey)) : ?>
		<div class="swiper-slide" data-slide-bg="<?= $files[$fileKey] ?>"></div>
		<div class="swiper-slide-caption">
			<div class="container">
				<div class="row justify-content-sm-center justify-content-lg-start text-lg-left">
					<div class="col-md-10 col-lg-8 col-xl-5 col-xxl-4">
						<div class="inset-xl-left-50 inset-xxl-left-0">
							<h1 data-caption-animate="fadeInUp" data-caption-delay="100">Enjoy Japan - from <span
										class="text-primary">$1299</span></h1>
							<p class="h6" data-caption-animate="fadeInUp" data-caption-delay="400">One of the most
								exotic destinations awaits. Find your tour today!</p><a class="button button-primary"
																						href="tours-grid-variant-2.html"
																						data-caption-animate="fadeInUp"
																						data-caption-delay="450">Find
								your perfect tour</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-prev">
			<span class="icon icon-xxs icon-circle icon-filled-white mdi mdi-chevron-left text-gray"></span>
		</div>
		<div class="swiper-button-next">
			<span class="icon icon-xxs icon-circle icon-filled-white mdi mdi-chevron-right text-gray"></span>
		</div>
</section>*/ ?>
