<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;


///echo '<pre>';print_r($host);echo '</pre>';
//foreach()
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
	<?php foreach($urls as $url) : ?>
		<url>
			<loc><?=!empty($host) ? $host : 'https://alandalus.ru'?>/<?= $url->uri?></loc>
			<lastmod><?= $url->lastmod?></lastmod>
		</url>
	<?php endforeach; ?>
</urlset>