<?php

use app\models\SiteContent;
use yii\helpers\Html;
use yii\helpers\Url;

$action_link = explode('/', Url::to());
$active_link = Url::to();

$links = array_filter($action_link);
$razdel1 = array_shift($links);
$razdel2 = array_shift($links);
$razdel3 = array_shift($links);
$razdel4 = array_shift($links);

$currentAction = Yii::$app->controller->action->id;

if ($currentAction === 'search-result' && Yii::$app->request->get('text_request')) {
	if ($sideMenuSearchResultCache = Yii::$app->cache->get($active_link . 'menuSearchResult')) {
		$side_menu = $sideMenuSearchResultCache;
	} else {
		$side_menu = SiteContent::getSideMenuSearchResult();
		Yii::$app->cache->set($active_link . 'menuSearchResult', $side_menu);
	}
} elseif ($currentAction === 'search-baza') {
	if ($sideMenuSearchResultCache = Yii::$app->cache->get($active_link . 'search-baza')) {
		$side_menu = $sideMenuSearchResultCache;
	} else {
		$side_menu = SiteContent::getSideMenuSearchResult();
		Yii::$app->cache->set($active_link . 'search-baza', $side_menu);
	}
} else {

	if ($sideMenuCache = Yii::$app->cache->get($active_link)) {
		$side_menu = $sideMenuCache;
	} else {
		$side_menu = SiteContent::getSideMenu($razdel1, $razdel2, $razdel3, $razdel4);
		Yii::$app->cache->set($active_link, $side_menu);
	}
}

$infoSideMenu = false;
if ($currentAction === 'sections' && strpos($active_link, '/poleznaya-informacziya') === false) {
	if ($sideMenuInfoCache = Yii::$app->cache->get($active_link . 'sideMenuInfoCache')) {
		$infoSideMenu = $sideMenuInfoCache;
	} else {
		$infoSideMenu = SiteContent::getInfoSideMenu();
		Yii::$app->cache->set($active_link . 'sideMenuInfoCache', $infoSideMenu);
	}
}


$id_cache = $active_link;
if ($this->beginCache($id_cache, ['duration' => defined('CACHE_DURATION') ? CACHE_DURATION : 86400])) :
?>

<aside class="blog-aside box box-xs d-block bg-default">
	<?php echo Yii::$app->controller->renderPartial("search-for-site-form",
		[
			'text_request' => Yii::$app->request->get('text_request')
		]
	); ?>
	<?php if (!empty($infoSideMenu)) : ?>
		<?php
		$titleInfoSideMenu = '<i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;<span class="text-info-side-menu">' . (!empty($infoSideMenu['menutitle']) ? $infoSideMenu['menutitle'] : 'Полезная информация') . '</span>';
		$urlInfoSideMenu = !empty($infoSideMenu['uri']) ? $infoSideMenu['uri'] : '#';
		echo Html::a($titleInfoSideMenu, Url::to('/'. $urlInfoSideMenu), ['class' => 'button button-width-110 button-primary button-info-side-menu']);
		?>
	<?php endif; ?>
	<?php if (!empty($razdel1) && ($razdel1 == 'nedvizimost-ispania')) : ?>
		<hr class="hr bg-gallery">
		<?php echo Yii::$app->controller->renderPartial("bazasearch_h"); ?>
	<?php endif; ?>
	<hr class="hr bg-gallery">

	<?php if (!empty($side_menu)) : ?>
		<div onclick="tree_toggle(arguments[0])" class="js-tree-side-menu tree-side-menu">
			<ul class="Container">
				<?php foreach ($side_menu as $sideMenu) : ?>
					<?php
					$activeClassSideMenu = (!empty($sideMenu['link']) && ($active_link == $sideMenu['link'] || $active_link == $sideMenu['link'] .'/')) ? 'active' : '';
					$expandSideMenu = 'ExpandClosed';
					if (!empty($sideMenu['child']) && !empty($sideMenu['link']) && strpos($active_link, $sideMenu['link']) !== false) {
						$expandSideMenu = 'ExpandOpen';
					}
					?>
					<li class="Node IsRoot <?= $expandSideMenu ?> <?= $activeClassSideMenu ?>">
						<?php if (!empty($sideMenu['child'])) : ?>
							<div class="Expand"></div>
						<?php else : ?>
							<div class="NoExpand"></div>
						<?php endif; ?>
						<div class="Content">
							<?= Html::a((!empty($sideMenu['title']) ? $sideMenu['title'] : ''), (!empty($sideMenu['link']) ? $sideMenu['link'] : '#')); ?>
							<?php if (!empty($sideMenu['child'])) : ?>
								<ul class="Container">
									<?php foreach ($sideMenu['child'] as $subMenu) : ?>
										<?php
										$activeClassSubMenu = (!empty($subMenu['link']) && ($active_link == $subMenu['link'] || $active_link == $subMenu['link'] .'/')) ? 'active' : '';
										$expandSubMenu = 'ExpandClosed';
										if (!empty($subMenu['child']) && !empty($subMenu['link']) && strpos($active_link, $subMenu['link']) !== false) {
											$expandSubMenu = 'ExpandOpen';
										}
										?>
										<li class="Node IsRoot <?= $expandSubMenu ?> <?= $activeClassSubMenu ?>">
											<?php if (!empty($subMenu['child'])) : ?>
												<div class="Expand"></div>
											<?php else : ?>
												<div class="NoExpand"></div>
											<?php endif; ?>
											<div class="Content">
												<?= Html::a((!empty($subMenu['title']) ? $subMenu['title'] : ''), (!empty($subMenu['link']) ? $subMenu['link'] : '#')); ?>
												<?php if (!empty($subMenu['child'])) : ?>
													<ul class="Container">
														<?php foreach ($subMenu['child'] as $subSubMenu) : ?>
															<?php
															$activeClassSubSubMenu = (!empty($subSubMenu['link']) && ($active_link == $subSubMenu['link'] || $active_link == $subSubMenu['link'] .'/')) ? 'active' : '';
															$expandSubSubMenu = 'ExpandClosed';
															if (!empty($subSubMenu['child']) && !empty($subSubMenu['link']) && strpos($active_link, $subSubMenu['link']) !== false) {
																$expandSubSubMenu = 'ExpandOpen';
															}
															?>
															<li class="Node IsRoot <?= $expandSubSubMenu ?> <?= $activeClassSubSubMenu ?>">
																<?php if (!empty($subSubMenu['child'])) : ?>
																	<div class="Expand"></div>
																<?php else : ?>
																	<div class="NoExpand"></div>
																<?php endif; ?>
																<div class="Content">
																	<?= Html::a((!empty($subSubMenu['title']) ? $subSubMenu['title'] : ''), (!empty($subSubMenu['link']) ? $subSubMenu['link'] : '#')); ?>
																</div>
															</li>
														<?php endforeach; ?>
													</ul>
												<?php endif; ?>
											</div>
										</li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>

</aside>
<?php
	$this->endCache();
endif;
?>


