<?php

use yii\helpers\Url;
use app\assets\AppAsset;

//var_dump($table_nedvizimost);die();
?>
<?php if (!empty($table_nedvizimost)) : ?>
<?php $this->registerCss(<<<CSS
	.gv_galleryWrap { width: 100% !important; height: 301px !important; }
	.gv_gallery { width: 100% !important; height: 291px !important; }
	.gv_panelWrap { width: 100% !important; height: 400px !important;}
	.gv_panelNavNext, .gv_panelNavPrev { top: 136px !important; }
	.gv_panel img { height: auto !important; }
		
	/* GALLERY PANELS */
	.gv_panel { width: 100% !important;  }
	.gv_panel img { height: auto !important; }
	.gv_filmstripWrap { width: 600px!important; }
	.gv_navWrap { z-index: 2; }
	.gallery-nedvizimost { margin-bottom: 10px; }
	
	@media (max-width: 320px) {
		.gv_galleryWrap { height: 175px !important; }
		.gv_gallery { height: 165px !important; }
		.gv_panelNavNext, .gv_panelNavPrev { top: 60px !important; }
	}
	@media (min-width: 320px) and (max-width: 375px) {
		.gv_galleryWrap { height: 215px !important; }
		.gv_gallery { height: 203px !important; }
		.gv_panel { height: 190px !important; }
		.gv_panelNavNext, .gv_panelNavPrev { top: 80px !important; }
	}
	@media (min-width: 376px) and (max-width: 575.98px) {
		.gv_galleryWrap { height: 245px !important; }
		.gv_gallery { height: 235px !important; }
		.gv_panel { height: 190px !important; }
		.gv_panelNavNext, .gv_panelNavPrev { top: 100px !important; }
	}
	@media (min-width: 576px) and (max-width: 768px) {
		.gv_galleryWrap { height: 410px !important; }
		.gv_gallery { height: 400px !important; }
		.gv_panel { height: 360px !important; }
		.gv_panelNavNext, .gv_panelNavPrev { top: 185px !important; }
	}
	@media (min-width: 769px) and (max-width: 1024px) {
		.gv_galleryWrap { height: 240px !important; }
		.gv_gallery { height: 230px !important; }
		.gv_panel { height: 35vh !important; }
		.gv_panelNavNext, .gv_panelNavPrev { top: 90px !important; }
	}
	@media (min-width: 1440px) {
		.gv_galleryWrap { height: 310px !important; }
		.gv_gallery { width: 100% !important; height: 295px !important; }
		.gv_panel { width: 100% !important; height: 35vh !important; }
		/*.gv_panel img { height: 20vh !important; }*/
		.gv_panelNavNext, .gv_panelNavPrev { top: 125px !important; }
	}

CSS
);

$this->registerCssFile('/plugins/gallery/css/jquery.galleryview-3.0-dev.css', ['depends' => AppAsset::className()]);
$id_cache = md5(Url::to() . 'table_nedvizimost');
	if ($this->beginCache($id_cache, ['duration' => defined('CACHE_DURATION') ? CACHE_DURATION : 86400])) :
		?>
		<div class="col-lg-12" style="display: block;">
			<div class="col-md-7 center">
				<div class="js-gallery-nedvizimost gallery-nedvizimost"></div>
			</div>
			<div class="col-md-5">
				<div class="row">
					<?php if (!empty($table_nedvizimost['province_baza'])): ?>
						<div class="col-xs-6"><strong>Провинция</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['province_baza'] ?></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['location_detail_baza'])): ?>
						<div class="col-xs-6"><strong>Область:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['location_detail_baza'] ?></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['town_baza'])): ?>
						<div class="col-xs-6"><strong>Город:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['town_baza'] ?></div>
					<?php endif; ?>
					<tr>
						<td class="table_row"></td>
						<td></td>
					</tr>
					<?php if (!empty($table_nedvizimost['type_baza'])): ?>
						<div class="col-xs-6"><strong>Тип:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['type_baza'] ?></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['built_baza'])): ?>
						<div class="col-xs-6"><strong>Площадь:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['built_baza'] ?> m<sup>2</sup></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['plot_baza'])): ?>
						<div class="col-xs-6"><strong>Участок:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['plot_baza'] ?> m<sup>2</sup></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['baths_baza'])): ?>
						<div class="col-xs-6"><strong>Ванных:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['baths_baza'] ?></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['beds_baza'])): ?>
						<div class="col-xs-6"><strong>Спален:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['beds_baza'] ?></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['pool_baza'])): ?>
						<div class="col-xs-6"><strong>Бассейнов:</strong></div>
						<div class="col-xs-6"><?= $table_nedvizimost['pool_baza'] ?></div>
					<?php endif; ?>
					<?php if (!empty($table_nedvizimost['price_baza'])): ?>
						<div class="col-xs-6"><strong>Цена:</strong></div>
						<div class="col-xs-6"
							 class="price"><?= number_format($table_nedvizimost['price_baza'], 0, ',', ' ') ?> €
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<?php

		$this->endCache();
endif;
$id_baza = $table_nedvizimost['id_baza'] ? $table_nedvizimost['id_baza'] : false;
$this->registerJs(<<<JS
	$.ajax({
		url: "/images-gallery?name=$id_baza",
		dataType: 'html',
		success: function(data){
			$('.js-gallery-nedvizimost').html(data);
			$('#myGallery').galleryView({
				frame_opacity: 0.8,
			});
		}
	});
JS
);
$this->registerJsFile('/plugins/gallery/js/jquery.timers-1.2.js', ['depends' => \yii\web\YiiAsset::className()]);
$this->registerJsFile('/plugins/gallery/js/jquery.galleryview-3.0-dev.js', [
	'depends' =>
		\yii\web\YiiAsset::className()
]);
?>
<?php endif; ?>