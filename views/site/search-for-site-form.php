<?php

use yii\helpers\Url;

?>

<div class="blog-aside-item">
	<p class="text-black text-ubold text-uppercase text-spacing-200">Поиск по сайту</p>
	<form class="form-blog-search form-blog-search-type-2 form-search rd-search"
		  action="<?= Url::to(['/search-result']) ?>" method="GET">
		<button class="form-search-submit" type="submit"><span class="fa fa-search"></span></button>
		<div class="form-wrap form-wrap-xs">
			<label class="form-label form-search-label form-label-sm" for="blog-sidebar-form-search-widget">Введите свой
				запрос</label>
			<input class="form-search-input input-sm form-input input-sm" id="blog-sidebar-form-search-widget"
				   type="text" name="text_request" autocomplete="off"
				   value="<?= !empty($text_request) ? $text_request : '' ?>" />
		</div>
	</form>
</div>
