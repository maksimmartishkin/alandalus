<?php

use app\assets\AppAsset;

$this->registerCss(<<<CSS
	.gv_galleryWrap { width: 100% !important; height: 252px !important; }
	.gv_gallery { width: 100% !important; height: 242px !important; }
	.gv_panelWrap { width: 100% !important; height: 400px !important;}
	.gv_panelNavNext, .gv_panelNavPrev { top: 100px !important; }
	.gv_panel img { width: 340px !important; height: auto !important; }
	.gv_navWrap { z-index: 2; }
	.gv_filmstripWrap { width: 100% !important; }
	
	@media (max-width: 575.98px) {
		.gv_galleryWrap {
			height: 42vh !important;
		}
		.gv_gallery {
			width: 100% !important;
			height: 40vh !important;
		}
		.gv_panelWrap {
			height: 41vh !important;
		}
		.gv_panel img {
			height: 35vh !important;
		}
	}
CSS
);
$this->registerCssFile('/plugins/gallery/css/jquery.galleryview-3.0-dev.css',
	[
		'depends' => AppAsset::className()
	]
);
?>
<div class="js-gallery-view-500 gallery-view-500"></div>
<?php
if (!empty($album_list) && !empty($album_attr)) :
	$this->registerJs(<<<JS
window.onload = function () {
	setTimeout(function(){
    	$.ajax({
			url: "/images-gallery?$album_attr=$album_list",
			dataType: 'html',
			success: function(data){
				// conso
				$('.js-gallery-view-500').html(data);
				$('#myGallery').galleryView({
					frame_opacity: 0.8,
				});
			}
		});
	}, 5000);
}
JS
	);
endif;

$this->registerJsFile('/plugins/gallery/js/jquery.timers-1.2.js',
	[
		'depends' => \yii\web\YiiAsset::className()
	]
);
$this->registerJsFile('/plugins/gallery/js/jquery.galleryview-3.0-dev.js',
	[
		'depends' => \yii\web\YiiAsset::className()
	]
);