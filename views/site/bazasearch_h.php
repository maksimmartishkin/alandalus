<?php

use app\models\SiteTmplvarContentvalues;
use yii\helpers\Url;

$category = Yii::$app->request->get('category');
$price = Yii::$app->request->get('price');
$town = Yii::$app->request->get('town');
$beds_ot = Yii::$app->request->get('beds_ot');
$type = Yii::$app->request->get('type');
$ref = Yii::$app->request->get('ref');

?>
<div class="bazasearch_h">
	<div class="buscador_t">
		<h2 class="buscador_titulo">Поиск недвижимости</h2>
	</div>
	<div id="bazasearch_v" style="background: #F2F2F2; padding-top: 15px; border-radius: 0px 0px 10px 10px;">

		<form name="search" method="get" action="<?= Url::to(['/search-baza']) ?>">
			<div class="">
				<label>Категория:</label>
				<span class='wrap2'>
					<select name="category" class="baza_select">
						<option <?= $category && $category == 'sale' ? 'selected' : '' ?> value="sale">Продажа</option>
						<option <?= $category && $category == 'rent' ? 'selected' : '' ?> value="rent">Аренда</option>
					</select>
				</span>
			</div>

			<div style="padding-top: 5px;" class="left">
				<label>Цена:</label>
				<span class='wrap2'>
        <select name="price" tabindex="0" class="baza_select">
          <option value="">Все</option>
          <option <?= $price && $price == '150001' ? 'selected' : '' ?> value="150001">до 150,000 €</option>
          <option <?= $price && $price == '150000|200001' ? 'selected' : '' ?> value="150000|200001">150 - 200,000 €</option>
          <option <?= $price && $price == '199999|250001' ? 'selected' : '' ?> value="199999|250001">200 - 250,000 €</option>
          <option <?= $price && $price == '249999|300001' ? 'selected' : '' ?> value="249999|300001">250 - 300,000 €</option>
          <option <?= $price && $price == '300000' ? 'selected' : '' ?> value="300000">более 300,000 €</option>
        </select>
        </span></div>

			<div style="padding-top: 5px;" class="width300 left"><label>Расположение:</label>
				<span class='wrap2'>
        <select name="town" class="baza_select" style="width: 115px;">
			<?php echo SiteTmplvarContentvalues::getBazaSelect($town) ?>
        </select>
        </span>
			</div>

			<div style="padding-top: 5px;" class="left"><label>Спальни:</label>
				<span class='wrap2'>

<select name="beds_ot" tabindex="0" class="baza_select">
<option value="0">Все</option>
<option <?= $beds_ot && $beds_ot == '1' ? 'selected' : '' ?> value="1">от 1</option>
<option <?= $beds_ot && $beds_ot == '2' ? 'selected' : '' ?> value="2">от 2</option>
<option <?= $beds_ot && $beds_ot == '3' ? 'selected' : '' ?> value="3">от 3</option>
<option <?= $beds_ot && $beds_ot == '4' ? 'selected' : '' ?> value="4">от 4 и более</option>
        </select>
        </span>
			</div>

			<div style="padding-top: 5px;" class="width240 left">
				<label>Тип:</label>
				<span class='wrap2'>
									<select name="type" class="baza_select">
									  <option value="">Все</option>
									  <option <?= $type && $type == 'apartment' ? 'selected' : '' ?> value="apartment">Апартаменты</option>
									  <option <?= $type && $type == 'studio' ? 'selected' : '' ?> value="studio">Студии</option>
									  <option <?= $type && $type == 'townhouse' ? 'selected' : '' ?> value="townhouse">Таунхаусы</option>
									  <option <?= $type && $type == 'villa' ? 'selected' : '' ?> value="villa">Виллы</option>
									  <option <?= $type && $type == 'commercial' ? 'selected' : '' ?> value="commercial">Коммерческая</option>
									  <option <?= $type && $type == 'plot' ? 'selected' : '' ?> value="plot">Земельные участки</option>
									</select>
								</span>
			</div>

			<div style="padding-top: 5px;" class="left">
				<label>Ref.</label>
				<span class='wrap2'>
        							<input type="text" name="ref" value="<?= $ref ? $ref : '' ?>" maxlength="10"
										   class="baza_select form-input" />
        						</span>
			</div>

			<div style="padding-top: 5px;" class="center">
				<input class="button button-primary" type="submit" value="Поиск" />
			</div>
		</form>
	</div>
</div>
<br>
