<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

//var_dump($query_result);die();
$this->title = 'Результаты поиска';
$this->params['breadcrumbs'][] = $this->title;


?>


<div class="box box-lg box-single-post bg-default d-block">
	<h4 class="text-ubold"><?=$this->title?></h4>
	<div class="text-silver-chalice text-left">

		<?php if(!empty($query_result)) : ?>
			<?php foreach ($query_result as $key => $result) : ?>
				<?php
				$number_iterator = !empty($pages)?($pages->page * $pages->pageSize + $key + 1):$key + 1;
				?>
				<div class="post-box post-box-wide d-block text-left">
					<div class="post-box-caption">
						<div class="post-box-title h5 text-ubold"><?=$number_iterator?>.
							<a class="text-black" href="<?=Url::to(['/'. $result->uri])?>" title="<?=$result->pagetitle?>">
								<?=$result->pagetitle?>
							</a>
						</div>
						<p class="text-small text-silver-chalice">
							<?=mb_strimwidth(strip_tags($result->content), 0, 300, '...');?>
						</p>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else : ?>
			<p class="alert alert-warning" role="alert">
				По вашему запросу ничего не найдено. Попробуйте ввести похожие по смыслу слова, чтобы получить лучший результат.
			</p>
		<?php endif; ?>
	</div>
	<hr class="hr bg-alto">
	<?php
	if(!empty($pages)) {
		echo LinkPager::widget([
			'pagination' => $pages,
			'hideOnSinglePage' => true,
			'maxButtonCount'=>5,
			'options' => ['class' => 'pagination-classic'],
		]);
	}
	?>

	<div class="d-inline-block inset-md-left-10">
		<ul class="list-inline list-inline-modern list-inline-11 bg-wild-wand">
			<li class="text-center"><span class="icon icon-sm icon-circle icon-filled-primary"><img class="img-responsive center-block" src="images/icons/icon-18-18x15.png" width="18" height="15" alt=""></span></li>
			<li class="text-center"><a class="icon fa fa-facebook-f text-gray" href="#"></a></li>
			<li class="text-center"><a class="icon fa fa-twitter text-gray" href="#"></a></li>
			<li class="text-center"><a class="icon fa fa-youtube text-gray" href="#"></a></li>
			<li class="text-center"><a class="icon fa fa-linkedin text-gray" href="#"></a></li>
		</ul>
	</div>
</div>

