<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/site.css?v=29082020.3',
		'plugins/bootstrap_datetimepicker/css/bootstrap-datetimepicker.min.css?v=22082020',
	];
	public $js = [
		[
			'js/share42.js?v=22082020',
			'defer' => true
		],
		//		'js/watermark.min.js',
		//		'plugins/jquery.lazyload.js',
		[
			'plugins/bootstrap_datetimepicker/js/moment-with-locales.min.js?v=22082020',
			'defer' => true
		],
		[
			'plugins/bootstrap_datetimepicker/js/bootstrap-datetimepicker.min.js?v=22082020',
			'defer' => true
		],
		[
			'js/site.js?v=25082020',
			'defer' => true
		],
		//    	'js/site.js',
	];
	public $depends = ['app\assets\CoreAsset',];
}
