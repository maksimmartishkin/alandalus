<?php


namespace app\assets;

use yii\web\AssetBundle;

class CoreAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
		'fonts/fonts.googleapis.css?v=22082020',
		'css/bootstrap.css?v=22082020',
		'plugins/tree/nogrid.css?v=22082020',
		'css/fonts.css?v=22082020',
		'css/style.css?v=22082020',
	];

	public $js = [
		'js/core.min.js?v=22082020',
		'plugins/tree/tree_toggle.js?v=22082020',
		'plugins/jquery.lazyload.js?v=22082020',
		'js/script.js?v=29082020',
	];

	public $depends = [
		'yii\web\JqueryAsset',
	];

}