<?php


namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\helpers\Html;
use yii\web\Controller;
use yii\data\Pagination;
use yii\web\HttpException;
use app\models\ContactForm;
use app\models\RentCarForm;
use app\models\SiteContent;
use yii\filters\VerbFilter;
use app\models\LastPublForm;
use app\models\QuipComments;
use app\models\LastPublPosts;
use yii\filters\AccessControl;
use app\models\SubscribeLists;
use app\models\YachtRentalForm;
use app\models\SiteContentSearch;
use app\models\RentRealEstateForm;
use yii\web\NotFoundHttpException;
use app\models\ExcursionGuidesForm;
use app\models\SiteTmplvarContentvalues;


class SiteController extends Controller
{
	const RENDER_PATH = '@app/views/site/';

	/**
	 * Время кэширования
	 *
	 * @var int
	 */
	public $duration = 604800;

	/**
	 * Объект компонента кэша
	 *
	 * @var yii\caching\FileCache
	 */
	public $cache;

	/**
	 * Если не нужно кэшировать данные, то ставим true
	 *
	 * @var bool
	 */
	public $nocache = false;

	/**
	 * Список случайных объектов недвижимости
	 *
	 * @var string[]
	 */
	public $random_object = [
		'5248',
		'5247',
		'5245',
		'5244',
		'5243',
		'5242'
	];

	//	/**
	//	 * Используемый шаблон
	//	 *
	//	 * @var string
	//	 */
	//	public $layout = 'main-new';

	/**
	 * @return array|array[]
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => ['logout' => ['post'],],
			],
		];
	}

	/**
	 * @return array
	 */
	public function actions()
	{
		return [
			'error' => ['class' => 'yii\web\ErrorAction',],
			'captcha' => [
				'class' => 'developit\captcha\CaptchaAction',
				'type' => 'default',
				// 'numbers', 'letters' or 'default' (contains numbers & letters)
				'minLength' => 4,
				'maxLength' => 6,
				'foreColor' => 0xff9f1c,
			],
		];
	}

	/**
	 * Настройка страниц до загрузки
	 *
	 * @param \yii\base\Action $action
	 *
	 * @return bool
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{
		$this->cache = Yii::$app->cache;

		if (defined('CACHE_DURATION')) {
			$this->duration = CACHE_DURATION;
		}

		if (in_array($action->id, ['get-last-posts-request'])) {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}

	/**
	 * Главная страница
	 *
	 * @return string
	 * @throws \yii\db\Exception
	 */
	public function actionIndex()
	{
		$model = new LastPublForm();
		if ($model->load(Yii::$app->request->post()) && $model->subscribe(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');

			return $this->refresh();
		}

		$content = $this->getConvertSiteContent(SiteContent::findContentByMain(1, $this->duration), 'index');
		return $this->render('index', [
			'content' => $content
		]);
	}

	/**
	 * Получение дополнительных публикаций
	 * @return string
	 */
	public function actionGetLastPostsRequest()
	{
		if (Yii::$app->request->post() && Yii::$app->request->isAjax) {
			$limit = Yii::$app->request->post('limit');
			$offset = Yii::$app->request->post('offset');
			$lastPosts = LastPublPosts::getLastPublPost($limit, $offset);
			$lastPublPosts = Yii::$app->controller->renderPartial(static::RENDER_PATH . "index/last-publ-posts", [
				'lastPosts' => $lastPosts,
				'request' => true,
			]);
			return $lastPublPosts;
		}

	}

	/**
	 * Подтверждение подписки на статьи
	 *
	 * @param false $token токен
	 *
	 * @return string
	 */
	public function actionSubscribeConfirm($token = false)
	{
		$dataToken = json_decode(base64_decode($token), true);
		$email = isset($dataToken['email']) ? $dataToken['email'] : null;
		$token = isset($dataToken['token']) ? $dataToken['token'] : null;
		if (!$email || !$token) {
			$text = "Токен не верен. ". Html::a('Главная', Url::to(['/']));
			return Html::tag('div', $text, ['class' => 'alert alert-warning']);
		}

		$emailSubscribe = SubscribeLists::findOne([
			'email' => $dataToken['email'],
			'token' => $dataToken['token'],
			'confirm' => 0,
		]);

		if (empty($emailSubscribe)) {
			$text = "Email $email подписан, или проблема с подтверждением. ". Html::a('Главная', Url::to(['/']));
			return Html::tag('div', $text, ['class' => 'alert alert-warning']);
		}

		$emailSubscribe->confirm = 1;
		if (!$emailSubscribe->save()) {
			$text = "Проблемы с подтверждением. ". $emailSubscribe->errors . ' '. Html::a('Главная', Url::to(['/']));
			return Html::tag('div', $text, ['class' => 'alert alert-danger']);
		}

		$text = "Вы успешно подтвердили подписку. ". Html::a('Главная', Url::to(['/']));
		return Html::tag('div', $text, ['class' => 'alert alert-success']);
	}

	/**
	 * Конвертирование данных из БД
	 *
	 * @param array $content текст контента
	 * @param bool  $cacheName имя кэша для сохранения
	 * @param int   $page номер страницы
	 * @param bool  $noCache если не нужно сохранять в кэш
	 *
	 * @return array|bool|false конвертированные контент
	 * @throws \yii\db\Exception
	 */
	private function getConvertSiteContent($content = [], $cacheName = false, $page = 1)
	{
		if (empty($content)) {
			return false;
		}

		if ($contentCache = $this->cache->get($cacheName)) {
			return $contentCache;
		}

		$renderPath = static::RENDER_PATH;
		$new_content = $content['content'];

		/** Слайдер*/
		preg_match('/\[\[\$slider_swiper(\?*)(.*)\]\]/uxi', $new_content, $matches);
		if (!empty($matches)) {
			parse_str(html_entity_decode(array_pop($matches), ENT_COMPAT | ENT_HTML401), $output);
			$idCacheAssets = 'swiper-slide-main-assets1';
			if (($swiperSliderCacheAssets = Yii::$app->cache->get($idCacheAssets)) || ($swiperSliderCacheImages = Yii::$app->cache->get($idCacheAssets))) {
				$files = $swiperSliderCacheAssets ? $swiperSliderCacheAssets : ($swiperSliderCacheImages ? $swiperSliderCacheImages : []);
			}
			else {
				$files = isset($output['images']) ? $output['images'] : [];
				Yii::$app->cache->set($idCacheAssets, $files);
			}

			if (!empty($files)) {
				$roundCountSlider = isset($output['roundCountSlider']) ? ($output['roundCountSlider'] <= count($files) ? $output['roundCountSlider'] : count($files)) : 5;
				$filesRand = array_rand($files, $roundCountSlider);
				$titles = isset($output['titles']) ? $output['titles'] : [];
				$captions = isset($output['captions']) ? $output['captions'] : [];
				$links = isset($output['links']) ? $output['links'] : [];
				$swiperSlider = Yii::$app->controller->renderPartial($renderPath . "index/swiper-slide-index", [
					'roundCountSlider' => $roundCountSlider,
					'titles' => $titles,
					'captions' => $captions,
					'filesRand' => $filesRand,
					'files' => $files,
					'links' => $links
				]);
				$new_content = str_replace($matches[0], $swiperSlider, $new_content);
				unset($matches);
			}
		}

		/** Последние публикации */
		preg_match('/\[\[\$last_publ_posts(\?*)(.*)\]\]/uxi', $new_content, $matches);
		if (!empty($matches)) {
			parse_str(html_entity_decode(array_pop($matches), ENT_COMPAT | ENT_HTML401), $output);
			$lastPosts = LastPublPosts::getLastPublPost(4, 0);
			$lastPublPosts = Yii::$app->controller->renderPartial($renderPath . "index/last-publ-posts", [
				'blocksData' => $output,
				'lastPosts' => $lastPosts,
			]);
			$new_content = str_replace($matches[0], $lastPublPosts, $new_content, $count);
			unset($matches);
		}

		/** Подписка на последние публикации */
		preg_match('/\[\[\$subscribe_last_posts(\?*)(.*)\]\]/uxi', $new_content, $matches);
		if (!empty($matches)) {
			parse_str(html_entity_decode(array_pop($matches), ENT_COMPAT | ENT_HTML401), $output);
			$model = new LastPublForm();
			if ($model->load(Yii::$app->request->post()) && $model->subscribe(Yii::$app->params['adminEmail'])) {
				Yii::$app->session->setFlash('contactFormSubmitted');
			}

			$subscribeLastPosts = Yii::$app->controller->renderPartial($renderPath . "index/subscribe-last-posts", [
				'dataBlocks' => $output,
				'model' => $model,
			]);
			$new_content = str_replace($matches[0], $subscribeLastPosts, $new_content);
			unset($matches);
		}

		/** Наши лучшие предложения */
		preg_match('/\[\[\$best_offers(\?*)(.*)\]\]/uxi', $new_content, $matches);
		if (!empty($matches)) {
			parse_str(html_entity_decode(array_pop($matches), ENT_COMPAT | ENT_HTML401), $output);
			$bestOffers = Yii::$app->controller->renderPartial($renderPath . "index/best-offers", [
				'blocksData' => $output,
			]);
			$new_content = str_replace($matches[0], $bestOffers, $new_content);
			unset($matches);
		}

		/** Отзывы */
		preg_match('/\[\[\$guestbook(\?*)(.*)\]\]/uxi', $new_content, $matches);
		if (!empty($matches)) {
			parse_str(html_entity_decode(array_pop($matches), ENT_COMPAT | ENT_HTML401), $output);
			$limit = isset($output['limit']) ? $output['limit'] : 5;
			$reviews = QuipComments::getGuestBook($limit);
			$guestBook = Yii::$app->controller->renderPartial($renderPath . "index/guestbook", [
				'blocksData' => $output,
				'reviews' => $reviews,
			]);
			$new_content = str_replace($matches[0], $guestBook, $new_content, $count);
			unset($matches);
		}

		preg_match('/\[\[\$form?(.*)\]\]/uxi', $new_content, $matchForm);
		if (!empty($matchForm['1'])) {
			$dataForm = $matchForm['1'];
			$config = [
				'dataConfig' => $dataForm,
				'formSubject' => $content['longtitle']
			];
			$new_content = preg_replace('/\[\[\$form\?(.*)\]\]/uxi', \app\components\FormMainWidget::widget($config), $new_content);
		}

		/** Обработка страницы Отзывы */
		preg_match('/\[\[!Quip(.+)\]\]/uxi', $new_content, $matchReview);
		if (!empty($matchReview)) {
			$reviews = QuipComments::getGuestBook();
			$guestBook = Yii::$app->controller->renderPartial($renderPath . "guestbook", [
				'reviews' => $reviews,
			]);
			$new_content = str_replace($matchReview[0], $guestBook, $new_content, $count);
			unset($matchReview);
			//			var_dump($matchReview);
			//			die();
		}

		preg_match('/\[\[\$form\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'form-question', ['form_subject' => $content['longtitle']]), $new_content, $count);
			$match = null;
		}

		preg_match('/\[\[\$andalucia_faktyi\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$fakty = SiteContent::findFacts();
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'andalucia_factyi', ['fakty' => $fakty]), $new_content, $count);
			$match = null;
		}

		preg_match('/\[\[\$info_list\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$info_list = SiteContent::findInfoList();
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'info_list', ['info_list' => $info_list]), $new_content, $count);
			$match = null;
		}

		preg_match('/\[\[\$slovar_terminov\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$slovar_terminov = SiteContent::findSlovarTerminov();
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'slovar_terminov', ['slovar_terminov' => $slovar_terminov]), $new_content, $count);
			$match = null;
		}

		preg_match('/\[\[\$ACII\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$akczii = SiteContent::findAccii();
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'akczii', ['akczii' => $akczii]), $new_content, $count);
			$match = null;
		}

		preg_match('/\[\[\$dittonews\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$dittonews = SiteContent::findDittoNews();
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'ditto-news', ['dittonews' => $dittonews]), $new_content, $count);
			$match = null;
		}

		preg_match('/\[\[\$googleMapsScript\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'googleMapsScript'), $new_content, $count);
			$match = null;
		}

		preg_match('/\[\[\$yandexMapsScript\]\]/uxi', $new_content, $match);
		if (!empty($match[0])) {
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'yandexMapsScript'), $new_content, $count);
			$match = null;
		}

		if (preg_match('/\[\[!baza_ditto(.*)\]\]/uxi', $new_content, $match_baza_ditto)) {
			preg_match('/&amp;parents=`([0-9,]+)`/uxi', $match_baza_ditto[0], $parents_match);
			$parent = !empty($parents_match) ? $parents_match[1] : null;
			$baza = SiteContent::getBazaDitto($parent, $page);
			$new_content = str_ireplace($match_baza_ditto[0], Yii::$app->controller->renderPartial($renderPath . 'baza_ditto', ['baza' => $baza]), $new_content, $count);
		}

		if (strpos($new_content, '[[!baza_ditto? &amp;parents=`5262,5264,5265` &srv=`1` ]]') !== false) {
			$parent = str_replace([
				'[[!baza_ditto? &amp;parents=`',
				'` &srv=`1` ]]'
			], [
				'',
				''
			], '[[!baza_ditto? &amp;parents=`5262,5264,5265` &srv=`1` ]]');
			$baza = SiteContent::getBazaDitto($parent, $page);
			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5262,5264,5265` &srv=`1` ]]', Yii::$app->controller->renderPartial($renderPath . 'baza_ditto', ['baza' => $baza]), $new_content, $count);
		}

		if (preg_match('/\[\[!special:default=`(.*)`\]\]/uxi', $new_content, $match)) {
			preg_match('/\[\[\$(.*)\]\]/uxi', $match[1], $match1);
			$realEstate = $match1[1];
			$parent = $this->$realEstate;
			$baza = SiteContent::getBazaDitto($parent, $page);
			$new_content = str_ireplace($match[0], Yii::$app->controller->renderPartial($renderPath . 'baza_ditto', ['baza' => $baza]), $new_content, $count);
		}

		if (preg_match('/<ul .*? id="mygallerynew" .*?>(.*?)<\/ul>/uxi', $new_content, $findmygallerynew)) {
			if (preg_match('/\[\[(.*?)\]\]/uxi', $findmygallerynew[1], $mygallerynew)) {
				$galleryview = $mygallerynew[1];
				$album_list = false;
				if (preg_match('/album=`(.*)`\]\]/ui', $findmygallerynew[1], $match_gallery)) {
					$id_gallery_album = $match_gallery[1];
					$album_attr = 'album';
					$album_list = $id_gallery_album;
				}
				elseif (preg_match('/tag=`(.*)`\]\]/ui', $findmygallerynew[1], $match_gallery)) {
					$tag_gallery_album = $match_gallery[1];
					$album_attr = 'tag';
					$album_list = $tag_gallery_album;
				}

				$new_content = str_ireplace($findmygallerynew, Yii::$app->controller->renderPartial($renderPath . $galleryview, [
					'album_attr' => $album_attr,
					'album_list' => $album_list
				]), $new_content, $count);
			}
		}

		$new_content = str_replace('costa del sol', 'costa-del-sol', $new_content);
		$content_ar = explode(' ', $new_content);
		$new_content = [];
		$count = 1;

		foreach ($content_ar as $value) {
			if (preg_match('/href="([^"]+)"/', $value, $match)) {
				$links = str_replace('.html', '', $match[1]);
				if ($links == '#') {
					$new_content[] = $value;
				}
				elseif ($links == 'https://alandalus.ru/' || $links == 'https://alandalus.ru') {
					$new_content[] = $value;
				}
				elseif (preg_match("/~([0-9]+)/", $links, $match)) {
					$int_link = $match[1];
					$hash_link = '';
					$find_link = SiteContent::findIdToUri($int_link);
					if ($is_hash_link = explode('#', $links)) {
						if (count($is_hash_link) > 1) {
							$hash_link = '#' . $is_hash_link[1];
						}
					}

					if ($find_link !== null) {
						$new_content[] = str_replace($links, Url::to([$find_link]) . $hash_link, $value, $count);
					}
				}
				else {
					$new_content[] = str_replace($links, Url::to([$links]), $value, $count);
				}

			}
			elseif (preg_match('/src="([^"]+)"/', $value, $match)) {
				if (strpos($match[1], '/assets') === false && preg_match('%(png|jpg|gif)%i', $match[1], $match_img)) {
					$new_content[] = str_ireplace($match[1], Url::to([$match[1]]), $value, $count);
				}
				else {
					$new_content[] = $value;
				}
			}
			else {
				$new_content[] = $value;
			}
		}

		$search_ar = [
			'.html',
			'Стр.: [[!+page.nav]]',
		];
		$needle_ar = [
			'',
			'',
		];
		$new_content = implode(' ', $new_content);

		$content['content'] = str_replace($search_ar, $needle_ar, $new_content);

		if ($this->nocache) {
			$this->cache->set($cacheName, $content);
		}

		return $content;
	}

	/**
	 * Раздел Контакты
	 *
	 * @return string|Response
	 */
	public function actionContact()
	{
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');

			return $this->refresh();
		}

		return $this->render('contact', ['model' => $model,]);
	}

	/**
	 * Поиск по базе недвижимости
	 *
	 * @return string
	 */
	public function actionSearchBaza()
	{
		$model = new SiteContentSearch();

		$query_result = false;
		if (Yii::$app->request->get() && /*$model->load(Yii::$app->request->get()) &&*/ $query_result = $model->searchBaza(Yii::$app->request->queryParams)) {
			return $this->render('search-baza', ['query_result' => $query_result,]);
		}

		return $this->render('search-baza', ['query_result' => $query_result,]);
	}

	/**
	 * Поиск по сайту
	 *
	 * @param null $text_request позисковый запрос
	 * @param null $page         страница
	 *
	 * @return string
	 */
	public function actionSearchResult($text_request = null, $page = null)
	{
		$model = new SiteContentSearch();

		$queryResult = $pages = null;
		if (Yii::$app->request->get() && /*$model->load(Yii::$app->request->get()) &&*/ $query = $model->searchForSite(Yii::$app->request->queryParams)) {
			$text_request = $model->text_request;
			$countQuery = clone $query;
			$pages = new Pagination(['totalCount' => $countQuery->count()]);
			$pages->page = $page - 1;
			$pages->defaultPageSize = 10;
			$queryResult = $query->offset($pages->offset)->limit($pages->limit)->all();
			if (!empty($queryResult)) {
				foreach ($queryResult as $key => $item) {
					$queryResult[$key]->content = preg_replace('/\[\[\$form?(.*)\]\]/uxi', '', $item->content);
				}
			}
		}

		return $this->render('search-result', [
			'query_result' => $queryResult,
			'pages' => $pages,
		]);
	}

	/**
	 * Форма запроса
	 *
	 * @return Response
	 */
	public function actionFormQuestion()
	{
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');

			return $this->refresh();
		}
	}

	/**
	 * @return string
	 */
	public function actionInfo()
	{
		return $this->render('info');
	}

	/**
	 * Для тестов
	 *
	 * @return string
	 */
	public function actionTestPage()
	{
		if (YII_ENV_DEV) {
			phpinfo();
			exit();
		}

		return $this->render('test');
	}

	/**
	 * Получение галереи
	 *
	 * @return array|string
	 * @throws \yii\db\Exception
	 */
	public function actionImagesGallery()
	{
		if (Yii::$app->request->get()) {
			if ($name = Yii::$app->request->get('name')) {
				$result = $query = Yii::$app->db->createCommand("SELECT i.`filename`, a.`description`
				FROM `gallery_albums` a
				LEFT JOIN `gallery_album_items` ai ON ai.`album`=a.id
				LEFT JOIN `gallery_items` i ON i.`id`=ai.`item`
				WHERE a.`name`=:name", [':name' => $name])->cache($this->duration)->queryAll();
				$imagesGallery = '';
				$imagesGallery = '<ul id="myGallery">';
				foreach ($result as $item) {
					$imagesGallery .= '<li><img src="/assets/images/' . $item['filename'] . '" alt="' . $item['description'] . '" />';
				}

				$imagesGallery .= '</ul>';
				return $imagesGallery;
			}

			if ($idAlbum = Yii::$app->request->get('album')) {
				$result = $query = Yii::$app->db->createCommand("SELECT i.`name`, i.`filename`
						FROM `gallery_album_items` ai
						LEFT JOIN `gallery_items` i ON ai.`item`=i.`id`
						WHERE ai.`album`=:album AND i.`active`", [':album' => $idAlbum])->cache($this->duration)->queryAll();
				$imagesGallery = '<ul id="myGallery">';
				foreach ($result as $item) {
					$imagesGallery .= '<li><img src="/assets/images/' . $item['filename'] . '" alt="' . $item['name'] . '" />';
				}

				$imagesGallery .= '</ul>';
				return $imagesGallery;
			}

			if ($tagAlbum = Yii::$app->request->get('tag')) {
				$result = $query = Yii::$app->db->createCommand("SELECT i.`name`, i.`filename`
						FROM `gallery_tags` t
						LEFT JOIN `gallery_items` i ON t.`item`=i.`id`
						WHERE t.`tag`=:tag AND i.`active`", [':tag' => $tagAlbum])->cache($this->duration)->queryAll();
				$imagesGallery = '<ul id="myGallery">';
				foreach ($result as $item) {
					$imagesGallery .= '<li><img src="/assets/images/' . $item['filename'] . '" alt="' . $item['name'] . '" />';
				}

				$imagesGallery .= '</ul>';
				return $imagesGallery;
			}

			return [];
		}

		return [];
	}

	/**
	 * Обработка данных с формы обратной связи
	 *
	 * @return Response
	 */
	public function actionFormMainQuestion()
	{
		$model = new ContactForm();
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['success' => 'Форма успешно отправлена'];
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return ['error' => 'Форма не отправлена'];
	}

	/**
	 * Обработка данных с формы аренды яхт
	 *
	 * @return string
	 */
	public function actionYachtsRentalWindow2()
	{
		$model = new YachtRentalForm();
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->retail(Yii::$app->params['adminEmail'])) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['success' => 'Форма успешно отправлена'];
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return ['error' => 'Форма не отправлена'];
	}

	/**
	 * Обработка данных с формы аренды недвижимости
	 *
	 * @return string[]
	 */
	public function actionFormRentRealEstate()
	{
		$model = new RentRealEstateForm();
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->rent(Yii::$app->params['adminEmail'])) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['success' => 'Форма успешно отправлена'];
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return ['error' => 'Форма не отправлена'];
	}

	/**
	 * Обработка данных с формы заказа экскурсии и гида
	 *
	 * @return string[]
	 */
	public function actionFormExcursionGuides()
	{
		$model = new ExcursionGuidesForm();
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['success' => 'Форма успешно отправлена'];
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return ['error' => 'Форма не отправлена'];
	}

	/**
	 * Обработка данных с формы проката автомобиля
	 *
	 * @return string[]
	 */
	public function actionFormRentCar()
	{
		$model = new RentCarForm();
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->rent(Yii::$app->params['adminEmail'])) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['success' => 'Форма успешно отправлена'];
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return ['error' => 'Форма не отправлена'];
	}

	/**
	 * Action для всех разделов
	 *
	 * @param null $razdel1 Раздел 1 типа /nedvizimost-ispania
	 * @param null $razdel2 Раздел 1 типа /nedvizimost-ispania
	 * @param null $razdel3 Раздел 1 типа /nedvizimost-ispania
	 * @param null $razdel4 Раздел 1 типа /nedvizimost-ispania
	 * @param int  $page    Номер страницы для пагинации
	 *
	 * @return string|Response
	 * @throws HttpException
	 * @throws NotFoundHttpException
	 * @throws \yii\db\Exception
	 */
	public function actionSections($razdel1 = null, $razdel2 = null, $razdel3 = null, $razdel4 = null, $page = 1)
	{
		if ($razdel1 === 'yachts-rental-window2') {
			$model = new YachtRentalForm();
			if ($model->load(Yii::$app->request->post()) && $model->retail(Yii::$app->params['adminEmail'])) {
				Yii::$app->session->setFlash('contactFormSubmitted');

				return Yii::$app->getResponse()->refresh();
			}

			return $this->render('yachts-rental-window2', ['model' => $model]);
		}

		if ($razdel1 === 'kontaktyi') {
			$model = new ContactForm();
			if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
				Yii::$app->session->setFlash('contactFormSubmitted');

				return $this->refresh();
			}
			return $this->render('contact', ['model' => $model,]);
		}

		if ($razdel1) {
			if ($razdel2) {
				if ($razdel3) {
					if ($razdel4) {
						$uri = $razdel1 . '/' . $razdel2 . '/' . $razdel3 . '/' . $razdel4;
						if ($contentCache = $this->cache->get($uri)) {
							$content = $contentCache;
						}
						else {
							$content = $this->getConvertSiteContent(SiteContent::findContentByUrlAndAlias($uri, $razdel4, $this->duration), $uri, $page);
						}

						if (empty($content)) {
							throw new NotFoundHttpException('Запрошенная страница не найдена', 404);
						}

						$table_nedvizimost = false;
						if ($razdel1 === 'nedvizimost-ispania') {
							$table_nedvizimost = SiteTmplvarContentvalues::getValuestImagesAlbum($content['id']);
						}

						return $this->render('sections', [
							'content' => $content,
							'link' => $razdel1,
							'table_nedvizimost' => $table_nedvizimost
						]);
					}

					$uri = $razdel1 . '/' . $razdel2 . '/' . $razdel3;
					if ($contentCache = $this->cache->get($uri)) {
						$content = $contentCache;
					}
					else {
						$content = $this->getConvertSiteContent(SiteContent::findContentByUrlAndAlias($uri, $razdel3, $this->duration), $uri, $page);
					}

					if (empty($content)) {
						throw new HttpException(404, 'Запрошенная страница не найдена');
					}

					return $this->render('sections', [
						'content' => $content,
						'link' => $razdel1,
						'table_nedvizimost' => false
					]);
				}

				$uri = $razdel1 . '/' . $razdel2;
				if ($contentCache = $this->cache->get($uri)) {
					$content = $contentCache;
				}
				else {
					$content = $this->getConvertSiteContent(SiteContent::findContentByUrlAndAlias($uri, $razdel2, $this->duration), $uri, $page);
				}

				if (empty($content)) {
					throw new HttpException(404, 'Запрошенная страница не найдена');
				}

				return $this->render('sections', [
					'content' => $content,
					'link' => $razdel1,
					'table_nedvizimost' => false
				]);
			}

			$uri = $razdel1;
			if ($contentCache = $this->cache->get($uri)) {
				$content = $contentCache;
			}
			else {
				$content = $this->getConvertSiteContent(SiteContent::findContentByUrlAndAlias($uri, $razdel1, $this->duration), $uri, $page);
			}

			if (empty($content)) {
				throw new HttpException(404, 'Запрошенная страница не найдена');
			}

			return $this->render('sections', [
				'content' => $content,
				'link' => $razdel1,
				'table_nedvizimost' => false
			]);
		}

		throw new HttpException(404, 'Запрошенная страница не найдена');
	}

	/**
	 * sitemap.xml
	 */
	public function actionSitemapxml()
	{
		if (!$xml_sitemap = Yii::$app->cache->get('sitemap')) {
			$rules = SiteContent::find()->select([
				'uri',
				'CASE editedon WHEN 0 THEN FROM_UNIXTIME(publishedon, \'%Y-%m-%d\') ELSE FROM_UNIXTIME(editedon, \'%Y-%m-%d\') END lastmod'
			])->where([
				'published' => 1,
				'context_key' => 'web',
				'hidemenu' => '0'
			])->all();

			foreach ($rules as $rule => $route) {
				// ненужные для карты сайта маршруты
				$blacklistRoutes = [
					'alandalus.ru',
					'sitemap',
					'baza',
					'sale',
					'search-baza',
					'foto',
					'search',
					'rss.rss',
					'test-map',
					'guestbook',
					'404',
					'1',
					'sitemap.xml',
					'booking',
					'robots.txt',
				];

				if (in_array($route->uri, $blacklistRoutes)) {
					unset($rules[$rule]);
				}
			}

			$xml_sitemap = $this->renderPartial('sitemapxml', [
				'host' => str_replace([
					":80",
					":443"
				], "", Yii::$app->request->hostInfo),
				// текущий домен сайта
				'urls' => $rules,
			]);/**/
			Yii::$app->cache->set('sitemap', $xml_sitemap, 3600 * 12); // кэшируем результат, чтобы не нагружать сервер и не выполнять код при каждом запросе карты сайта.
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_XML; // устанавливаем формат отдачи контента

		echo $xml_sitemap;
		exit();
	}

	public function actionRobot()
	{

	}

	public function actionMenuTree()
	{
		if (!Yii::$app->request->get('url')) {
			return [];
		}

		$links = array_filter(explode('/', Yii::$app->request->get('url')));
		$razdel1 = array_shift($links);
		$razdel2 = array_shift($links);
		$razdel3 = array_shift($links);
		$razdel4 = array_shift($links);

		$side_menu = SiteContent::getSideMenuTree($razdel1, $razdel2, $razdel3, $razdel4);

		return $this->render('aside-menu-request', [
			'links' => $links,
			'side_menu' => $side_menu,
		]);

	}
}