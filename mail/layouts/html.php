<?php
use yii\helpers\Html;
//use yii\helpers\Url;
use app\assets\AppAsset;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
	<nav class="rd-navbar rd-navbar-light rd-navbar-light-lightmenu rd-navbar-right-side rd-navbar-right-side-wrap-mobile"
		 data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-static"
		 data-lg-device-layout="rd-navbar-static" data-lg-layout="rd-navbar-static"
		 data-xl-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static"
		 data-xxl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-lg-stick-up-offset="1px"
		 data-xl-stick-up-offset="1px" data-xxl-stick-up-offset="1px">
		<div class="rd-navbar-inner">
			<div class="rd-navbar-panel">
				<button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap">
					<span></span>
				</button>
				<div class="rd-navbar-brand rd-navbar-brand-desktop">
					<a class="brand-name" href="<?php /*= Url::to(['/']) */?>">
						<img width="148" height="30" src="https://alandalus.ru/images/logo-dark-250x76.png" alt="">
					</a>
				</div>
			</div>
			<div class="rd-navbar-right-side-wrap">
				<p>
					<a class="text-black" href="tel:#">
						<i class="fa fa-phone icon-navbar-menu-phone" aria-hidden="true"></i>
						<span class="h5 text-big text-ubold text-middle">+34 603-306-511</span><br />
					</a>
					<a class="text-black" href="tg://+34603306511">
						<img class="icon-messagers-block-custom margin-right-20"
							 src="/images/icons/icon-telegram-1.png" />
					</a>
					<a class="text-black" href="whatsapp://+34603306511">
						<img class="icon-messagers-block-custom margin-right-20"
							 src="/images/icons/icon-whatsapp-1.png" />
					</a>
					<a class="text-black" href="viber://add?number=34603306511">
						<img class="icon-messagers-block-custom"
							 src="/images/icons/icon-viber-1.png">
					</a>
				</p>
			</div>
		</div>
	</nav>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
