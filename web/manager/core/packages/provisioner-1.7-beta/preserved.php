<?php return array (
  'c666fdf2905bfbbe35ff7771df0a7cb8' => 
  array (
    'criteria' => 
    array (
      'name' => 'provisioner',
    ),
    'object' => 
    array (
      'name' => 'provisioner',
      'path' => '{core_path}components/provisioner/',
      'assets_path' => '',
    ),
  ),
  'e06437d0b15e6b31a20ce8888331976b' => 
  array (
    'criteria' => 
    array (
      'namespace' => 'provisioner',
      'controller' => 'index',
    ),
    'object' => 
    array (
      'id' => 78,
      'namespace' => 'provisioner',
      'controller' => 'index',
      'haslayout' => 1,
      'lang_topics' => 'provisioner:default,file',
      'assets' => '',
      'help_url' => '',
    ),
  ),
  '30776030a4f402d89c309d42e550eb49' => 
  array (
    'criteria' => 
    array (
      'text' => 'provisioner',
    ),
    'object' => 
    array (
      'text' => 'provisioner',
      'parent' => 'components',
      'action' => '78',
      'description' => 'provisioner.desc',
      'icon' => 'images/icons/plugin.gif',
      'menuindex' => 0,
      'params' => '',
      'handler' => '',
      'permissions' => '',
      'namespace' => 'core',
    ),
  ),
  '486a618cc0590f01f1ed48a1baa7166a' => 
  array (
    'criteria' => 
    array (
      'key' => 'status',
    ),
    'object' => 
    array (
      'key' => 'status',
      'value' => '1',
      'xtype' => 'textfield',
      'namespace' => 'provisioner',
      'area' => 'Provisioner',
      'editedon' => '2014-07-23 02:58:12',
    ),
  ),
  'f4e030303d226658aa863d910d856a14' => 
  array (
    'criteria' => 
    array (
      'key' => 'cookiefile',
    ),
    'object' => 
    array (
      'key' => 'cookiefile',
      'value' => '/pub/home/alandalus/new/assets/components/provisioner/tmp/CURL52hJZQ',
      'xtype' => 'textfield',
      'namespace' => 'provisioner',
      'area' => 'Provisioner',
      'editedon' => '2014-07-23 03:37:51',
    ),
  ),
  'd9b6423c8fbd2da86ed5693f1623bc1b' => 
  array (
    'criteria' => 
    array (
      'key' => 'url',
    ),
    'object' => 
    array (
      'key' => 'url',
      'value' => 'http://alandalus.ru',
      'xtype' => 'textfield',
      'namespace' => 'provisioner',
      'area' => 'Provisioner',
      'editedon' => '2014-06-15 20:39:25',
    ),
  ),
  'de0ac318a13fe9c2f78230fd73990002' => 
  array (
    'criteria' => 
    array (
      'key' => 'sitetype',
    ),
    'object' => 
    array (
      'key' => 'sitetype',
      'value' => 'evolution',
      'xtype' => 'textfield',
      'namespace' => 'provisioner',
      'area' => 'Provisioner',
      'editedon' => '2014-06-15 20:39:25',
    ),
  ),
  '16b0c5c486288afee6f4b24e76ebb54e' => 
  array (
    'criteria' => 
    array (
      'key' => 'account',
    ),
    'object' => 
    array (
      'key' => 'account',
      'value' => 'weter',
      'xtype' => 'textfield',
      'namespace' => 'provisioner',
      'area' => 'Provisioner',
      'editedon' => '2014-07-23 01:25:15',
    ),
  ),
  'e9bc7fb76c811ec8d7d0f6a7724b5a42' => 
  array (
    'criteria' => 
    array (
      'key' => 'siteid',
    ),
    'object' => 
    array (
      'key' => 'siteid',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'provisioner',
      'area' => 'Provisioner',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'e7f40fee3bce52a63741fcd7f1b773f9' => 
  array (
    'criteria' => 
    array (
      'key' => 'user',
    ),
    'object' => 
    array (
      'key' => 'user',
      'value' => 'sergey',
      'xtype' => 'textfield',
      'namespace' => 'provisioner',
      'area' => 'Provisioner',
      'editedon' => '2014-06-15 20:39:25',
    ),
  ),
  '266dd4d5ddffeaa42f6c9a0c2b9eff0e' => 
  array (
    'criteria' => 
    array (
      'key' => 'provisioner',
    ),
    'object' => 
    array (
      'key' => 'provisioner',
      'name' => NULL,
      'description' => 'The provisioner component context',
      'rank' => 1,
    ),
  ),
  '418fabc6b1f2752fba6d526ed29d8500' => 
  array (
    'criteria' => 
    array (
      'name' => 'Provisioner',
    ),
    'object' => 
    array (
      'id' => 2,
      'name' => 'Provisioner',
      'description' => NULL,
      'parent' => 0,
      'rank' => 0,
      'dashboard' => 1,
    ),
  ),
);