
Provisioner component for MODx Revolution

Purpose: Allows for the provisioning of installations from remote 
         Revolution and Evolution sites.
Author: S. Hamblett steve.hamblett@linux.com
For: MODx CMS (www.modxcms.com) Revolution
Date: 07/06/2010

If you wish to use Evolution sites as remote sites please ensure
you have the evolution gateway package installed into them.

See the user guide for details, especially the 'Installation' and 'Security' sections.



