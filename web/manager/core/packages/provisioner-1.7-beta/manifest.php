<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => 'You must agree to the License before continuing installation.

Usage of this software is subject to the GPL license. To help you understand
what the GPL licence is and how it affects your ability to use the software, we
have provided the following summary:

The GNU General Public License is a Free Software license.
Like any Free Software license, it grants to you the four following freedoms:-
	The freedom to run the program for any purpose. 
    The freedom to study how the program works and adapt it to your needs. 
    The freedom to redistribute copies so you can help your neighbor.
    The freedom to improve the program and release your improvements to the
    public, so that the whole community benefits.
    
You may exercise the freedoms specified here provided that you comply with
the express conditions of this license. The principal conditions are:-
	
	You must conspicuously and appropriately publish on each copy distributed an
    appropriate copyright notice and disclaimer of warranty and keep intact all the
    notices that refer to this License and to the absence of any warranty; and give
    any other recipients of the Program a copy of the GNU General Public License
    along with the Program. Any translation of the GNU General Public License must
    be accompanied by the GNU General Public License.

    If you modify your copy or copies of the program or any portion of it, or
    develop a program based upon it, you may distribute the resulting work provided
    you do so under the GNU General Public License. Any translation of the GNU
    General Public License must be accompanied by the GNU General Public License.

    If you copy or distribute the program, you must accompany it with the
    complete corresponding machine-readable source code or with a written offer,
    valid for at least three years, to furnish the complete corresponding
    machine-readable source code.

    Any of these conditions can be waived if you get permission from the
    copyright holder.

    Your fair use and other rights are in no way affected by the above.
    
The above is a summary of the GNU General Public License. By proceeding, you
are agreeing to the GNU General Public Licence, not the above. The above is
simply a summary of the GNU General Public Licence, and its accuracy is not
guaranteed. It is strongly recommended you read the <a href="http://www.gnu.org/copyleft/gpl.html">GNU General Public
License</a> in full before proceeding. 
',
    'readme' => '
Provisioner component for MODx Revolution

Purpose: Allows for the provisioning of installations from remote 
         Revolution and Evolution sites.
Author: S. Hamblett steve.hamblett@linux.com
For: MODx CMS (www.modxcms.com) Revolution
Date: 07/06/2010

If you wish to use Evolution sites as remote sites please ensure
you have the evolution gateway package installed into them.

See the user guide for details, especially the \'Installation\' and \'Security\' sections.



',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => 'c666fdf2905bfbbe35ff7771df0a7cb8',
      'native_key' => 'provisioner',
      'filename' => 'modNamespace/ac0c179bba90d130f2bb1a5365f8bc40.vehicle',
      'namespace' => 'provisioner',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modMenu',
      'guid' => '30776030a4f402d89c309d42e550eb49',
      'native_key' => 'provisioner',
      'filename' => 'modMenu/7f73a30670fe78f2fabdb9d83c34abcb.vehicle',
      'namespace' => 'provisioner',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '486a618cc0590f01f1ed48a1baa7166a',
      'native_key' => 'status',
      'filename' => 'modSystemSetting/fba12f89361b0cba762760fcf40817e0.vehicle',
      'namespace' => 'provisioner',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'f4e030303d226658aa863d910d856a14',
      'native_key' => 'cookiefile',
      'filename' => 'modSystemSetting/bbbb4eb82fb6d1ea5f7e66f56c149244.vehicle',
      'namespace' => 'provisioner',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd9b6423c8fbd2da86ed5693f1623bc1b',
      'native_key' => 'url',
      'filename' => 'modSystemSetting/b57f44c4398e6e8c6f8d05eb6aac1731.vehicle',
      'namespace' => 'provisioner',
    ),
    5 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'de0ac318a13fe9c2f78230fd73990002',
      'native_key' => 'sitetype',
      'filename' => 'modSystemSetting/1adfe3f370cf2e3f9635a4bdf6af5ea1.vehicle',
      'namespace' => 'provisioner',
    ),
    6 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '16b0c5c486288afee6f4b24e76ebb54e',
      'native_key' => 'account',
      'filename' => 'modSystemSetting/1d1f5a7db6d1010a59c131fe11f89b20.vehicle',
      'namespace' => 'provisioner',
    ),
    7 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'e9bc7fb76c811ec8d7d0f6a7724b5a42',
      'native_key' => 'siteid',
      'filename' => 'modSystemSetting/d0060af9b643b87ceebd5be03d1e0782.vehicle',
      'namespace' => 'provisioner',
    ),
    8 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'e7f40fee3bce52a63741fcd7f1b773f9',
      'native_key' => 'user',
      'filename' => 'modSystemSetting/4d628ad4c2331b36ef3778a081057b1c.vehicle',
      'namespace' => 'provisioner',
    ),
    9 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '0200634ca6c5858eca14a2f145030696',
      'native_key' => NULL,
      'filename' => 'modCategory/61a8ef535aba1fe260ecae2eebfd20b6.vehicle',
      'namespace' => 'provisioner',
    ),
    10 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modContext',
      'guid' => '266dd4d5ddffeaa42f6c9a0c2b9eff0e',
      'native_key' => 'provisioner',
      'filename' => 'modContext/1843507fd533d97bcab37eba4039edde.vehicle',
      'namespace' => 'provisioner',
    ),
    11 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modUserGroup',
      'guid' => '418fabc6b1f2752fba6d526ed29d8500',
      'native_key' => NULL,
      'filename' => 'modUserGroup/ed1b9dcbb7879c66094732bbddd22fb1.vehicle',
      'namespace' => 'provisioner',
    ),
  ),
);