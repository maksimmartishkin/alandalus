<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => '',
    'readme' => 'Console
=====================================================

Console allow to execute php-code at back-end by simple interface.
',
    'changelog' => 'Console-2.0.1-beta
=============================================================
- Saving code in $_SESSION;

Console-2.0.0-rc
=============================================================
- Ace integration fix;

Console-1.2.0-rc
=============================================================
- Set default LogTarget(\'HTML\');
- Set default LogLevel(xPDO::LOG_LEVEL_DEBUG);

Console-1.1.0-rc
=============================================================

- First release',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => '822ab7e96d86b87a1f4048160ea0fa05',
      'native_key' => 'console',
      'filename' => 'modNamespace/bfa56873b9e6794324d52221d655b701.vehicle',
      'namespace' => 'console',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => 'deb452e8da63f49895d35439593105b8',
      'native_key' => 'console',
      'filename' => 'modNamespace/0c035a7b686e7339177d924a8e59930d.vehicle',
      'namespace' => 'console',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modMenu',
      'guid' => 'cfb838831021836fb3beeb57cafec328',
      'native_key' => 'console',
      'filename' => 'modMenu/f82c37430de508e3a3a6fd8da880e85a.vehicle',
      'namespace' => 'console',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modMenu',
      'guid' => '9ec0441f4bd16f22819daa926a94ac20',
      'native_key' => 'console',
      'filename' => 'modMenu/791e256a7cb661c59d89d4a45fe2c158.vehicle',
      'namespace' => 'console',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '2f1ff764ba5633c28e969f8b93476038',
      'native_key' => 1,
      'filename' => 'modCategory/03d193ed3109eb3fa0190b1057a75700.vehicle',
      'namespace' => 'console',
    ),
  ),
);