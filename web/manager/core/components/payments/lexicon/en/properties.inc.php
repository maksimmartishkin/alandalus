<?php
/**
 * @package payments
 * @subpackage lexicon
 */
$_lang['prop_payments.ascending'] = 'Ascending';
$_lang['prop_payments.descending'] = 'Descending';
$_lang['prop_payments.dir_desc'] = 'The direction to sort by.';
$_lang['prop_payments.sort_desc'] = 'The field to sort by.';
$_lang['prop_payments.tpl_desc'] = 'The chunk for displaying each row.';
