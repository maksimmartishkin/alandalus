<?php
/**
 * Default English Lexicon Entries for Payments
 *
 * @package payments
 * @subpackage lexicon
 */
$_lang['payment'] = 'Payment';
$_lang['payments'] = 'Payments';
$_lang['payments.desc'] = 'Manage your payments here.';
$_lang['payments.description'] = 'Page';
$_lang['payments.payment_err_ae'] = 'A payment with that name already exists.';
$_lang['payments.payment_err_nf'] = 'Payment not found.';
$_lang['payments.payment_err_ns'] = 'Payment not specified.';
$_lang['payments.payment_err_ns_name'] = 'Please specify a name for the payment.';
$_lang['payments.payment_err_remove'] = 'An error occurred while trying to remove the payment.';
$_lang['payments.payment_err_save'] = 'An error occurred while trying to save the payment.';
$_lang['payments.payment_err_data'] = 'Invalid data.';
$_lang['payments.payment_create'] = 'Create New Payment';
$_lang['payments.payment_remove'] = 'Remove Payment';
$_lang['payments.payment_remove_confirm'] = 'Are you sure you want to remove this payment?';
$_lang['payments.payment_update'] = 'Update Payment';
$_lang['payments.downloads'] = 'Downloads';
$_lang['payments.location'] = 'Price';
$_lang['payments.management'] = 'Payments Management';
$_lang['payments.management_desc'] = 'Manage your payments here. You can edit them by either double-clicking on the grid or right-clicking on the respective row.';
$_lang['payments.name'] = 'Date';
$_lang['payments.search...'] = 'Search...';
$_lang['payments.top_downloaded'] = 'Top Downloaded Payments';

