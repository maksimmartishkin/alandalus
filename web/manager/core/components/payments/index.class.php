<?php
/**
 * @package payments
 * @subpackage controllers
 */
require_once dirname(__FILE__) . '/model/payments/payments.class.php';
abstract class PaymentsManagerController extends modExtraManagerController {
    /** @var Payments $payments */
    public $payments;
    public function initialize() {
        $this->payments = new Payments($this->modx);

        $this->addCss($this->payments->config['cssUrl'].'mgr.css');
        $this->addJavascript($this->payments->config['jsUrl'].'mgr/payments.js');
        $this->addHtml('<script type="text/javascript">
        Ext.onReady(function() {
            Payments.config = '.$this->modx->toJSON($this->payments->config).';
        });
        </script>');
        return parent::initialize();
    }
    public function getLanguageTopics() {
        return array('payments:default');
    }
    public function checkPermissions() { return true;}
}
/**
 * @package payments
 * @subpackage controllers
 */
class IndexManagerController extends PaymentsManagerController {
    public static function getDefaultController() { return 'home'; }
}
