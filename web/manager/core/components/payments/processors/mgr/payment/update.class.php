<?php
/**
 * @package payment
 * @subpackage processors
 */
class PaymentUpdateProcessor extends modObjectUpdateProcessor {
    public $classKey = 'Payment';
    public $languageTopics = array('payments:default');
    public $objectType = 'payments.payment';
}
return 'PaymentUpdateProcessor';
