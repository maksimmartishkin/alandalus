<?php
/**
 * @package payments
 * @subpackage processors
 */
class PaymentCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'Payment';
    public $languageTopics = array('payments:default');
    public $objectType = 'payments.payment';

    public function beforeSave() {
        $name = $this->getProperty('name');

        if (empty($name)) {
            $this->addFieldError('name',$this->modx->lexicon('payments.payment_err_ns_name'));
        } else if ($this->doesAlreadyExist(array('name' => $name))) {
            $this->addFieldError('name',$this->modx->lexicon('payments.payment_err_ae'));
        }
        return parent::beforeSave();
    }
}
return 'PaymentCreateProcessor';
