<?php
/**
 * Get a list of Payments
 *
 * @package payments
 * @subpackage processors
 */
class PaymentGetListProcessor extends modObjectGetListProcessor {
    public $classKey = 'Payment';
    public $languageTopics = array('payments:default');
    public $defaultSortField = 'name';
    public $defaultSortDirection = 'ASC';
    public $objectType = 'payments.payment';

    public function prepareQueryBeforeCount(xPDOQuery $c) {
        $query = $this->getProperty('query');
        if (!empty($query)) {
            $c->where(array(
                'name:LIKE' => '%'.$query.'%',
                'OR:description:LIKE' => '%'.$query.'%',
                'OR:location:LIKE' => '%'.$query.'%',
            ));
        }
        return $c;
    }
}
return 'PaymentGetListProcessor';
