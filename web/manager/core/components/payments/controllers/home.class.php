<?php
/**
 * @package payments
 * @subpackage controllers
 */
class PaymentsHomeManagerController extends PaymentsManagerController {
    public function process(array $scriptProperties = array()) {

    }
    public function getPageTitle() { return $this->modx->lexicon('payments'); }
    public function loadCustomCssJs() {
        $this->addJavascript($this->payments->config['jsUrl'].'mgr/widgets/payments.grid.js');
        $this->addJavascript($this->payments->config['jsUrl'].'mgr/widgets/home.panel.js');
        $this->addLastJavascript($this->payments->config['jsUrl'].'mgr/sections/index.js');
    }
    public function getTemplateFile() { return $this->payments->config['templatesPath'].'home.tpl'; }
}
