<?php
/**
 * Provisioner lexicon
 *
 * @category  Provisioning
 * @author    S. Hamblett <steve.hamblett@linux.com>
 * @copyright 2009 S. Hamblett
 * @license   GPLv3 http://www.gnu.org/licenses/gpl.html
 * @link      none
 * @language en
 *
 * @package   provisioner
 * @subpackage lexicon
 */
$_lang['provisioner'] = 'Provisioner';
$_lang['button_go'] = 'Go';
$_lang['menu_account'] = 'Account';
$_lang['menu_provisioner'] = 'Provisioner';
$_lang['menu_administration'] = 'Remote Site Administration';
$_lang['menu_administration_tab'] = 'Administration';
$_lang['menu_resources'] = 'Resources';
$_lang['menu_password'] = 'Password';
$_lang['menu_elements'] = 'Elements';
$_lang['menu_files'] = 'Files';
$_lang['menu_url'] = 'URL';
$_lang['noaccount_error'] = 'No Account supplied';
$_lang['nopassword_error'] = 'No Password supplied';
$_lang['nourl_error'] = 'No Connector URL supplied';
$_lang['loginsuccess'] = 'Login to the Remote Site successful';
$_lang['loginfailed'] = 'Login to the Remote Site failed';
$_lang['logoutsuccess'] = 'Logout of the Remote Site successful';
$_lang['logoutfailed'] = 'Logout of the Remote Site failed';
$_lang['wronguser'] = 'A user is already logged in';
$_lang['radio_login'] = 'Login';
$_lang['radio_logout'] = 'Logout';
$_lang['notloggedin'] = 'Not logged in';
$_lang['urlnotfound'] = 'Connector URL not found';
$_lang['unknownerror'] = 'Unknown error';
$_lang['pv_resources'] = 'Remote Resources';
$_lang['resources_desc'] = 'Resources present in the remote site';
$_lang['menu_resources_tab'] = 'Resources';
$_lang['getresourcesfailed'] = 'Failed to get remote resources';
$_lang['resource_menu'] = 'Remote Resources';
$_lang['import_resource'] = 'Import';
$_lang['importresourcesfailed'] = 'Failed to import remote resources';
$_lang['importsuccess'] = 'Import of the remote resources successful';
$_lang['invalidresourceid'] = 'The resource id is invalid';
$_lang['failedtogetremoteresource'] = 'Cannot get remote resource';
$_lang['failedtocreatelocalresource'] = 'Cannot create the local resource';
$_lang['menu_elements_tab'] = 'Elements';
$_lang['pv_elements'] = 'Remote Elements';
$_lang['elements_desc'] = 'Elements present in the remote site';
$_lang['getelementsfailed'] = 'Failed to get remote elements';
$_lang['import_element'] = 'Import';
$_lang['importelementsfailed'] = 'Failed to import remote elements';
$_lang['failedtocreatelocalelement'] = 'Cannot create the local element';
$_lang['notyetimplemented'] = 'Function not yet implemented - Sorry!';
$_lang['menu_files_tab'] = 'Files';
$_lang['pv_files'] = 'Remote Files';
$_lang['files_desc'] = 'Files present in the remote site';
$_lang['getfilesfailed'] = 'Failed to get remote files';
$_lang['import_file'] = 'Import';
$_lang['nofilename'] = 'File name not supplied';
$_lang['nopathname'] = 'Path name not supplied';
$_lang['menu_packages_tab'] = 'Packages';
$_lang['packages'] = 'Remote Packages';
$_lang['packages_desc'] = 'Packages installed in the remote site';
$_lang['getpackagesfailed'] = 'Failed to get remote packages';
$_lang['package_signature'] = 'Package Name';
$_lang['localinstall'] = 'Installed Locally';
$_lang['local_installed'] = 'Yes';
$_lang['not_local_installed'] = 'No';
$_lang['menu_users_tab'] = 'Users';
$_lang['users'] = 'Remote Users';
$_lang['users_desc'] = 'Users present in the remote site';
$_lang['getusersfailed'] = 'Failed to get remote users';
$_lang['user_full_name'] = 'Full name';
$_lang['user_block'] = 'Blocked';
$_lang['import_user'] = 'Import';
$_lang['importusersfailed'] = 'Failed to import remote users';
$_lang['invaliduserid'] = 'The user id is invalid';
$_lang['failedtocreatelocaluser'] = 'Cannot create the local user';
$_lang['failedtocreatelocaluserprofile'] = 'Cannot create the local user profile';
$_lang['failedtocreatelocalfile'] = 'Cannot create the local file';
$_lang['failedtocreatelocalfolder'] = 'Cannot create the local folder';
$_lang['provisioner.desc'] = 'A component to allow local provisioning from a remote Revolution or Evolution site';
$_lang['importfilesfailed'] = 'File import failed - Remote: ';
/* 1.0 GA additions */
$_lang['revolution'] = 'Revolution';
$_lang['evolution'] = 'Evolution';
$_lang['remotesite'] = 'Remote site is';
$_lang['adminaction'] = 'Action';
$_lang['import_convert_resource'] = 'Import Convert Tags';
$_lang['account_details'] = 'Remote site account credentials and URL';
$_lang['remote_site'] = 'Remote site type';
$_lang['admin_action'] = 'Action';
$_lang['already_imported'] = ': Already imported?';
$_lang['user_type'] = 'Type';
$_lang['user_email'] = 'Email';
$_lang['importfoldernotwriteable'] = 'Provisioners Import folder is not writeable';
$_lang['importfilenolength'] = 'The imported file length is 0';
$_lang['admin_remember'] = 'Remember....';
$_lang['nositeid_error'] = 'No Remote Site Identifier supplied';
$_lang['menu_siteid'] = 'Remote Site Identifier';
$_lang['nocurl'] = 'Oops! The PHP CURL pacakge seems not to be present, please install it.';
$_lang['norevogateway'] = 'Revolution gateway package is not installed in remote Evolution site';
$_lang['admin_helptext'] = "<p>For Revolution sites the URL is the URL of the connectors directory.
                For Evolution sites it is the URL of the site itself.</p><p> The account used must be a manager account
                for Evolution sites and an account with sufficient privileges to perform view(aka list) operations for Revolution sites.</p>
                <p>A Remote Site Identifier must be supplied for Revolution sites.</p>
                <p>If in doubt please read the user guide.</p>";
