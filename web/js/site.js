(function () {
    $('.text-silver-chalice #accordion a').on('click', function (event) {
        event.preventDefault();
        $('#accordion').find('div').toggle();
    });
    share42('/js/');
    $(function () {
        $('.js-datetimepicker-form').datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY',
            useCurrent: false,
            tooltips: {
                today: "Сегодня",
                clear: "Clear selection",
                close: "Закрыть календарь",
                selectMonth: "Выберите месяц",
                prevMonth: "Предыдущий месяц",
                nextMonth: "Следующий месяц",
                selectYear: "Выберите год",
                prevYear: "Предыдущий год",
                nextYear: "Следующий год",
                selectDecade: "Выберите десятилетие",
                prevDecade: "Предыдущие десятилетие",
                nextDecade: "Следующие десятилетие",
                prevCentury: "Предыдущий век",
                nextCentury: "Следующий век",
                pickHour: "Pick Hour",
                incrementHour: "Increment Hour",
                decrementHour: "Decrement Hour",
                pickMinute: "Pick Minute",
                incrementMinute: "Increment Minute",
                decrementMinute: "Decrement Minute",
                pickSecond: "Pick Second",
                incrementSecond: "Increment Second",
                decrementSecond: "Decrement Second",
                togglePeriod: "Toggle Period",
                selectTime: "Выберите время"
            },
            icons: {

            },
            showTodayButton: true,
            dayViewHeaderFormat: 'MMMM YYYY',
        });
    });

    // $('img.img').each(function() {
    // 	var _this = $(this);
    // 	watermark([_this.attr('src')])
    // 		.image(watermark.text.lowerRight('Alandalus', '28px serif', '#fff', 0.5))
    // 		.then(function (img) {
    // 			var src_image = $(img).attr('src');
    // 			_this.removeAttr('src');
    // 			_this.attr('src', src_image);
    // 		});
    //
    // 	watermark([_this.attr('src'), '/images/logo-dark-250x76.png'])
    // 		.image(watermark.image.lowerLeft(1))
    // 		.then(function (img) {
    // 			var src_image = $(img).attr('src');
    // 			_this.removeAttr('src');
    // 			_this.attr('src', src_image);
    // 		});
    // });

    /** Получение дополнительных публикаций */
    $('.js-get-last-publication-posts').on('click', function () {
        let offset = $('.js-last-publication-offset').text();
        let data = {'limit': 8, 'offset': offset}
        $.ajax({
            type: 'POST',
            url: window.get_last_posts_request,
            timeout: 50000,
            dataType: 'html',
            data: data,
            beforeSend: function () {
                $('.js-spinner-last-publication').removeClass('last-publication-hidden');
                $('.js-get-last-publication-posts').addClass('last-publication-hidden');
            },
            success: function (responce) {
                let new_offset = parseInt(offset) + 8;
                let posts = $(responce).find('.js-block-last-publication').html();
                $('.js-block-last-publication').append(posts);
                $('.js-last-publication-offset').text(new_offset);
                $('.js-spinner-last-publication').addClass('last-publication-hidden');
                $('.js-get-last-publication-posts').removeClass('last-publication-hidden');
            },
            error: function (xhr) { // if error occured
                console.log(xhr.responseText);
            },
        });
    });
    $('.js-best-offers-again-show').on('click', function () {
        if ($('.js-block-offers-hidden').hasClass('hidden')) {
            $('.js-block-offers-hidden').removeClass('hidden');
            $(this).addClass('hidden');
        } else {
            $('.js-block-offers-hidden').addClass('hidden');
        }
    })
}());
