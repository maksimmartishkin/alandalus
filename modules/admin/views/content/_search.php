<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SiteContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'contentType') ?>

    <?= $form->field($model, 'pagetitle') ?>

    <?= $form->field($model, 'longtitle') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'alias') ?>

    <?php // echo $form->field($model, 'link_attributes') ?>

    <?php // echo $form->field($model, 'published') ?>

    <?php // echo $form->field($model, 'pub_date') ?>

    <?php // echo $form->field($model, 'unpub_date') ?>

    <?php // echo $form->field($model, 'parent') ?>

    <?php // echo $form->field($model, 'isfolder') ?>

    <?php // echo $form->field($model, 'introtext') ?>

    <?php // echo $form->field($model, 'content') ?>

    <?php // echo $form->field($model, 'richtext') ?>

    <?php // echo $form->field($model, 'template') ?>

    <?php // echo $form->field($model, 'menuindex') ?>

    <?php // echo $form->field($model, 'searchable') ?>

    <?php // echo $form->field($model, 'cacheable') ?>

    <?php // echo $form->field($model, 'createdby') ?>

    <?php // echo $form->field($model, 'createdon') ?>

    <?php // echo $form->field($model, 'editedby') ?>

    <?php // echo $form->field($model, 'editedon') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'deletedon') ?>

    <?php // echo $form->field($model, 'deletedby') ?>

    <?php // echo $form->field($model, 'publishedon') ?>

    <?php // echo $form->field($model, 'publishedby') ?>

    <?php // echo $form->field($model, 'menutitle') ?>

    <?php // echo $form->field($model, 'donthit') ?>

    <?php // echo $form->field($model, 'privateweb') ?>

    <?php // echo $form->field($model, 'privatemgr') ?>

    <?php // echo $form->field($model, 'content_dispo') ?>

    <?php // echo $form->field($model, 'hidemenu') ?>

    <?php // echo $form->field($model, 'class_key') ?>

    <?php // echo $form->field($model, 'context_key') ?>

    <?php // echo $form->field($model, 'content_type') ?>

    <?php // echo $form->field($model, 'uri') ?>

    <?php // echo $form->field($model, 'uri_override') ?>

    <?php // echo $form->field($model, 'hide_children_in_tree') ?>

    <?php // echo $form->field($model, 'show_in_tree') ?>

    <?php // echo $form->field($model, 'properties') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
