<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SiteContent */

$this->title = $model->longtitle;
$this->params['breadcrumbs'][] = ['label' => 'Site Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

	<div class="group group-lg">
		<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'button button-primary']) ?>
		<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'button button-primary',
			'style' => 'margin-top: 0;',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	</div>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
//            'type',
//            'contentType',
            'pagetitle',
//            'longtitle',
            'description',
            'alias',
//            'link_attributes',
			'introtext:ntext',
			[
				'attribute' => 'content',
				'value' => function($model){
					return $model->content;
				},
				'format' => 'raw',
			],
            'published',
            'pub_date',
            'unpub_date',
            'parent',
            'isfolder',
            'richtext',
//            'template',
            'menuindex',
            'searchable',
            'cacheable',
            'createdby',
            'createdon',
            'editedby',
            'editedon',
            'deleted',
            'deletedon',
            'deletedby',
            'publishedon',
            'publishedby',
            'menutitle',
            'donthit',
            'privateweb',
            'privatemgr',
            'content_dispo',
            'hidemenu',
//            'class_key',
//            'context_key',
//            'content_type',
            'uri:ntext',
			[
				'attribute' => 'uri',
				'value' => function($model){
					return Html::a(Url::to([$model->uri]), Url::to(['/'.$model->uri]));
				},
				'format' => 'raw',
			],
            'uri_override',
            'hide_children_in_tree',
            'show_in_tree',
            'properties:ntext',
        ],
    ]) ?>

