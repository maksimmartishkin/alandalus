<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiteContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контент';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-content-index">
    <p>
        <?= Html::a('Новый контент', ['create'], ['class' => 'button button-primary']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'type',
//            'contentType',
            'pagetitle',
            'longtitle',
            //'description',
            'alias',
            //'link_attributes',
            //'published',
            //'pub_date',
            //'unpub_date',
            //'parent',
            //'isfolder',
            //'introtext:ntext',
            //'content:ntext',
            //'richtext',
            //'template',
            //'menuindex',
            //'searchable',
            //'cacheable',
            //'createdby',
            //'createdon',
//            'editedby',
            //'editedon',
            //'deleted',
            //'deletedon',
            //'deletedby',
            'publishedon',
            //'publishedby',
            //'menutitle',
            //'donthit',
            //'privateweb',
            //'privatemgr',
            //'content_dispo',
            //'hidemenu',
            //'class_key',
            //'context_key',
            //'content_type',
            //'uri:ntext',
            //'uri_override',
            //'hide_children_in_tree',
            //'show_in_tree',
            //'properties:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
