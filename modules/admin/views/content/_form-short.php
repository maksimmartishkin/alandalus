<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SiteContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-content-form">

    <?php $form = ActiveForm::begin([
		'id' => 'content-form',
		//'enableClientValidation' => true,
		//'enableAjaxValidation' => true,
//		'id' => 'form-pages',
		'options' => [
			'class' => 'rd-mailform text-left',
			'data-form-output' => 'form-output-global',
//			'data-form-type' => 'content',
		],

	]); ?>

    <?//= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'contentType')->textInput(['maxlength' => true]) ?>
	<div class="row row-60 justify-content-sm-center text-left">
		<div class="col-md-12">
			<?= $form->field($model, 'content', [
				'options' => [
					'class' => 'form-wrap form-wrap-xs col-lg-12'
				],
			])->textarea([
				'rows' => 25,
			]) ?>
		</div>
		<div class="col-md-7">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'pagetitle', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
//		'template' => '
//			<div class="form-wrap form-wrap-xs">
//				{label}
//				{input}
//			</div>
//		'
				])->textInput(['maxlength' => true, 'class' => 'form-input', 'data-constraints' => '@Required']); ?>
			</div>
			<hr class="hr bg-gallery" style="margin-top: 25px;">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'longtitle', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->textInput(['maxlength' => true, 'class' => 'form-input']); ?>
			</div>
			<hr class="hr bg-gallery" style="margin-top: 25px;">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'description', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->textarea(['maxlength' => true, 'class' => 'form-input', 'style' => 'height: 150px;']) ?>
			</div>
			<hr class="hr bg-gallery" style="margin-top: 25px;">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'introtext', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->textarea(['rows' => 6, 'class' => 'form-input', 'style' => 'height: 150px;']) ?>
			</div>
		</div>
		<div class="col-md-5">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'alias', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->textInput(['maxlength' => true, 'class' => 'form-input']) ?>
			</div>
			<hr class="hr bg-gallery" style="margin-top: 25px;">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'menutitle', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->textInput(['maxlength' => true, 'class' => 'form-input']) ?>
			</div>
			<hr class="hr bg-gallery" style="margin-top: 25px;">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'published', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->checkbox([
					'checked' => $model->isNewRecord?true:($model->published == 1?true:false)
				]) ?>
			</div>
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'hidemenu', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->checkbox([
					'checked' => $model->isNewRecord?false:($model->hidemenu == 1?true:false)
				]) ?>
			</div>
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'searchable', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->checkbox([
					'checked' => $model->isNewRecord?true:($model->searchable == 1?true:false)
				]) ?>
			</div>
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'isfolder', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->checkbox([
					'checked' => $model->isNewRecord?true:($model->isfolder == 1?true:false)
				]) ?>
			</div>
			<hr class="hr bg-gallery">
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'parent', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->textInput(['class' => 'form-input'])?>
			</div>
<!--			<div class="box-socials unit flex-sm-row unit-spacing-md">-->
<!--				--><?//= $form->field($model, 'parent', [
//					'options' => [
//						'class' => 'form-wrap form-wrap-xs col-lg-12'
//					],
//				])->dropDownList($model->parent, [
////					'prompt' => '',
//					'class' => 'form-input'
//				])?>
<!--			</div>-->
			<div class="box-socials unit flex-sm-row unit-spacing-md">
				<?= $form->field($model, 'menuindex', [
					'options' => [
						'class' => 'form-wrap form-wrap-xs col-lg-12'
					],
				])->textInput(['maxlength' => true, 'class' => 'form-input']);?>
			</div>
		</div>
		<hr class="hr bg-gallery" style="margin-top: 25px;">
<!--		<div class="form-wrap form-wrap-xs">-->
<!--			<label>-->
<!--				<input type="checkbox" id="is-vusual-editor-on" value="1" checked="checked" class="checkbox-custom">-->
<!--				<span class="checkbox-custom-dummy"></span> Использовать визуальный редактор-->
<!--			</label>-->
<!--		</div>-->
		<div class="col-lg-12">
			<?= $form->field($model, 'uri', [
				'options' => [
					'class' => 'form-wrap form-wrap-xs col-lg-12'
				],
			])->textInput(['maxlength' => true, 'class' => 'form-input']); ?>




		</div>
	</div>


	<div class="col-md-5">

	</div>
	<hr class="hr bg-gallery" style="margin-top: 25px;">
<!--	<div class="form-wrap form-wrap-xs">-->
<!--		<label class="form-label" for="questions-contact-name">Your Name</label>-->
<!--		<input class="form-input" id="questions-contact-name" type="text" name="name" data-constraints="@Required">-->
<!--	</div>-->

<!--    --><?//= $form->field($model, 'longtitle')->textInput(['maxlength' => true]) ?>


<!--    --><?//= $form->field($model, 'pub_date')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'unpub_date')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'richtext')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'template')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'cacheable')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'createdby')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'createdon')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'editedby')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'editedon')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'deleted')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'deletedon')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'deletedby')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'publishedon')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'publishedby')->textInput() ?>
<!---->
<!---->
<!--    --><?//= $form->field($model, 'donthit')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'privateweb')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'privatemgr')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'context_key')->textInput(['maxlength' => true]) ?>
<!---->
<!---->
<!--    --><?//= $form->field($model, 'uri_override')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'hide_children_in_tree')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'show_in_tree')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'properties')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'button button-primary btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
//$this->registerJsFile('https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js', ['depends' => \yii\web\YiiAsset::className()]);
//$this->registerJsFile('/js/tinymce/tinymce.min.js', ['depends' => \yii\web\YiiAsset::className()]);
//$this->registerJsFile('/js/tinymce1/jquery.tinymce.min.js', ['depends' => \yii\web\YiiAsset::className()]);
$this->registerJs(<<<JS
	tinymce.init({
		selector: 'textarea#sitecontent-content',
		height: 600,
		language: 'ru',
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table contextmenu paste code'
        ],
        // relative_urls: false,
	});
	$('#is-vusual-editor-on').on('click', function() {
	    $('#mceu_38-text').click();
	    // $('#mceu_13').toggle();
	    // $('#sitecontent-content').toggle();
	});

JS
);
?>