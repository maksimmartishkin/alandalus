<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SiteContent */

$this->title = 'Update Site Content: ' . $model->longtitle;
$this->params['breadcrumbs'][] = ['label' => 'Site Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->longtitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

