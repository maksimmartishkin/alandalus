<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SiteContent */

$this->title = 'Create Site Content';
$this->params['breadcrumbs'][] = ['label' => 'Site Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
