<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title', [
			'options' => [
				'class' => 'col-md-6'
			],
			'template' => '
				<div class="j-input">
					{label}
					{input}
				</div>
			'
		])->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'link', [
		'options' => [
			'class' => 'col-md-6'
		],
		'template' => '
				<div class="j-input">
					{label}
					{input}
				</div>
			'
	])->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'title_menu', [
		'options' => [
			'class' => 'col-md-6'
		],
		'template' => '
				<div class="j-input">
					{label}
					{input}
				</div>
			'
	])->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'public', [
		'options' => [
			'class' => 'col-md-3',
			'style' => 'margin-top: 35px;',
		],
	])->checkbox([
//		'class' => 'js-single',

		'checked' => $model->isNewRecord?true:($model->public == 1?true:false)
	]) ?>

	<?= $form->field($model, 'hide_menu', [
		'options' => [
			'class' => 'col-md-3',
			'style' => 'margin-top: 35px;',
		],
	])->checkbox([
//		'class' => 'js-single',
		'checked' => $model->isNewRecord?false:($model->public == 0?true:false)
	]) ?>

	<?= $form->field($model, 'description', [
		'options' => [
			'class' => 'col-md-6'
		],
		'template' => '
				<div class="j-input">
					{label}
					{input}
				</div>
			'
	])->textarea(['rows' => 3]) ?>

	<?= $form->field($model, 'annotation', [
		'options' => [
			'class' => 'col-md-6'
		],
		'template' => '
				<div class="j-input">
					{label}
					{input}
				</div>
			'
	])->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'content', [
		'options' => [
			'class' => 'col-md-12'
		],
		'template' => '
				<div class="j-input">
					{label}
					{input}
				</div>
			'
	])->textarea(['rows' => 10]) ?>

<!--    --><?//= $form->field($model, 'create')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'update')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'public')->textInput() ?>

    <div class="col-xs-12 form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
