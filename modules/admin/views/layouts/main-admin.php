<?php


use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\admin\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	<style>
		.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}
		.row {
			display: flex;
			flex-wrap: wrap !important;
		}
	</style>
</head>
<body>
<?php $this->beginBody() ?>
<div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
<!--<div class="preloader">-->
<!--	<div class="preloader-body">-->
<!--		<div class="cssload-container">-->
<!--			<div class="cssload-speeding-wheel"></div>-->
<!--		</div>-->
<!--		<p>Loading...</p>-->
<!--	</div>-->
<!--</div>-->
<!-- Page-->
<div class="page text-center">
	<!-- Page Header-->
	<header class="page-header slider-menu-position">
		<!-- RD Navbar-->
		<div class="rd-navbar-wrap">
			<nav class="rd-navbar rd-navbar-transparent rd-navbar-right-side rd-navbar-right-side-contacts rd-navbar-right-side-contacts-mobile rd-navbar-dark-stuck rd-navbar-overlay" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fullwidth" data-xl-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-lg-stick-up-offset="1px" data-xl-stick-up-offset="1px" data-xxl-stick-up-offset="1px">
				<div class="rd-navbar-inner">
					<!-- RD Navbar Panel-->
					<div class="rd-navbar-panel">
						<!-- RD Navbar Toggle-->
						<button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
						<!-- RD Navbar Brand-->
						<div class="rd-navbar-brand rd-navbar-brand-desktop"><a class="brand-name" href="<?=Url::to(['/admin'])?>"><img width="148" height="30" src="/images/logo-dark-250x76.png" alt=""></a></div>
						<!-- RD Navbar Brand-->
						<div class="rd-navbar-brand rd-navbar-brand-mobile"><a class="brand-name" href="<?=Url::to(['/admin'])?>"><img width="148" height="30" src="/images/logo-dark-250x76.png" alt=""></a></div>
						<!-- RD Navbar Toggle-->
						<button class="rd-navbar-collapse" data-rd-navbar-toggle=".rd-navbar-right-side-contacts"><span></span></button>
					</div>
					<div class="rd-navbar-right-side-wrap">
						<div class="rd-navbar-nav-wrap">
							<!-- RD Navbar Nav-->
							<ul class="rd-navbar-nav">
								<li><a href="<?=Url::to(['/'])?>">На сайт</a></li>
								<li><a href="<?=Url::to(['/admin/content'])?>">Контент</a></li>
<!--								<li><a href="about.html">About</a>-->
									<!-- RD Navbar Dropdown-->
<!--									<ul class="rd-navbar-dropdown">-->
<!--										<li><a href="our-team.html">Our Team</a></li>-->
<!--										<li><a href="faq.html">FAQ</a></li>-->
<!--										<li><a href="testimonials.html">Testimonials</a></li>-->
<!--									</ul>-->
<!--								</li>-->
<!--								<li><a href="#">Pages</a>-->
									<!-- RD Navbar Megamenu-->
<!--									<div class="rd-navbar-megamenu">-->
<!--										<div class="row">-->
<!--											<div class="col-lg-4">-->
<!--												<p class="rd-megamenu-header text-big text-black text-ubold">Pages 1</p>-->
<!--												<ul class="rd-megamenu-list">-->
<!--													<li><a href="press.html">Press</a></li>-->
<!--													<li><a href="services.html">Services</a></li>-->
<!--													<li><a href="pricing.html">Pricing</a></li>-->
<!--													<li><a href="destinations.html">Destinations</a></li>-->
<!--													<li><a href="signup.html">Sign Up</a></li>-->
<!--													<li><a href="signup-variant-2.html">Sign Up v2</a></li>-->
<!--												</ul>-->
<!--											</div>-->
<!--											<div class="col-lg-4">-->
<!--												<p class="rd-megamenu-header text-big text-black text-ubold">Pages 2</p>-->
<!--												<ul class="rd-megamenu-list">-->
<!--													<li><a href="login.html">Login</a></li>-->
<!--													<li><a href="forgot-password.html">Forgot Password</a></li>-->
<!--													<li><a href="privacy.html">Privacy Policy</a></li>-->
<!--													<li><a href="terms-of-use.html">Terms Of Use</a></li>-->
<!--													<li><a href="sitemap.html">Sitemap</a></li>-->
<!--													<li><a href="search-results.html">Search Results</a></li>-->
<!--												</ul>-->
<!--											</div>-->
<!--											<div class="col-lg-4">-->
<!--												<p class="rd-megamenu-header text-big text-black text-ubold">Pages 3</p>-->
<!--												<ul class="rd-megamenu-list">-->
<!--													<li><a href="404.html">404</a></li>-->
<!--													<li><a href="503.html">503</a></li>-->
<!--													<li><a href="comingsoon.html">Comingsoon</a></li>-->
<!--													<li><a href="maintenance.html">Maintenance</a></li>-->
<!--													<li><a href="underconstruction.html">Underconstruction</a></li>-->
<!--												</ul>-->
<!--											</div>-->
<!--										</div>-->
<!--									</div>-->
<!--								</li>-->
<!--								<li><a href="#">Gallery</a>-->
									<!-- RD Navbar Dropdown-->
<!--									<ul class="rd-navbar-dropdown">-->
<!--										<li><a href="gallery-cobbles.html">Gallery Cobbles</a></li>-->
<!--										<li><a href="gallery-fullwidth.html">Gallery Fullwidth</a></li>-->
<!--										<li><a href="gallery-grid.html">Gallery Grid</a></li>-->
<!--										<li><a href="gallery-masonry.html">Gallery Masonry</a></li>-->
<!--									</ul>-->
<!--								</li>-->
<!--								<li><a href="#">Blog</a>-->
									<!-- RD Navbar Dropdown-->
<!--									<ul class="rd-navbar-dropdown">-->
<!--										<li><a href="blog-grid.html">Blog Grid</a></li>-->
<!--										<li><a href="blog-grid-sidebar-left.html">Blog Grid Sidebar</a></li>-->
<!--										<li><a href="blog-list.html">Blog List</a></li>-->
<!--										<li><a href="blog-list-sidebar-left.html">Blog List Sidebar</a></li>-->
<!--										<li><a href="blog-list-variant-2.html">Blog List v2</a></li>-->
<!--										<li><a href="blog-list-variant-2-sidebar-left.html">Blog List v2 Sidebar</a></li>-->
<!--										<li><a href="blog-masonry.html">Blog Masonry</a></li>-->
<!--										<li><a href="blog-modern.html">Blog Modern</a></li>-->
<!--										<li><a href="blog-single-post.html">Blog Single Post</a></li>-->
<!--										<li><a href="blog-single-post-sidebar-left.html">Blog Single Post Sidebar</a></li>-->
<!--									</ul>-->
<!--								</li>-->
<!--								<li><a href="contacts.html">Contacts</a>-->
									<!-- RD Navbar Dropdown-->
<!--									<ul class="rd-navbar-dropdown">-->
<!--										<li><a href="contacts.html">Contacts</a></li>-->
<!--										<li><a href="contacts-variant-2.html">Contacts v2</a></li>-->
<!--									</ul>-->
<!--								</li>-->
							</ul>
						</div>
						<div class="divider-vertical bg-default-light d-none d-lg-inline-block"></div>
						<div class="rd-navbar-right-side-contacts">
							<!-- List Inline-->
<!--							<ul class="list-inline list-inline-13">-->
<!--								<li><a class="text-black" href="tel:#"><img class="img-semi-transparent-inverse d-inline-block" src="images/icons/icon-12-19x19.png" width="19" height="19" alt=""></a></li>-->
<!--								<li><a class="text-black" href="mailto:#"><img class="img-semi-transparent-inverse d-inline-block" src="images/icons/icon-13-20x13.png" width="20" height="13" alt=""></a></li>-->
<!--								<li><a class="text-black" href="#"><img class="img-semi-transparent-inverse d-inline-block" src="images/icons/icon-14-19x19.png" width="19" height="19" alt=""></a></li>-->
<!--							</ul>-->
						</div>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!-- Swiper-->

	<section class="section parallax-container bg-black section-height-mac" data-parallax-img="/images/background/background-23-1920x900.jpg">
		<div class="parallax-content">
			<div class="bg-overlay-darker">
				<div class="container section-34 section-md-60 section-lg-115">
					<h1 class="d-none d-lg-inline-block text-white"><?=$this->title?></h1>
				</div>
			</div>
		</div>
	</section>


	<section class="section-80 bg-wild-wand text-md-left">
		<div class="container-admin">
			<div class="row justify-content-sm-center row-50">
				<div class="col-md-11 col-lg-9 order-lg-1">
					<!-- Box-->
					<div class="box box-lg box-single-post bg-default d-block">
						<h4 class="text-ubold"><?=$this->title?></h4>
						<div class="text-silver-chalice text-left">

<!--							--><?//= Breadcrumbs::widget([
//								'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//							]) ?>
							<?= Alert::widget() ?>
							<?= $content ?>



						</div>
						<hr class="hr bg-alto">


					</div>
				</div>
				<div class="col-md-11 col-lg-3 text-lg-left">
					<?php echo Yii::$app->controller->renderPartial("@app/modules/admin/views/layouts/side-menu");?>
				</div>

			</div>
		</div>
	</section>



	<!-- Page Footer-->
	<footer class="page-footer footer-variant-3 section-top-80 section-bottom-34 section-lg-top-70 section-lg-bottom-15">
		<div class="container">
			<hr class="hr bg-gallery">
			<div class="row row-20 justify-content-sm-center justify-content-lg-between">
				<div class="col-md-8 col-lg-3 col-xl-2 text-lg-left"><a href="index.html"><img width="110" height="22" src="/images/logo-dark-250x76.png" alt=""></a></div>
				<div class="col-md-8 col-lg-6 col-xl-8 text-lg-center">
					<p class="text-extra-small text-black">
						Copyright &#169;<span class="copyright-year"></span><a href="./"> SunTravel</a>. All Rights Reserved.
						Design&nbsp;by&nbsp;<a href="https://zemez.io/">Zemez</a>
					</p>
				</div>
				<div class="col-md-8 col-lg-3 col-xl-2 text-lg-right">
					<!-- List Inline-->
					<ul class="list-inline list-inline-13">
						<li class="text-center"><a class="icon fa fa-facebook-f text-gray" href="#"></a></li>
						<li class="text-center"><a class="icon fa fa-twitter text-gray" href="#"></a></li>
						<li class="text-center"><a class="icon fa fa-youtube text-gray" href="#"></a></li>
						<li class="text-center"><a class="icon fa fa-linkedin text-gray" href="#"></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"> </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
