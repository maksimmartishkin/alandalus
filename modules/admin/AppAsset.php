<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Lato:400,400i,700,700i,900,900i',
        'css/bootstrap.css',
        'css/fonts.css',
		'css/style.css',
        'css/site.css',
    ];
    public $js = [
//    	'https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js',
    	'js/tinymce/tinymce.min.js',
//    	'js/tinymce1/jquery.tinymce.min.js',
    	'js/core.min.js',
		'js/script.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

	public function init()
	{
		parent::init();
	}
}
