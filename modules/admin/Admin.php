<?php

namespace app\modules\admin;

use Yii;
use app\models\SiteContent;
use yii\helpers\Url;

/**
 * admin module definition class
 */
class Admin extends \yii\base\Module
{

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
		$this->layout = 'main-admin.php';
        parent::init();

        // custom initialization code goes here
    }

	/**
	 * Конвертирование полученных данных
	 * @param $content
	 *
	 * @return bool
	 */
	public static function getConvertSiteContent($content)
	{
//		var_dump($content);die();
		if($content === null) {
			return false;
		}
		$new_content = $content->content;
//		if (strpos($new_content, '[[$form]]') !== false) {
//			$new_content = str_ireplace('[[$form]]', Yii::$app->controller->renderPartial('@app/views/site/form-question'), $new_content, $count);
//		} elseif (strpos($new_content, '[[$andalucia_faktyi]]') !== false) {
//			$fakty = SiteContent::findFacts();
//			$new_content = str_ireplace('[[$andalucia_faktyi]]', Yii::$app->controller->renderPartial('@app/views/site/andalucia_factyi', ['fakty' => $fakty]), $new_content, $count);
//		} elseif (strpos($new_content, '[[$info_list]]') !== false) {
//			$info_list = SiteContent::findInfoList();
//			$new_content = str_ireplace('[[$info_list]]', Yii::$app->controller->renderPartial('@app/views/site/info_list', ['info_list' => $info_list]), $new_content, $count);
//		} elseif (strpos($new_content, '[[$slovar_terminov]]') !== false) {
//			$slovar_terminov = SiteContent::findSlovarTerminov();
//			$new_content = str_ireplace('[[$slovar_terminov]]', Yii::$app->controller->renderPartial('@app/views/site/slovar_terminov', ['slovar_terminov' => $slovar_terminov]), $new_content, $count);
//		} elseif (strpos($new_content, '[[$ACII]]') !== false) {
//			$akczii = SiteContent::findAccii();
//			$new_content = str_ireplace('[[$ACII]]', Yii::$app->controller->renderPartial('@app/views/site/akczii', ['akczii' => $akczii]), $new_content, $count);
//		} elseif (strpos($new_content, '[[$dittonews]]') !== false) {
//			$dittonews = SiteContent::findDittoNews();
//			$new_content = str_ireplace('[[$dittonews]]', Yii::$app->controller->renderPartial('@app/views/site/ditto-news', ['dittonews' => $dittonews]), $new_content, $count);
//		} elseif (strpos($new_content, '[[$googleMapsScript]]') !== false) {
////			$new_content = str_ireplace('[[$googleMapsScript]]', '24234', $new_content, $count);
//			$new_content = str_ireplace('[[$googleMapsScript]]', Yii::$app->controller->renderPartial('@app/views/site/googleMapsScript'), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5261` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto?&amp;parents=`', '`&srv=`1`]]'], ['', ''], '[[!baza_ditto?&amp;parents=`5261`&srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5261` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5262` &srv=`1` ]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &srv=`1` ]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5262` &srv=`1` ]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5262` &srv=`1` ]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5246` &amp;price_freq_baza=`sale` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &amp;price_freq_baza=`sale` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5246` &amp;price_freq_baza=`sale` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5246` &amp;price_freq_baza=`sale` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5262,5264,5265` &srv=`1` ]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &srv=`1` ]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5262,5264,5265` &srv=`1` ]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5262,5264,5265` &srv=`1` ]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5265` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5265` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5265` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5264` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5264` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5264` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5248` &amp;price_freq_baza=`sale` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &amp;price_freq_baza=`sale` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5248` &amp;price_freq_baza=`sale` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5248` &amp;price_freq_baza=`sale` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5247` &amp;price_freq_baza=`sale` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &amp;price_freq_baza=`sale` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5247` &amp;price_freq_baza=`sale` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5247` &amp;price_freq_baza=`sale` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5245` &amp;price_freq_baza=`sale` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &amp;price_freq_baza=`sale` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5245` &amp;price_freq_baza=`sale` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5245` &amp;price_freq_baza=`sale` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5244` &amp;price_freq_baza=`sale` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &amp;price_freq_baza=`sale` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5244` &amp;price_freq_baza=`sale` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5244` &amp;price_freq_baza=`sale` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5243` &amp;price_freq_baza=`sale` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &amp;price_freq_baza=`sale` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5243` &amp;price_freq_baza=`sale` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5243` &amp;price_freq_baza=`sale` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!baza_ditto? &amp;parents=`5242` &amp;price_freq_baza=`sale` &srv=`1`]]') !== false) {
//			$parent = str_replace(['[[!baza_ditto? &amp;parents=`', '` &amp;price_freq_baza=`sale` &srv=`1`]]'], ['', ''], '[[!baza_ditto? &amp;parents=`5242` &amp;price_freq_baza=`sale` &srv=`1`]]');
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!baza_ditto? &amp;parents=`5242` &amp;price_freq_baza=`sale` &srv=`1`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		} elseif (strpos($new_content, '[[!special:default=`[[$random_object]]`]]') !== false) {
//			$parent = ['5248', '5247', '5245', '5244', '5243', '5242'];
//			$baza = SiteContent::getBazaDitto($parent);
//			$new_content = str_ireplace('[[!special:default=`[[$random_object]]`]]', Yii::$app->controller->renderPartial('@app/views/site/baza_ditto', ['baza' => $baza]), $new_content, $count);
//		}
/*		if (preg_match('/<ul .*? id="mygallerynew" .*?>(.*?)<\/ul>/uxi', $new_content, $findmygallerynew)) {*/
//			if(preg_match('/\[\[(.*?)\]\]/uxi', $findmygallerynew[1], $mygallerynew)) {
//				$galleryview = $mygallerynew[1];
//				$album_list = false;
//				if (preg_match('/\&album=`(.*)`\]\]/ui', $findmygallerynew[1], $match_gallery)) {
////					var_dump($match_gallery);
//					$id_gallery_album = $match_gallery[1];
//					$album_list = $query = Yii::$app->db->createCommand("SELECT i.`name`, i.`filename`
//						FROM `gallery_album_items` ai
//						LEFT JOIN `gallery_items` i ON ai.`item`=i.`id`
//						WHERE ai.`album`=:album AND i.`active`", [':album' => $id_gallery_album])->queryAll();
////					var_dump($id_gallery_album, $album_list);die();
//				}
//				$new_content = str_ireplace($findmygallerynew, Yii::$app->controller->renderPartial('@app/views/site/'.$galleryview, ['album_list' => $album_list]), $new_content, $count);
//			}
////			if (preg_match('/\&album=`(.*)`\]\]/ui', $findmygallerynew[1], $match_gallery)) {
////
////				var_dump($match_gallery);
////			}
////
////			var_dump($mygallerynew);
//		}
//		if (preg_match('/\&album=`(.*)`\]\]/ui', $new_content, $match_gallery)) {
//
//			var_dump($match_gallery);
//		}
		/*'/<ul .*? id="mygallerynew" .*?>[\S\s]*?<\/ul>/ix'
		'/<ul id=\"mygallerynew\">([^"]+)"</ul>/'*/
		$find_gallery = preg_match_all('/<ul .*? id="mygallerynew" .*?>(.*?)<\/ul>/ui', $new_content, $match);
		$find_gallery_2 = preg_match('/\&album=`(.*)`\]\]/uxi', $new_content, $match2);
//		var_dump($match, $match2, strpos($new_content, '<ul id="mygallerynew">[[!Gallery? &amp;album=`403`]]</ul>') !== false);die();
//		$content_ar = explode(' ', $new_content);
//		$new_content = [];
//		$count = 1;
//		foreach ($content_ar as $value) {
////			var_dump($value, preg_match('%src=\"([^\"])+(png|jpg|gif)\"%i', $value, $match));//die();
//			if(preg_match('/href="([^"]+)"/', $value, $match)) {
//				$links = str_replace('.html', '', $match[1]);
////				var_dump($links);
//				if ($links == '#') {
//					$new_content[] = $value;
//				} elseif ($links == 'http://alandalus.ru/' || $links == 'http://alandalus.ru') {
//					$new_content[] = $value;
//				} elseif (preg_match("/~([0-9]+)/", $links, $match)) {
////					var_dump($links, $match);die();
////					$int_link = implode('', $match[0]);
//					$int_link = $match[1];
//					$hash_link = '';
//					$find_link = SiteContent::findIdToUri($int_link);
//					if($is_hash_link = explode('#', $links)) {
//						if(count($is_hash_link) > 1) {
//							$hash_link = '#'. $is_hash_link[1];
//						}
//					}
//					if ($find_link !== null) {
//						$new_content[] = str_replace($links, str_replace('/admin', '', Url::to([$find_link]).$hash_link), $value, $count);
//					}
//				} else {
//					$new_content[] = str_replace($links, str_replace('/admin', '', Url::to([$links])), $value, $count);
//				}
//
////			} elseif (preg_match('%src=\"([^\"])+(png|jpg|gif)\"%i', $value, $match)) {
//			} elseif (preg_match('/src="([^"]+)"/', $value, $match)) {
//				if (strpos($match[1], '/assets') === false && preg_match('%(png|jpg|gif)%i', $match[1], $match_img)) {
////						var_dump($match[1]);
//					$new_content[] = str_ireplace($match[1], str_replace('/admin', '', Url::to([$match[1]])), $value, $count);
////					var_dump( strpos($match[1], '/assets') !== false, str_ireplace('assets', '/assets', $value, $count));//die();
//				} else {
//					$new_content[] = $value;
//				}
////			$new_content = str_ireplace('assets', '/assets', $new_content);
////			echo '<pre>';print_r($new_content);die();
////		} elseif (strpos($new_content, '/assets') === false) {
////			var_dump(3453);die();
////			$new_content = str_ireplace('assets', '/assets', $new_content, $count);
////		} elseif (strpos($new_content, 'Стр.: [[!+page.nav]]') === false) {
//////			var_dump(3453);die();
////			$new_content = str_ireplace('Стр.: [[!+page.nav]]', '', $new_content, $count);
//			} else {
//				$new_content[] = $value;
//			}
////			if (preg_match('/src="([^"]+)"/', $value, $match)) {
//////				var_dump(Url::to([$match[1]]));
////				if (strpos($match[1], '/assets') === false) {
////					$new_content[] = str_ireplace($match[1], Url::to([$match[1]]), $value, $count);
//////					var_dump( strpos($match[1], '/assets') !== false, str_ireplace('assets', '/assets', $value, $count));//die();
////				}
//////			$new_content = str_ireplace('assets', '/assets', $new_content);
//////			echo '<pre>';print_r($new_content);die();
//////		} elseif (strpos($new_content, '/assets') === false) {
//////			var_dump(3453);die();
//////			$new_content = str_ireplace('assets', '/assets', $new_content, $count);
//////		} elseif (strpos($new_content, 'Стр.: [[!+page.nav]]') === false) {
////////			var_dump(3453);die();
//////			$new_content = str_ireplace('Стр.: [[!+page.nav]]', '', $new_content, $count);
////			}
//		}
//		$search_ar = [
//			'.html',
//			'Стр.: [[!+page.nav]]',
//			'[[galleryview500x350]]',
////			'[[!baza_ditto? &amp;parents=`5261` &srv=`1`]]'
//		];
//		$needle_ar = [
//			'',
//			'',
//			'',
////			''
//		];
//		$new_content = implode(' ', $new_content);
//		echo '<pre>';print_r($new_content);die();

//		echo '<pre>';print_r($new_content);die();
		$content->content = str_replace($search_ar, $needle_ar, $new_content);
		return $content;
	}

}
