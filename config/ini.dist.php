<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
defined('DB_HOST') or define('DB_HOST', '');
defined('DB_NAME') or define('DB_NAME', 'alandalus');
defined('DB_USERNAME') or define('DB_USERNAME', '');
defined('DB_PASSWORD') or define('DB_PASSWORD', '');
defined('DB_CHARSET') or define('DB_CHARSET', '');
