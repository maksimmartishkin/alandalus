<?php

require __DIR__ . '/ini.php';
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$url_rules = require __DIR__ . '/url-rules-new.php';

$config = [
	'id' => 'basic',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'sourceLanguage' =>'en-US',
	'language' => 'ru-RU',
	'name'=>'Alandalus',
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'defaultRoute' => 'main',
	'components' => [
//		'assetManager' => [
//			'appendTimestamp' => true,
//			'bundles' => [
//				'all' => [
//					'class' => 'yii\web\AssetBundle',
//					'basePath' => '@webroot/assets',
//					'baseUrl' => '@web/assets',
//					'css' => ['all-xyz.css'],
//					'js' => ['all-xyz.js'],
//				],
//				'A' => ['css' => [], 'js' => [], 'depends' => ['all']],
//				'B' => ['css' => [], 'js' => [], 'depends' => ['all']],
//				'C' => ['css' => [], 'js' => [], 'depends' => ['all']],
//				'D' => ['css' => [], 'js' => [], 'depends' => ['all']],
//			],
//		],
		'request' => [
			'cookieValidationKey' => 'lkljfsldkjfjhfahfLKHKJjbcvYTUG&756',
			'baseUrl'=> '',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
			'cachePath' => CACHE_PATH,
			'dirMode' => DIR_MODE,
			'defaultDuration' => CACHE_DURATION,
//			'cacheFileSuffix' => '.php'
		],
		'user' => [
			'identityClass' => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'main/error',
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => USE_FILE_TRANSPORT_MAIL,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => HOST_MAIL,  // e.g. smtp.mandrillapp.com or smtp.gmail.com
				'username' => USER_NAME_MAIL,
				'password' => PASSWORD_MAIL,
				'port' => '465', // Port 25 is a very common port too
				'encryption' => 'ssl', // It is often used, check your provider or mail server specs
			]        ],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => $db,

		'reCaptcha' => [
			'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
			'siteKeyV2' => RECAPTCHA_SITE_KEY_V2,
			'secretV2' => RECAPTCHA_SECRET_V2,
			'siteKeyV3' => RECAPTCHA_SITE_KEY_V3,
			'secretV3' => RECAPTCHA_SECRET_V3,
		],

		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
//			'suffix' => '/',
			'rules' => $url_rules,
			'normalizer' => [
				'class' => 'yii\web\UrlNormalizer',
				'normalizeTrailingSlash' => true,
				'collapseSlashes' => true,
			],
		],
	],
	'params' => $params,
	'modules' => [
		'admin' => [
			'class' => 'app\modules\admin\Admin',
		],
	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		'allowedIPs' => ['127.0.0.1', '::1', '213.87.*.*', '192.168.56.101', '192.168.56.1'],
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		'allowedIPs' => ['127.0.0.1', '::1', '213.87.*.*', '192.168.56.101', '192.168.56.1'],
	];
}

return $config;
