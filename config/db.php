<?php

return [
	'class' => 'yii\db\Connection',
	'dsn' => 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME,
	'emulatePrepare' => true,
	'username' => DB_USERNAME,
	'password' => DB_PASSWORD,
	'charset' => DB_CHARSET,

	// Schema cache options (for production environment)
	'enableSchemaCache' => true,
	'schemaCacheDuration' => defined('CACHE_DURATION') ? CACHE_DURATION : 3600,
	'schemaCache' => 'cache',
];
