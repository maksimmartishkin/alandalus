<?php

require __DIR__ . '/ini.php';
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$url_rules = require __DIR__ . '/url-rules-new.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => USE_FILE_TRANSPORT_MAIL,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => HOST_MAIL,  // e.g. smtp.mandrillapp.com or smtp.gmail.com
				'username' => USER_NAME_MAIL,
				'password' => PASSWORD_MAIL,
				'port' => '465', // Port 25 is a very common port too
				'encryption' => 'ssl', // It is often used, check your provider or mail server specs
			]
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			//			'suffix' => '/',
			'rules' => $url_rules,
//			'normalizer' => [
//				'class' => 'yii\web\UrlNormalizer',
//				'normalizeTrailingSlash' => true,
//				'collapseSlashes' => true,
//			],
		],
		'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
