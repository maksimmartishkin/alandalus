<?php
return [
	[
		'pattern'=>'/',
		'route' => 'site/index',
		'suffix'=>'/'
	],
	[
		'pattern'=>'contact',
		'route' => 'site/contact',
		'suffix'=>'/'
	],
	[
		'pattern'=>'search-baza',
		'route' => 'site/search-baza',
		'suffix'=>'/'
	],
	'search-result' => 'site/search-result',

	'get-last-posts-request' => 'site/get-last-posts-request',

	/** Подтверждение подписка на статьи */
	'subscribe-confirm' => 'site/subscribe-confirm',

	'menu-tree' => 'site/menu-tree',

	/**
	 * sitemap.xml
	 */
	[
		'pattern'=>'sitemap.xml',
		'route' => 'site/sitemapxml',
		'suffix'=>''
	],

//	'/robot.txt' => 'site/robot',

	'form-question' => 'site/form-question',
	'form-main-question' => 'site/form-main-question',
	'form-rent-real-estate' => 'site/form-rent-real-estate',
	'form-excursion-guides' => 'site/form-excursion-guides',
	'yachts-rental-window2' => 'site/yachts-rental-window2',
	'form-rent-car' => 'site/form-rent-car',
	[
		'pattern'=>'search-query-for-site',
		'route' => 'site/search-query-for-site',
		'suffix'=>'/'
	],
	[
		'pattern'=>'login',
		'route' => 'site/login',
		'suffix'=>'/'
	],
	[
		'pattern'=>'images-gallery',
		'route' => 'site/images-gallery',
		'suffix'=>''
	],
	[
		'pattern'=>'yachts-rental-window',
		'route' => 'site/yachts-rental-window2',
		'suffix'=>''
	],

	/**
	 * Страница для тестирования
	 */
	'test-page' => 'site/test-page',


	/**
	 * Разделы
	 */

	[
		'pattern'=>'<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>/<razdel4:[a-zA-Z0-9\-_()\.]*>',
		'route' => 'site/sections',
		'suffix'=>'/'
	],

	[
		'pattern'=>'<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>',
		'route' => 'site/sections',
		'suffix'=>'/'
	],

	[
		'pattern'=>'<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>',
		'route' => 'site/sections',
		'suffix'=>'/'
	],

	[
		'pattern'=>'<razdel1:[a-zA-Z0-9\-_\.]*>',
		'route' => 'site/sections',
		'suffix'=>'/'
	],




	//Админка 
	[
		'class' => 'yii\web\GroupUrlRule',
		'prefix' => 'admin',
		'routePrefix' => 'admin',
		'rules' => [
			'/' => 'content/index',
			'<content:[a-zA-Z0-9\-_]*>' => 'content/index',
		]
	],



];