<?php
return [
	'/' => 'main/index',
	'/index2' => 'site/index2',
	'/index3' => 'site/index3',
//	'poleznaya-informacziya' => 'site/info',
	/**
	 * Раздел Недвижимость
	 */
	'nedvizimost-ispania' => 'site/nedvizimost-ispania',
	'nedvizimost-ispania/<razdel1:[a-zA-Z0-9\-_\.]*>' => 'site/nedvizimost-ispania',
	'nedvizimost-ispania/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>' => 'site/nedvizimost-ispania',
	'nedvizimost-ispania/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_()\.]*>' => 'site/nedvizimost-ispania',

	/**
	 * Раздел Информация
	 */
	'poleznaya-informacziya' => 'site/poleznaya-informacziya',
	'poleznaya-informacziya/<razdel1:[a-zA-Z0-9\-_\.]*>' => 'site/poleznaya-informacziya',
	'poleznaya-informacziya/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>' => 'site/poleznaya-informacziya',
	'poleznaya-informacziya/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>' => 'site/poleznaya-informacziya',

	/**
	 * Раздел Яхты
	 */
	'arenda-yaxt' => 'site/arenda-yaxt',
	'arenda-yaxt/<razdel1:[a-zA-Z0-9\-_\.]*>' => 'site/arenda-yaxt',
	'arenda-yaxt/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>' => 'site/arenda-yaxt',
	'arenda-yaxt/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>' => 'site/arenda-yaxt',

	/**
	 * Раздел об Андалусии
	 */
	'andalucia/' => 'site/andalucia',
	'andalucia/<razdel1:[a-zA-Z0-9\-_]*>' => 'site/andalucia',
	'andalucia/<razdel1:[a-zA-Z0-9\-_]*>/<razdel2:[a-zA-Z0-9\-_]*>' => 'site/andalucia',
	'andalucia/<razdel1:[a-zA-Z0-9\-_]*>/<razdel2:[a-zA-Z0-9\-_]*>/<razdel3:[a-zA-Z0-9\-_]*>' => 'site/andalucia',

	/**
	 * Раздел Отдых
	 */
	'otdyh-v-ispanii' => 'site/otdyh-v-ispanii',
	'otdyh-v-ispanii/<razdel1:[a-zA-Z0-9\-_\.]*>' => 'site/otdyh-v-ispanii',
	'otdyh-v-ispanii/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>' => 'site/otdyh-v-ispanii',
	'otdyh-v-ispanii/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>' => 'site/otdyh-v-ispanii',

	/**
	 * Раздел Автомобили
	 */
	'arenda-avtomobilya-v-ispanii' => 'site/arenda-avtomobilya-v-ispanii',
	'arenda-avtomobilya-v-ispanii/<razdel1:[a-zA-Z0-9\-_\.]*>' => 'site/arenda-avtomobilya-v-ispanii',
	'arenda-avtomobilya-v-ispanii/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>' => 'site/arenda-avtomobilya-v-ispanii',
	'arenda-avtomobilya-v-ispanii/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>' => 'site/arenda-avtomobilya-v-ispanii',


	'akczii' => 'site/akczii',
	'akczii/<razdel1:[a-zA-Z0-9\-_\.]*>' => 'site/akczii',
	'akczii/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>' => 'site/akczii',
	'akczii/<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>' => 'site/akczii',


	'kontaktyi' => 'site/contact',
	'form-question' => 'site/form-question',
	'search-query-for-site' => 'site/search-query-for-site',
	'search-result' => 'site/search-result',
	'search-baza' => 'site/search-baza',
	'login' => 'site/login',
	'images-gallery' => 'site/images-gallery',
	'yachts-rental-window' => 'site/yachts-rental-window2',
	'yachts-rental-window2' => 'site/yachts-rental-window2',

	/**
	 * Страница для тестирования
	 */
	'test-page' => 'site/test-page',

	//Андалусия 
//	[
//		'class' => 'yii\web\GroupUrlRule',
//		'prefix' => 'andalucia',
//		'routePrefix' => 'andalucia',
//		'rules' => [
//			'/' => 'about',
//			'<razdel1:[a-zA-Z0-9\-_]*>' => 'about',
//			'<razdel1:[a-zA-Z0-9\-_]*>/<razdel2:[a-zA-Z0-9\-_]*>' => 'about',
//			'<razdel1:[a-zA-Z0-9\-_]*>/<razdel2:[a-zA-Z0-9\-_]*>/<razdel3:[a-zA-Z0-9\-_]*>' => 'about',
//		]
//	],
	//Автомобили 
//	[
//		'class' => 'yii\web\GroupUrlRule',
//		'prefix' => 'arenda-avtomobilya-v-ispanii',
//		'routePrefix' => 'automobile',
//		'rules' => [
//			'/' => 'index',
//			'<razdel1:[a-zA-Z0-9\-_\.]*>' => 'index',
//			'<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>' => 'index',
//			'<razdel1:[a-zA-Z0-9\-_\.]*>/<razdel2:[a-zA-Z0-9\-_\.]*>/<razdel3:[a-zA-Z0-9\-_\.]*>' => 'index',
////			'transfer' => 'transfer',
////			'transfer/<transfer:[a-zA-Z0-9\-_]*>' => 'transfer',
//
////			'arenda-avtomobilya-v-malage' => 'arenda-avtomobilya-v-malage',
////			'arenda-avtomobilya-v-malage/usloviya-kontrakta' => 'usloviya-kontrakta',
//
////			'procat-sportivnyix-avtomobilej-v-ispanii' => 'procat-sportivnyix-avtomobilej-v-ispanii',
////			'procat-sportivnyix-avtomobilej-v-ispanii/<procat:[a-zA-Z0-9\-_]*>' => 'procat',
//		]
//	],

	//Отдых в Испании 
//	[
//		'class' => 'yii\web\GroupUrlRule',
//		'prefix' => 'otdyh-v-ispanii',
//		'routePrefix' => 'otdyh',
//		'rules' => [
//			'/' => 'index',
//			'ekskursii-v-ispanii' => 'ekskursii',
//			'ekskursii-v-ispanii/<ekskursii:[a-zA-Z0-9\-_]*>' => 'ekskursii',
//			'ekskursii-v-ispanii/<ekskursii:[a-zA-Z0-9\-_]*>/<gorod:[a-zA-Z0-9\-_]*>' => 'gorod',
//			'aktivnyij-otdyh' => 'aktivnyij-otdyh',
//			'novogodnie-turyi' => 'novogodnie-turyi',
//			'aktivnyij-otdyh/<aktivnyij:[a-zA-Z0-9\-_]*>' => 'aktivnyij',
//			'aktivnyij-otdyh/<aktivnyij:[a-zA-Z0-9\-_]*>/<actotdyh:[a-zA-Z0-9\-_]*>' => 'actotdyh',
//			'otdyix-s-detmi-v-ispanii' => 'otdyix-s-detmi-v-ispanii',
//			'otdyix-na-kosta-del-sol' => 'otdyix-na-kosta-del-sol',
//			'otdyix-s-detmi-v-ispanii/<aktivnyij:[a-zA-Z0-9\-_]*>' => 'otdyix-s-detmi',
//			'svadba-v-ispanii' => 'svadba-v-ispanii',
//			'korporativ' => 'korporativ',
//			'flamenko' => 'flamenko',
//			'korporativ/<finka:[a-zA-Z0-9\-_]*>' => 'korporativ-finka',
//			'flamenko/<sakromonte:[a-zA-Z0-9\-_]*>' => 'flamenko-sakromonte',
//			'svadba-v-ispanii/<aktivnyij:[a-zA-Z0-9\-_]*>' => 'svadba',
//			'otdyix-s-detmi-v-ispanii/<aktivnyij:[a-zA-Z0-9\-_]*>/<actotdyh:[a-zA-Z0-9\-_]*>' => 'otdyix--v-ispanii',
//			'svadba-v-ispanii/<aktivnyij:[a-zA-Z0-9\-_]*>/<actotdyh:[a-zA-Z0-9\-_]*>' => 'svadba-v-ispaniii',
//		]
//	],


	//Админка 
	[
		'class' => 'yii\web\GroupUrlRule',
		'prefix' => 'admin',
		'routePrefix' => 'admin',
		'rules' => [
			'/' => 'content/index',
//			'<page:[a-zA-Z0-9\-_]*>' => 'pages/index',
			'<content:[a-zA-Z0-9\-_]*>' => 'content/index',
//			'<about:[a-zA-Z0-9\-_]*>/<about1:[a-zA-Z0-9\-_]*>' => 'about1',
//			'<about:[a-zA-Z0-9\-_]*>/<about1:[a-zA-Z0-9\-_]*>/<about2:[a-zA-Z0-9\-_]*>' => 'about2',
		]
	],



//				'about' => 'site/about',
//				'contacts' => 'site/contact',
//				'info' => 'site/info',
];