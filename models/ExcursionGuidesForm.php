<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ExcursionGuidesForm is the model behind the contact form.
 */
class ExcursionGuidesForm extends Model
{
	/**
	 * Имя
	 * @var string
	 */
	public $name;

	/**
	 * Email
	 * @var string
	 */
	public $email;

	/**
	 * Тема письма
	 * @var string
	 */
	public $subject;

	/**
	 * Дата
	 * @var string
	 */
	public $date;

	/**
	 * Количество гостей
	 * @var int
	 */
	public $countGuest;

	/**
	 * Сообщение или комментарий
	 * @var string
	 */
	public $body;

	/**
	 * recapchta 2
	 * @var himiklab\yii2\recaptcha\ReCaptchaValidator
	 */
	public $reCaptchav2;

	/**
	 * recapchta 2
	 * @var himiklab\yii2\recaptcha\ReCaptchaValidator
	 */
	public $reCaptchav3;


    /**
	 * Правила валидации
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['name', 'email', 'countGuest', 'date', 'body'], 'required', 'message' => 'Поле не заполнено'],
			[['reCaptchav2'], 'required', 'message' => 'Подтвердите, что Вы не робот'],
            ['email', 'email'],
			[['countGuest'], 'integer', 'min' => 1],
			[['date'], 'date', 'format' => 'php:d.m.Y'],
			[['subject'], 'safe'],
			[
				['reCaptchav2'],
				\himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
				'secret' => RECAPTCHA_SECRET_V2
			],
//			[
//				['reCaptchav3'], \himiklab\yii2\recaptcha\ReCaptchaValidator3::className(),
//				'secret' => '6LdVMOsUAAAAADBtfrLxPFzwpD9XKy_F37xq17cA',
//				'threshold' => 0.5,
//				'action' => 'yachts-rental-window2',
//			],
        ];
    }

    /**
	 * Заголовки полей формы
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
			'countGuest' => 'Количество человек',
			'date' => 'Дата',
            'body' => 'Сообщение или комментарий',
        ];
    }

    /**
	 * Отправка формы на почту
     *
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
			$body = "Имя: " . $this->name . "\nEmail: " . $this->email . "\nДата: " . $this->date . "\nКоличество гостей: " . $this->countGuest . "\nПожелания: " . $this->body;
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($body)
                ->send();

            return true;
        }

        return false;
    }
}
