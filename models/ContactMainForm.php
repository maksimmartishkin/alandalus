<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactMainForm extends Model
{
	/**
	 * Имя
	 * @var string
	 */
    public $name;

	/**
	 * Email
	 * @var string
	 */
    public $email;

	/**
	 * Тема письма
	 * @var string
	 */
    public $subject;

	/**
	 * Сообщение или комментарий
	 * @var string
	 */
    public $body;

	/**
	 * recapchta 2
	 * @var himiklab\yii2\recaptcha\ReCaptchaValidator
	 */
    public $reCaptchav2;

	/**
	 * recapchta 2
	 * @var himiklab\yii2\recaptcha\ReCaptchaValidator
	 */
    public $reCaptchav3;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['name', 'email', 'body'], 'required', 'message' => 'Поле не заполнено'],

			[['reCaptchav2'], 'required', 'message' => 'Подтвердите, что Вы не робот'],

            ['email', 'email'],

			[['subject'], 'safe'],

			[
				['reCaptchav2'],
				\himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
				'secret' => RECAPTCHA_SECRET_V2
			],

//			[
//				['reCaptchav3'], \himiklab\yii2\recaptcha\ReCaptchaValidator3::className(),
//				'secret' => '6LdVMOsUAAAAADBtfrLxPFzwpD9XKy_F37xq17cA',
//				'threshold' => 0.5,
//				'action' => 'yachts-rental-window2',
//			],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'body' => 'Сообщение',
            'verifyCode' => 'Проверочный код',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }

        return false;
    }
}
