<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SiteTmplvarContentvalues;

/**
 * SiteTmplvarContentvaluesSearch represents the model behind the search form of `app\models\SiteTmplvarContentvalues`.
 */
class SiteTmplvarContentvaluesSearch extends SiteTmplvarContentvalues
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tmplvarid', 'contentid'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteTmplvarContentvalues::find();

        // add conditions that should always apply here

		var_dump($params);die();
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return false;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tmplvarid' => $this->tmplvarid,
            'contentid' => $this->contentid,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

//        return $dataProvider;
    }
}
