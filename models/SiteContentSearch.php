<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SiteContent;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * SiteContentSearch represents the model behind the search form of `app\models\SiteContent`.
 */
class SiteContentSearch extends SiteContent
{
	public $text_request;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'published', 'pub_date', 'unpub_date', 'parent', 'isfolder', 'richtext', 'template', 'menuindex', 'searchable', 'cacheable', 'createdby', 'createdon', 'editedby', 'editedon', 'deleted', 'deletedon', 'deletedby', 'publishedon', 'publishedby', 'donthit', 'privateweb', 'privatemgr', 'content_dispo', 'hidemenu', 'content_type', 'uri_override', 'hide_children_in_tree', 'show_in_tree'], 'integer'],
            [['type', 'contentType', 'pagetitle', 'longtitle', 'description', 'alias', 'link_attributes', 'introtext', 'content', 'menutitle', 'class_key', 'context_key', 'uri', 'properties'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'editedby' => SORT_DESC,
				],
			]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'published' => $this->published,
            'pub_date' => $this->pub_date,
            'unpub_date' => $this->unpub_date,
            'parent' => $this->parent,
            'isfolder' => $this->isfolder,
            'richtext' => $this->richtext,
            'template' => $this->template,
            'menuindex' => $this->menuindex,
            'searchable' => $this->searchable,
            'cacheable' => $this->cacheable,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'editedby' => $this->editedby,
            'editedon' => $this->editedon,
            'deleted' => $this->deleted,
            'deletedon' => $this->deletedon,
            'deletedby' => $this->deletedby,
            'publishedon' => $this->publishedon,
            'publishedby' => $this->publishedby,
            'donthit' => $this->donthit,
            'privateweb' => $this->privateweb,
            'privatemgr' => $this->privatemgr,
            'content_dispo' => $this->content_dispo,
            'hidemenu' => $this->hidemenu,
            'content_type' => $this->content_type,
            'uri_override' => $this->uri_override,
            'hide_children_in_tree' => $this->hide_children_in_tree,
            'show_in_tree' => $this->show_in_tree,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'contentType', $this->contentType])
            ->andFilterWhere(['like', 'pagetitle', $this->pagetitle])
            ->andFilterWhere(['like', 'longtitle', $this->longtitle])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'link_attributes', $this->link_attributes])
            ->andFilterWhere(['like', 'introtext', $this->introtext])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'menutitle', $this->menutitle])
            ->andFilterWhere(['like', 'class_key', $this->class_key])
            ->andFilterWhere(['like', 'context_key', $this->context_key])
            ->andFilterWhere(['like', 'uri', $this->uri])
            ->andFilterWhere(['like', 'properties', $this->properties]);

        return $dataProvider;
    }

	/**
	 * Поиск по сайту
	 * @return bool|\yii\db\ActiveQuery
	 */
	public function searchForSite($params = false)
	{
		$query = SiteContent::find();

		if($params && !$this->text_request) {
			$this->text_request = $params['text_request'];
		} else {
		}
//		var_dump($this);die();
		if (!$this->validate()) {
			return false;
		}

		$query->select(['`pagetitle`', 'REPLACE(uri, \'.html\', \'\') AS `uri`', 'content']);
//		$query->where('MATCH (title,annotation,description,content) AGAINST (:text_request)', [':text_request' => $this->text_request]);
		$query->where(['like', 'pagetitle', $this->text_request])
			->orFilterWhere(['like', 'longtitle', $this->text_request])
			->orFilterWhere(['like', 'description', $this->text_request])
			->orFilterWhere(['like', 'introtext', $this->text_request])
			->orFilterWhere(['like', 'content', $this->text_request]);
//			->orFilterWhere(['like', 'menutitle', $this->menutitle]);
		$query->andFilterWhere(['`published`'=>1])->andFilterWhere(['`hidemenu`'=>0]);
//		var_dump($this, $query->createCommand()->getRawSql());die();

		return $query;
//		var_dump($query);die();
	}

	public function searchBaza($params)
	{
		$page = 1;
//		$get_category = $get_town = $get_type = $get_price = $get_beds_ot = $get_beds_do = $get_ref = $get_ref = [];
//		if (isset($params['beds_do']) and $params['beds_do'] != "") {
//			$get_beds_do = $params['beds_do'];
//		}
//		$id_baza_data = array_merge_recursive($get_category, $get_town, $get_type, $get_price, $get_beds_ot, $get_ref);

		$query = SiteContent::find()->select(['id', 'longtitle', 'REPLACE(uri, \'.html\', \'\') AS `uri`']);

		if (!empty($params['category']) and $params['category'] != "") {
			$get_category = SiteTmplvarContentvalues::getCategories($params['category']);
			$query->andWhere(['in', 'id', $get_category['contentid']]);
		}
		if (!empty($params['town']) and $params['town'] != "") {
			$get_town = SiteTmplvarContentvalues::getTowns($params['town']);
			$query->andWhere(['in', 'id', $get_town]);
		}
		if (!empty($params['type']) and $params['type'] != "") {
			$get_type = SiteTmplvarContentvalues::getTypes(strtolower($params['type']));
			$query->andWhere(['in', 'id', $get_type]);
		}
		if (!empty($params['price']) and $params['price'] != "") {
			$get_price = SiteTmplvarContentvalues::getPrices(strtolower($params['price']));
			$query->andWhere(['in', 'id', $get_price]);
		}
		if (!empty($params['beds_ot']) and $params['beds_ot'] != "") {
			$get_beds_ot = SiteTmplvarContentvalues::getBeds(strtolower($params['beds_ot']));
			$query->andWhere(['in', 'id', $get_beds_ot]);
		}
		if (!empty($params['ref']) and $params['ref'] != "") {
			$get_ref = SiteTmplvarContentvalues::getRefs(strtolower($params['ref']));
			$query->andWhere(['in', 'id', $get_ref]);
		}
		if (!empty($params['page']) and $params['page'] != "") {
			$page = $params['page'];
		}
		if (empty($parents) or $parents == "") {
			$parents = [5241,5242,5243,5244,5245,5246,5247,5248,5261,5262,5264,5265];
			$query->andWhere(['in', 'parent', $parents]);
		}
		$query->andWhere(['published' => 1]);
		$query->orderBy('publishedon');

		$countQuery = clone $query;
		$pages = new \yii\data\Pagination([
			'totalCount' => $countQuery->count(),
			'pageSize' => 10,
			'page' => $page - 1,
			'defaultPageSize' => 10,
			'pageParam' => 'page',
			'pageSizeParam' => false,
			'route' => parse_url(Url::to(), PHP_URL_PATH),
			'params' => array_merge($_GET, ['page' => $page]),
		]);
//		$pages->page = $page - 1;
//			$pages->pageSizeParam = false;
//		$pages->defaultPageSize = 10;
		$query_result = $query->orderBy('publishedon')
			->offset($pages->offset)
			->limit($pages->limit);
		$baza['pages'] = $pages;
		foreach ($query->all() as $key => $item) {
			$baza['data'][$key]['id'] = $item->id;
			$baza['data'][$key]['longtitle'] = $item->longtitle;
			$baza['data'][$key]['uri'] = $item->uri;
			$query = Yii::$app->db->createCommand('SELECT t.`name`, t.`description`, tc.`value`
				FROM `site_tmplvar_contentvalues` tc
				LEFT JOIN `site_tmplvars` t ON t.`id` IN (tc.`tmplvarid`)
				WHERE tc.`contentid`=:contentid', [':contentid' => $item->id]);
			foreach ($query->queryAll() as $keys => $value) {
//				var_dump($value);die();
				$baza['data'][$key]['value'][$value['name']] = $value;
//				$baza_ditto[$key]['value'][$value->name] = $value;
			}
		}

		return $baza;
		var_dump($baza);die();


//		if ($get_category == "sale" and !isset($get_ref)) {
//			$filter .= (empty($filter) ? "" : ",") . "price_freq_baza==sale";
//		} else if ($get_category == "rent" and !isset($get_ref)) {
//			$filter .= (empty($filter) ? "" : ",") . "price_freq_baza!=sale";
//		}
//
//
//		if (isset($get_town) and $get_town != "") {
//			$filter .= (empty($filter) ? "" : ",") . "town_baza==" . strip_tags($get_town);
//		}
//
//		if (isset($get_type) and $get_type != "") {
//			$filter .= (empty($filter) ? "" : ",") . "type_of_item_baza==" . strip_tags($get_type);
//		}
//
//		if (isset($get_beds_ot) and $get_beds_ot != "") {
//			$filter .= (empty($filter) ? "" : ",") . "beds_baza_do>=" . $get_beds_ot;
//		}
////if (isset($get_beds_ot) and $get_beds_ot != "") { $filter .= (empty($filter)?"":"|")."beds_baza_do,".$get_beds_ot.",4";}
//
//		if (isset($get_ref) and $get_ref != "") {
//			$filter .= (empty($filter) ? "" : ",") . "id_baza==" . strip_tags($get_ref);
//		}
////$filter.='|template,16,1';
//
//		if (isset($get_price) and $get_price != "") {
//
//			$get_price_array = explode("|", $get_price);
//			// print_r($get_price_array);
//
//			if (isset($get_price_array[0]) and $get_price_array[0] != 150001) {
//				$min = preg_replace("/[^0-9]/iu", "", $get_price_array[0]);
//				$filter .= (empty($filter) ? "" : ",") . "price_baza>=" . $min;
//
//			}
//			if (isset($get_price_array[1])) {
//				$max = preg_replace("/[^0-9]/iu", "", $get_price_array[1]);
//				$filter .= (empty($filter) ? "" : ",") . "price_baza<=" . $max;
//			}
//			if (isset($get_price_array[0]) and $get_price_array[0] == 150001) {
//				$min = preg_replace("/[^0-9]/iu", "", $get_price_array[0]);
//				$filter .= (empty($filter) ? "" : ",") . "price_baza<=" . $min;
//			}
//
//		}


		ini_set('pcre.backtrack_limit', '1600000');


		if (!empty($price_freq_baza)) $filter .= (empty($filter) ? "" : ",") . "price_freq_baza==" . strip_tags($price_freq_baza);

		$ar = array('getPage', array('element' => 'getResources', 'parents' => $parents, 'debug' => '1', 'tpl' => 'sale', 'limit' => '8', 'sortBy' => 'publishedon', 'showHidden' => '1', 'includeTVs' => '1', 'tvFilters' => $filter, 'processTVs' => '1', 'tvPrefix' => '', 'cache' => 0,));

		var_dump($filter, $ar);die();
	}
}
