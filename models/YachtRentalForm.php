<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class YachtRentalForm extends Model
{
	/**
	 * Имя
	 * @var string
	 */
	public $name;

	/**
	 * Email
	 * @var string
	 */
	public $email;

	/**
	 * Тема письма
	 * @var string
	 */
	public $subject = 'Yachts rental window2';

	/**
	 * Количество гостей
	 * @var int
	 */
	public $countGuest;

	/**
	 * Дата
	 * @var string
	 */
	public $date;

	/**
	 * Вид судна
	 * @var array
	 */
	public $typeVessel;

	/**
	 * Регион
	 * @var array
	 */
	public $region;

	/**
	 * Сообщение или комментарий
	 * @var string
	 */
	public $body;

	/**
	 * recaptcha 2
	 * @var \himiklab\yii2\recaptcha\ReCaptchaValidator
	 */
	public $reCaptcha;


	/**
	 * @return array the validation rules.
	 */
	public function rules ()
	{
		return [
			// name, email, subject and body are required
			[['name', 'email', 'countGuest', 'date', 'typeVessel', 'body', 'region'], 'required', 'message' => 'Поле не заполнено'],
			[['reCaptchav2'], 'required', 'message' => 'Подтвердите, что Вы не робот'],
			['email', 'email'],
			[['countGuest'], 'integer', 'min' => 1],
			[['date'], 'date', 'format' => 'php:d.m.Y'],
			[
				['reCaptcha'],
				\himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
				'secret' => RECAPTCHA_SECRET_V2
			],
//			[
//				['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator3::className(),
//				'secret' => '6LdVMOsUAAAAADBtfrLxPFzwpD9XKy_F37xq17cA',
//				'threshold' => 0.5,
//				'action' => 'yachts-rental-window2',
//			],
			[['subject'], 'safe'],
		];
	}

	/**
	 * Заголовки полей формы
	 * @return array customized attribute labels
	 */
	public function attributeLabels ()
	{
		return [
			'name' => 'Имя',
			'email' => 'E-mail',
			'countGuest' => 'Количество человек',
			'date' => 'Дата',
			'typeVessel' => 'Вид судна',
			'region' => 'Регион',
			'body' => 'Сообщение или комментарий',
			'verifyCode' => 'Проверочный код',
		];
	}

	/**
	 * Отправка формы на почту
	 *
	 * @param $email
	 *
	 * @return bool
	 */
	public function retail ($email)
	{
		if ($this->validate()) {
			$body = "Имя: " . $this->name . "\nEmail: " . $this->email . "\nДата: " . $this->date . "\nКоличество гостей: " . $this->countGuest . "\nВид судна: " . $this->getTypeVessel() . "\nРегион: " . $this->getRegion() .  "\nПожелания: " . $this->body;
			Yii::$app->mailer->compose()
				->setTo($email)
				->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
				->setReplyTo([$this->email => $this->name])
				->setSubject($this->subject)
				->setTextBody($body)
				->send();

			return true;
		}
		return false;
	}

	/**
	 * Определение названия виду судна
	 * @return string
	 */
	public function getTypeVessel()
	{
		switch ($this->typeVessel) {
			case 1:
				$typeVessel = 'Катер';
				break;
			case 2:
				$typeVessel = 'Моторное';
				break;
			case 3:
				$typeVessel = 'Парусное';
				break;
			case 4:
				$typeVessel = 'Катамаран';
				break;
			case 5:
				$typeVessel = 'Любое';
				break;
		}

		return $typeVessel;
	}

	/**
	 * Определение названия региона
	 * @return string
	 */
	public function getRegion()
	{
		switch ($this->region) {
			case 1:
				$region = 'Коста дель Соль';
				break;
			case 2:
				$region = 'Барселона';
				break;
		}

		return $region;
	}
}
