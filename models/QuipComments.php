<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quip_comments".
 *
 * @property int $id
 * @property string $thread
 * @property int $parent
 * @property string|null $rank
 * @property int $author
 * @property string $body
 * @property string|null $createdon
 * @property string|null $editedon
 * @property int $approved
 * @property string|null $approvedon
 * @property int $approvedby
 * @property string $name
 * @property string $email
 * @property string $website
 * @property string $ip
 * @property int $deleted
 * @property string|null $deletedon
 * @property int $deletedby
 * @property int $resource
 * @property string $idprefix
 * @property string|null $existing_params
 */
class QuipComments extends \yii\db\ActiveRecord
{
	/**
	 * Имя таблицы
	 * @return string
	 */
    public static function tableName()
    {
        return 'quip_comments';
    }

	/**
	 * Правила валидации
	 * @return array|array[]
	 */
    public function rules()
    {
        return [
            [['parent', 'author', 'approved', 'approvedby', 'deleted', 'deletedby', 'resource'], 'integer'],
            [['rank', 'body', 'existing_params'], 'string'],
            [['body'], 'required'],
            [['createdon', 'editedon', 'approvedon', 'deletedon'], 'safe'],
            [['thread', 'name', 'email', 'website', 'ip', 'idprefix'], 'string', 'max' => 255],
        ];
    }

	/**
	 * Заголовки
	 * @return array|string[]
	 */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'thread' => 'Thread',
            'parent' => 'Parent',
            'rank' => 'Rank',
            'author' => 'Author',
            'body' => 'Body',
            'createdon' => 'Createdon',
            'editedon' => 'Editedon',
            'approved' => 'Approved',
            'approvedon' => 'Approvedon',
            'approvedby' => 'Approvedby',
            'name' => 'Name',
            'email' => 'Email',
            'website' => 'Website',
            'ip' => 'Ip',
            'deleted' => 'Deleted',
            'deletedon' => 'Deletedon',
            'deletedby' => 'Deletedby',
            'resource' => 'Resource',
            'idprefix' => 'Idprefix',
            'existing_params' => 'Existing Params',
        ];
    }

	/**
	 * Получение отзывов
	 * @param false $limit лимит выборки
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
    public static function getGuestBook($limit = false)
	{
		$query = QuipComments::find()->select([
			'id', 'name', 'body'
		])->where([
			'approved' => 1,
			'deleted' => 0
		]);

		if ($limit) {
			$query->limit($limit);
		}

		$query->orderBy('id DESC');

		return $query->all();
	}
}
