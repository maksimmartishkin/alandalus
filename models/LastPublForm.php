<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

class LastPublForm extends Model
{
	/** @var string Email */
	public $email;

	/**
	 * Правила валидации
	 *
	 * @return array|\string[][]
	 */
	public function rules()
	{
		return [
			[
				['email'],
				'required',
				'message' => 'Поле не заполнено'
			],
			[
				'email',
				'email'
			],
		];
	}

	/**
	 * Заголовки
	 *
	 * @param string $attribute атрибут, для которого нужен заголовок
	 *
	 * @return string|string[]
	 */
	public function attributeLabels()
	{
		return [
			'email' => 'Ваш e-mail'
		];
	}

	/**
	 * Подписка на последние новости
	 *
	 * @param string $email email
	 *
	 * @return array|bool
	 */
	public function subscribe($email)
	{
		if ($this->validate()) {
			$token = md5(json_encode(['email' => $this->email]));
			$lists = SubscribeLists::findOne(['token' => $token]);
			if (empty($lists)) {
				$lists = new SubscribeLists();
			}

			$lists->email = $this->email;
			$lists->confirm = 0;
			$lists->token = $token;
			if (!$lists->save()) {
				return $lists->errors;
			}

			$tokenHash = base64_encode(json_encode(['email' => $this->email,
				'token' => $token]));
			$link = Url::base(true) . Url::to([
					'/subscribe-confirm',
					'token' => $tokenHash
				]);
			$mailer = Yii::$app->mailer->compose('subscribe-confirm', [
				'link' => $link
			])->setTo($this->email)->setFrom([
				Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']
			])->setReplyTo([$email]);
			$mailer->setSubject('Подписка на статьи с сайта Alandalus.ru');
			//			$mailer->setTextBody($body);
			$mailer->send();

			return true;
		}

		return false;

	}

}