<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $phone;
    public $body;
    public $verifyCode;
    public $reCaptchav2;
    public $reCaptchav3;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'reCaptchav2', 'body', 'phone'], 'required', 'message' => 'Поле не заполнено'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha'],
			[
				['reCaptchav2'],
				\himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
				'secret' => RECAPTCHA_SECRET_V2
			],
//			[
//				['reCaptchav3'], \himiklab\yii2\recaptcha\ReCaptchaValidator3::className(),
//				'secret' => '6LdVMOsUAAAAADBtfrLxPFzwpD9XKy_F37xq17cA',
//				'threshold' => 0.5,
//				'action' => 'yachts-rental-window2',
//			],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'body' => 'Сообщение',
            'verifyCode' => 'Проверочный код',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();
//			var_dump($this, $this->validate(), $email);die();

            return true;
        }
        return false;
    }
}
