<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "site_tmplvar_contentvalues".
 *
 * @property int $id
 * @property int $tmplvarid
 * @property int $contentid
 * @property string $value
 */
class SiteTmplvarContentvalues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_tmplvar_contentvalues';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tmplvarid', 'contentid'], 'integer'],
            [['value'], 'required'],
            [['value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tmplvarid' => 'Tmplvarid',
            'contentid' => 'Contentid',
            'value' => 'Value',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getContent()
	{
		return $this->hasOne(SiteContent::className(), ['id' => 'contentid']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTmplvars()
	{
		return $this->hasOne(SiteTmplvars::className(), ['id' => 'tmplvarid']);
	}

	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}


//	public static function searchForSite($params = false)
//	{
//		$query = SiteTmplvarContentvalues::find();
//
//		var_dump($params);die();
//		$this->load($params);
//
//		if (!$this->validate()) {
//			return false;
//		}
//
//		// grid filtering conditions
//		$query->andFilterWhere([
//			'id' => $this->id,
//			'published' => $this->published,
//			'pub_date' => $this->pub_date,
//			'unpub_date' => $this->unpub_date,
//			'parent' => $this->parent,
//			'isfolder' => $this->isfolder,
//			'richtext' => $this->richtext,
//			'template' => $this->template,
//			'menuindex' => $this->menuindex,
//			'searchable' => $this->searchable,
//			'cacheable' => $this->cacheable,
//			'createdby' => $this->createdby,
//			'createdon' => $this->createdon,
//			'editedby' => $this->editedby,
//			'editedon' => $this->editedon,
//			'deleted' => $this->deleted,
//			'deletedon' => $this->deletedon,
//			'deletedby' => $this->deletedby,
//			'publishedon' => $this->publishedon,
//			'publishedby' => $this->publishedby,
//			'donthit' => $this->donthit,
//			'privateweb' => $this->privateweb,
//			'privatemgr' => $this->privatemgr,
//			'content_dispo' => $this->content_dispo,
//			'hidemenu' => $this->hidemenu,
//			'content_type' => $this->content_type,
//			'uri_override' => $this->uri_override,
//			'hide_children_in_tree' => $this->hide_children_in_tree,
//			'show_in_tree' => $this->show_in_tree,
//		]);
//
//		$query->andFilterWhere(['like', 'type', $this->type])
//			->andFilterWhere(['like', 'contentType', $this->contentType])
//			->andFilterWhere(['like', 'pagetitle', $this->pagetitle])
//			->andFilterWhere(['like', 'longtitle', $this->longtitle])
//			->andFilterWhere(['like', 'description', $this->description])
//			->andFilterWhere(['like', 'alias', $this->alias])
//			->andFilterWhere(['like', 'link_attributes', $this->link_attributes])
//			->andFilterWhere(['like', 'introtext', $this->introtext])
//			->andFilterWhere(['like', 'content', $this->content])
//			->andFilterWhere(['like', 'menutitle', $this->menutitle])
//			->andFilterWhere(['like', 'class_key', $this->class_key])
//			->andFilterWhere(['like', 'context_key', $this->context_key])
//			->andFilterWhere(['like', 'uri', $this->uri])
//			->andFilterWhere(['like', 'properties', $this->properties]);
//	}

	/**
	 * База городов недвижимости
	 * @return string
	 */
	public static function getBazaSelect($town_data)
	{
		$tmplvarid = 163;
		$query = SiteTmplvarContentvalues::find()->select(['id', '`value`'])
			->where(['`tmplvarid`' => $tmplvarid]);
//		$query->groupBy('`value`')->all();
		$return_value = "<option value=\"\">Все</option>";
		foreach($query->all() as $town){
			$return_value .= '<option '. ($town_data == $town->id?'selected':'') .' value="'. $town->id .'">'.$town->value.'</option>';
		}
		return $return_value;
//		var_dump($query->createCommand()->getRawSql());die();
		return $query->all();
	}

	/**
	 * Получаем contentid из категорий
	 * @param $params
	 *
	 * @return array
	 */
	public static function getCategories($params)
	{
		if(empty($params)) {
			return [];
		}
		$query = SiteTmplvarContentvalues::find()
			->select(['tc.`contentid`', 't.name', 'tc.`value`'])
			->from('`site_tmplvar_contentvalues` tc')
			->leftJoin('`site_tmplvars` t', 'tc.`tmplvarid`=t.`id`')
			->where(['t.`name`' => 'price_freq_baza']);
		if ($params == "sale" and !isset($get_ref)) {
			$query->andWhere(['tc.`value`'=>'sale']);
//			$filter .= (empty($filter) ? "" : ",") . "price_freq_baza==sale";
		} else if ($params == "rent" and !isset($get_ref)) {
			$query->andWhere(['!=', 'tc.`value`', 'sale']);
//			$filter .= (empty($filter) ? "" : ",") . "price_freq_baza!=sale";
		}

		$categories = [];
		foreach ($query->all() as $key => $item) {
			$categories['contentid'][$key] = $item->contentid;
			$categories['value'][$key] = $item->value;
			$categories['name'][$key] = 'price_freq_baza';
		}

		return $categories;
	}

	/**
	 * Получаем contentid из городов
	 * @param $params
	 *
	 * @return |null
	 */
	public static function getTowns($params)
	{
		if(empty($params)) {
			return [];
		}
		$query = SiteTmplvarContentvalues::find()
			->select(['tc.`contentid`', 't.name', 'tc.`value`'])
			->from('`site_tmplvar_contentvalues` tc')
			->leftJoin('`site_tmplvars` t', 'tc.`tmplvarid`=t.`id`')
			->where(['t.`name`' => 'town_baza'])->andWhere(['tc.`id`'=>strip_tags($params)]);
//		var_dump($category);die();

//		$towns = [];
//		foreach ($query->all() as $key => $item) {
//			$towns['contentid'][$key] = $item->contentid;
//			$towns['value'][$key] = $item->value;
//			$towns['name'][$key] = 'town_baza';
//		}
//
//		return $towns;
		return $query->column();
	}

	/**
	 * Получаем contentid из цены
	 * @param $params
	 *
	 * @return array
	 */
	public static function getPrices($params)
	{
		if (isset($params) and $params != "") {

			$query = SiteTmplvarContentvalues::find()
				->select(['tc.`contentid`'])
				->from('`site_tmplvar_contentvalues` tc')
				->leftJoin('`site_tmplvars` t', 'tc.`tmplvarid`=t.`id`')
				->where(['t.`name`' => 'price_baza']);
			$get_price_array = explode("|", $params);
			if (isset($get_price_array[0]) and $get_price_array[0] != 150001) {
				$min = preg_replace("/[^0-9]/iu", "", $get_price_array[0]);
				$query->andWhere(['>=', 'tc.`value`', $min]);
			}
			if (isset($get_price_array[1])) {
				$max = preg_replace("/[^0-9]/iu", "", $get_price_array[1]);
				$query->andWhere(['<=', 'tc.`value`', $max]);
			}
			if (isset($get_price_array[0]) and $get_price_array[0] == 150001) {
				$min = preg_replace("/[^0-9]/iu", "", $get_price_array[0]);
				$query->andWhere(['<=', 'tc.`value`', $min]);
			}
			return $query->column();
		}
		return [];
	}

	/**
	 * Получаем contentid из спален
	 * @param $params
	 *
	 * @return array
	 */
	public static function getBeds($params)
	{
		if(empty($params)) {
			return [];
		}
		$query = SiteTmplvarContentvalues::find()
			->select(['tc.`contentid`'])
			->from('`site_tmplvar_contentvalues` tc')
			->leftJoin('`site_tmplvars` t', 'tc.`tmplvarid`=t.`id`')
			->where(['t.`name`' => 'beds_baza_do'])->andWhere(['>=', 'tc.`value`', strip_tags($params)]);

		return $query->column();
	}

	/**
	 * Получаем contentid из номера недвижимости
	 * @param $params
	 *
	 * @return array
	 */
	public static function getRefs($params)
	{
		if(empty($params)) {
			return [];
		}
		$query = SiteTmplvarContentvalues::find()
			->select(['tc.`contentid`'])
			->from('`site_tmplvar_contentvalues` tc')
			->leftJoin('`site_tmplvars` t', 'tc.`tmplvarid`=t.`id`')
			->where(['t.`name`' => 'id_baza'])->andWhere(['tc.`value`' => strip_tags($params)]);

		return $query->column();
	}

	/**
	 * Получаем contentid из типов недвижимости
	 * @param $params
	 *
	 * @return array
	 */
	public static function getTypes($params)
	{
		if(empty($params)) {
			return [];
		}
		$query = SiteTmplvarContentvalues::find()
			->select(['tc.`contentid`'])
			->from('`site_tmplvar_contentvalues` tc')
			->leftJoin('`site_tmplvars` t', 'tc.`tmplvarid`=t.`id`')
			->where(['t.`name`' => 'type_of_item_baza'])->andWhere(['tc.`value`' => strip_tags($params)]);

		return $query->column();
	}

	public static function getValuestImagesAlbum($content_id)
	{
//		$duration = 86400;
//		$contentId = $content_id;
//		$result = SiteTmplvarContentvalues::getDb()->cache(function ($db) {
			$query = SiteTmplvarContentvalues::find()->where(['contentid' => $content_id])->cache(86400);
			$result = [];
			foreach ($query->all() as $item) {
				$result[$item->tmplvars->name] = $item->value;
			}
			return $result;
//		}, $duration, );
//		return ArrayHelper::map($query->all(), 'name', 'value');
//		var_dump($result);die();
	}
}
