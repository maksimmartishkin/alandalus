<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pages;

/**
 * PagesSearch represents the model behind the search form of `app\models\Pages`.
 */
class PagesSearch extends Pages
{
	public $text_request;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'public'], 'integer'],
            [['title', 'description', 'link', 'content', 'create', 'update', 'text_request'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'update' => SORT_DESC,
//					'title' => SORT_ASC,
				]
			]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'create' => $this->create,
            'update' => $this->update,
            'public' => $this->public,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }

	/**
	 * Поиск по сайту
	 * @return bool|\yii\db\ActiveQuery
	 */
    public function searchForSite($params = false)
	{
		$query = Pages::find();

		if($params && !$this->text_request) {
			$this->text_request = $params['text_request'];
		} else {
		}
//		var_dump($this);die();
		if (!$this->validate()) {
			return false;
		}

		$query->select('title, link, content');
		$query->where('MATCH (title,annotation,description,content) AGAINST (:text_request)', [':text_request' => $this->text_request]);

//		var_dump($this, $query->createCommand()->getRawSql());die();

		return $query;
//		var_dump($query);die();
	}
}
