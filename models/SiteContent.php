<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "site_content".
 *
 * @property int         $id
 * @property string      $type
 * @property string      $contentType
 * @property string      $pagetitle
 * @property string      $longtitle
 * @property string      $description
 * @property string|null $alias
 * @property string      $link_attributes
 * @property int         $published
 * @property int         $pub_date
 * @property int         $unpub_date
 * @property int         $parent
 * @property int         $isfolder
 * @property string|null $introtext
 * @property string|null $content
 * @property int         $richtext
 * @property int         $template
 * @property int         $menuindex
 * @property int         $searchable
 * @property int         $cacheable
 * @property int         $createdby
 * @property int         $createdon
 * @property int         $editedby
 * @property int         $editedon
 * @property int         $deleted
 * @property int         $deletedon
 * @property int         $deletedby
 * @property int         $publishedon
 * @property int         $publishedby
 * @property string      $menutitle
 * @property int         $donthit
 * @property int         $privateweb
 * @property int         $privatemgr
 * @property int         $content_dispo
 * @property int         $hidemenu
 * @property string      $class_key
 * @property string      $context_key
 * @property int         $content_type
 * @property string|null $uri
 * @property int         $uri_override
 * @property int         $hide_children_in_tree
 * @property int         $show_in_tree
 * @property string|null $properties
 * @property int         $alias_visible
 */
class SiteContent extends \yii\db\ActiveRecord
{
	/** Время кэширования неделя */
	const DURATION_CACHE = 604800;

	/** Опубликовано */
	const PUBLISHED = 1;

	public $lastmod;

	/**
	 * Название таблицы
	 *
	 * @return string
	 */
	public static function tableName()
	{
		return 'site_content';
	}

	/**
	 * Боковое меню
	 *
	 * @param null $razdel1
	 * @param null $razdel2
	 * @param null $razdel3
	 * @param null $razdel4
	 *
	 * @return array|bool
	 */
	public static function getSideMenu($razdel1 = null, $razdel2 = null, $razdel3 = null, $razdel4 = null)
	{
		if ($razdel1 === null || empty($razdel1)) {
			return false;
		}

		$query = SiteContent::find()->select(
			[
				's2.`id`', 'IF(s2.`menutitle`=\'\', s2.`pagetitle`, s2.`menutitle`) menutitle', 's2.`isfolder`', 'REPLACE(s2.uri, \'.html\', \'\') uri'
			]
		)->from('`site_content` s')
			->leftJoin('`site_content` s2', 's2.`parent`=s.id')
			->where(
				[
					's.`alias`' => $razdel1
				]
			)->andWhere(
				[
					's2.`published`' => 1
				]
			)->andWhere(
				[
					's2.`hidemenu`' => 0
				]
			)->orderBy('s2.`menuindex`')->cache(self::DURATION_CACHE);

		$side_menu = [];
		if ($menus = $query->all()) {
			foreach ($menus as $key => $menu) {
				$side_menu[$key]['title'] = $menu->menutitle;
				$side_menu[$key]['link'] = Url::to([$menu->uri]);
				if ($menu->isfolder) {
					$query = SiteContent::find()->select(
						[
							'`id`', 'IF(`menutitle`=\'\', `pagetitle`, `menutitle`) menutitle', '`isfolder`', 'REPLACE(uri, \'.html\', \'\') uri'
						]
					)->where(
						[
							'`parent`' => $menu->id
						]
					)->andWhere(
						[
							'`published`' => 1
						]
					)->andWhere(
						[
							'`hidemenu`' => 0
						]
					)->orderBy('`menuindex`')->cache(self::DURATION_CACHE);

					if ($submenus = $query->all()) {
						foreach ($submenus as $key_sub => $submenu) {
							$side_menu[$key]['child'][$key_sub]['title'] = $submenu->menutitle;
							$side_menu[$key]['child'][$key_sub]['link'] = Url::to([$submenu->uri]);
							if ($submenu->isfolder) {
								$query = SiteContent::find()->select(
									[
										'`id`', 'IF(`menutitle`=\'\', `pagetitle`, `menutitle`) menutitle', '`isfolder`', 'REPLACE(uri, \'.html\', \'\') uri'
									]
								)->where(
									[
										'`parent`' => $submenu->id
									]
								)->andWhere(
									[
										'`published`' => 1
									]
								)->andWhere(
									[
										'`hidemenu`' => 0
									]
								)->orderBy('`menuindex`')->cache(self::DURATION_CACHE);

								if ($subsubmenus = $query->all()) {
									foreach ($subsubmenus as $key_sub_sub => $subsubmenu) {
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['title'] = $subsubmenu->menutitle;
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['link'] = Url::to([$subsubmenu->uri]);
									}
								}
							}
						}
					}
				}
			}
		}

		return $side_menu;
	}

	/**
	 * Меню для результата поиска
	 *
	 * @return array
	 */
	public static function getSideMenuSearchResult()
	{
		$query = SiteContent::find()->select(
			[
				'IF(`menutitle`=\'\', `pagetitle`, `menutitle`) AS `menutitle`', 'REPLACE(uri, \'.html\', \'\') AS `uri`'
			]
		)->where(
			[
				'`id`' => [4, 5, 6, 37, 5239, 5240]
			]
		)->orderBy('`menuindex`')->cache(self::DURATION_CACHE);

		$side_menu = [];
		if ($menus = $query->all()) {
			foreach ($menus as $key => $menu) {
				$side_menu[$key]['title'] = $menu->menutitle;
				$side_menu[$key]['link'] = Url::to(['/' . $menu->uri]);
			}
		}

		return $side_menu;
	}

	/**
	 * Меню в админке
	 *
	 * @return array
	 */
	public static function getSideMenuAdmin()
	{
		$query = SiteContent::find()->select(['`id`', '`pagetitle`', '`isfolder`', 'CONCAT(\'/admin/content/view?id=\', `id`, \'&parent=\', `parent`) uri', '`parent`'])
			->where(['`parent`' => 0])->andWhere(['not in', '`id`', [1, 5402, 312, 9, 142, 900, 870, 1041, 1172]])->orderBy('`parent`, `menuindex`')->cache(86400);
		$side_menu = [];
		if ($menus = $query->all()) {
			foreach ($menus as $key => $menu) {
				$side_menu[$key]['id'] = $menu->id;
				$side_menu[$key]['title'] = $menu->pagetitle;
				$side_menu[$key]['link'] = Url::to([$menu->uri]);
				$side_menu[$key]['parent'] = $menu->parent;
				if ($menu->isfolder) {
					$query = SiteContent::find()->select(['`id`', '`pagetitle`', '`isfolder`', 'CONCAT(\'/admin/content/view?id=\', `id`, \'&parent=\', `parent`) uri', '`parent`'])
						->where(['`parent`' => $menu->id])->andWhere(['`published`' => 1])->andWhere(['`hidemenu`' => 0])->orderBy('`menuindex`')->cache(86400);
					if ($submenus = $query->all()) {
						foreach ($submenus as $key_sub => $submenu) {
							$side_menu[$key]['child'][$key_sub]['id'] = $submenu->id;
							$side_menu[$key]['child'][$key_sub]['title'] = $submenu->pagetitle;
							$side_menu[$key]['child'][$key_sub]['link'] = Url::to([$submenu->uri]);
							$side_menu[$key]['child'][$key_sub]['parent'] = $submenu->parent;
							if ($submenu->isfolder) {
								$query = SiteContent::find()->select(['`id`', '`pagetitle`', '`isfolder`', 'CONCAT(\'/admin/content/view?id=\', `id`, \'&parent=\', `parent`) uri', '`parent`'])
									->where(['`parent`' => $submenu->id])->andWhere(['`published`' => 1])->andWhere(['`hidemenu`' => 0])->orderBy('`menuindex`')->cache(86400);
								if ($subsubmenus = $query->all()) {
									foreach ($subsubmenus as $key_sub_sub => $subsubmenu) {
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['id'] = $subsubmenu->id;
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['title'] = $subsubmenu->pagetitle;
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['link'] = Url::to([$subsubmenu->uri]);
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['parent'] = $subsubmenu->parent;
									}
								}
							}
						}
					}
				}
			}
		}

		return $side_menu;
	}

	/**
	 * Боковое меню, но вроде не используется
	 *
	 * @param null $razdel1
	 * @param null $razdel2
	 * @param null $razdel3
	 * @param null $razdel4
	 *
	 * @return array|bool
	 */
	public static function getSideMenuTree($razdel1 = null, $razdel2 = null, $razdel3 = null, $razdel4 = null)
	{
		if ($razdel1 === null || empty($razdel1)) {
			return false;
		}

		$query = SiteContent::find()->select(['s2.`id`', 'IF(s2.`menutitle`=\'\', s2.`pagetitle`, s2.`menutitle`) menutitle', 's2.`isfolder`', 'REPLACE(s2.uri, \'.html\', \'\') uri'])
			->from('`site_content` s')
			->leftJoin('`site_content` s2', 's2.`parent`=s.id')
			->where(['s.`alias`' => $razdel1])->andWhere(['s2.`published`' => 1])->andWhere(['s2.`hidemenu`' => 0])->orderBy('s2.`menuindex`')->cache(86400);
		$side_menu = [];
		if ($menus = $query->all()) {
			foreach ($menus as $key => $menu) {
				$side_menu[$key]['title'] = $menu->menutitle;
				$side_menu[$key]['link'] = Url::to([$menu->uri]);
				if ($menu->isfolder) {
					$query = SiteContent::find()->select(['`id`', 'IF(`menutitle`=\'\', `pagetitle`, `menutitle`) menutitle', '`isfolder`', 'REPLACE(uri, \'.html\', \'\') uri'])
						->where(['`parent`' => $menu->id])->andWhere(['`published`' => 1])->andWhere(['`hidemenu`' => 0])->orderBy('`menuindex`')->cache(86400);
					if ($submenus = $query->all()) {
						foreach ($submenus as $key_sub => $submenu) {
							$side_menu[$key]['child'][$key_sub]['title'] = $submenu->menutitle;
							$side_menu[$key]['child'][$key_sub]['link'] = Url::to([$submenu->uri]);
							if ($submenu->isfolder) {
								$query = SiteContent::find()->select(['`id`', 'IF(`menutitle`=\'\', `pagetitle`, `menutitle`) menutitle', '`isfolder`', 'REPLACE(uri, \'.html\', \'\') uri'])
									->where(['`parent`' => $submenu->id])->andWhere(['`published`' => 1])->andWhere(['`hidemenu`' => 0])->orderBy('`menuindex`')->cache(86400);
								if ($subsubmenus = $query->all()) {
									foreach ($subsubmenus as $key_sub_sub => $subsubmenu) {
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['title'] = $subsubmenu->menutitle;
										$side_menu[$key]['child'][$key_sub]['child'][$key_sub_sub]['link'] = Url::to([$subsubmenu->uri]);
									}
								}
							}
						}
					}
				}
			}
		}


		return $side_menu;
	}

	/**
	 * Получение данных по разделу Полезная информация
	 *
	 * @return array|\yii\db\ActiveRecord|null
	 */
	public static function getInfoSideMenu()
	{
		$query = SiteContent::find()->select(
			[
				'IF(`menutitle`=\'\', `pagetitle`, `menutitle`) AS `menutitle`', 'REPLACE(uri, \'.html\', \'\') AS `uri`'
			]
		)->where(
			[
				'`id`' => 8
			]
		)->cache(self::DURATION_CACHE);

		return $query->one();
	}

	/**
	 * База недвижимости
	 *
	 * @param $parent
	 *
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public static function getBazaDitto($parent, $page)
	{
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$query = SiteContent::find()->select(['id', 'longtitle', 'REPLACE(uri, \'.html\', \'\') uri'])->where(['in', 'parent', $parent])->andFilterWhere(['published' => 1]);
		$countQuery = clone $query;

		$pages = new \yii\data\Pagination([
			'totalCount' => $countQuery->count(),
			'pageSize' => 8,
			'pageParam' => 'page',
			'pageSizeParam' => false,
			'route' => parse_url(Url::to(), PHP_URL_PATH),
			'params' => ['page' => $page],
		]);
		$query->offset($pages->offset)
			->limit($pages->limit)
			->orderBy('publishedon DESC')->cache(86400);

		$baza_ditto['pages'] = $pages;
		foreach ($query->all() as $key => $item) {
			$baza_ditto['data'][$key]['id'] = $item->id;
			$baza_ditto['data'][$key]['longtitle'] = $item->longtitle;
			$baza_ditto['data'][$key]['uri'] = $item->uri;
			$query = Yii::$app->db->createCommand('SELECT t.`name`, t.`description`, tc.`value`
				FROM `site_tmplvar_contentvalues` tc
				LEFT JOIN `site_tmplvars` t ON t.`id` IN (tc.`tmplvarid`)
				WHERE tc.`contentid`=:contentid', [':contentid' => $item->id])->cache(86400);
			foreach ($query->queryAll() as $keys => $value) {
				$baza_ditto['data'][$key]['value'][$value['name']] = $value;
			}
		}

		return $baza_ditto;
	}

	/**
	 * Преобразование из id в URl
	 *
	 * @param int $id идентификатор урла
	 *
	 * @return false|string|null
	 */
	public static function findIdToUri($id)
	{
		return SiteContent::find()->select(
			[
				'REPLACE(uri, \'.html\', \'\') uri'
			]
		)->where(
			[
				'id' => $id
			]
		)->cache(self::DURATION_CACHE)
			->scalar();
	}

	/**
	 * Новости для раздела Недвижимость
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function findDittoNews()
	{
		$query = SiteContent::find()->select(
			[
				's2.`pagetitle`', 'REPLACE(s2.uri, \'.html\', \'\') `uri`', 's2.`introtext`'
			]
		)->from('`site_content` s')
			->leftJoin('`site_content` s2', 's2.`parent`=s.id')
			->where(
				[
					's.`alias`' => 'informacziya'
				]
			)->andWhere(
				[
					's2.`published`' => 1
				]
			)->andWhere(
				[
					's2.`hidemenu`' => 0
				]
			)->orderBy('s2.`menuindex`')->cache(self::DURATION_CACHE);

		return $query->all();
	}

	/**
	 * Список интересных фактов для раздела Об Андалусии
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function findFacts()
	{
		return SiteContent::find()->select(
			[
				's2.`pagetitle`', 'REPLACE(s2.uri, \'.html\', \'\') `uri`', 's2.`introtext`'
			]
		)->from('`site_content` s')
			->leftJoin('`site_content` s2', 's2.`parent`=s.id')
			->where(
				[
					's.`alias`' => 'faktyi'
				]
			)->andWhere(
				[
					's2.`published`' => 1
				]
			)->andWhere(
				[
					's2.`hidemenu`' => 0
				]
			)->orderBy('s2.`menuindex`')->cache(self::DURATION_CACHE)->all();
	}

	/**
	 * Список информации для раздела Полезная информация
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function findInfoList()
	{
		$info_list = SiteContent::find()->select(
			[
				's2.`pagetitle`', 'REPLACE(s2.uri, \'.html\', \'\') `uri`', 's2.`introtext`'
			]
		)->from('`site_content` s')
			->leftJoin('`site_content` s2', 's2.`parent`=s.id')
			->where(
				[
					's.`alias`' => 'poleznaya-informacziya'
				]
			)->andWhere(
				[
					's2.`published`' => static::PUBLISHED
				]
			)->andWhere(
				[
					's2.`hidemenu`' => 0
				]
			)->orderBy('s2.`menuindex`')->cache(self::DURATION_CACHE)->all();
		return $info_list;
	}

	/**
	 * Словарь (список) терминов для раздела Полезная информация
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function findSlovarTerminov()
	{
		return SiteContent::find()->select(
			[
				's2.`pagetitle`', 'REPLACE(s2.uri, \'.html\', \'\') `uri`', 's2.`introtext`'
			]
		)->from('`site_content` s')
			->leftJoin('`site_content` s2', 's2.`parent`=s.id')
			->where(
				[
					's.`alias`' => 'slovar-terminov'
				]
			)->andWhere(
				[
					's2.`published`' => static::PUBLISHED
				]
			)->andWhere(
				[
					's2.`hidemenu`' => 0
				]
			)->orderBy('s2.`menuindex`')->cache(self::DURATION_CACHE)->all();
	}

	/**
	 * Список акций
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function findAccii()
	{
		$query = SiteContent::find()->select(
			[
				's2.`id`', 's2.`pagetitle`', 'REPLACE(s2.uri, \'.html\', \'\') `uri`', 's2.`description`'
			]
		)->from('`site_content` s')
			->leftJoin('`site_content` s2', 's2.`parent`=s.id')
			->where(
				[
					's.`alias`' => 'akczii'
				]
			)->andWhere(
				[
					's2.`published`' => static::PUBLISHED
				]
			)->andWhere(
				[
					's2.`hidemenu`' => 0
				]
			)->orderBy('s2.`menuindex` DESC, s2.`alias`')->cache(self::DURATION_CACHE);

		return $query->all();
	}

	/**
	 * Возвращает список верхнего меню
	 *
	 * @return array
	 */
	public static function getMainMenu()
	{
		$query = SiteContent::find()->select(
			[
				'IF(`menutitle`=\'\', `pagetitle`, `menutitle`) AS `menutitle`', 'REPLACE(uri, \'.html\', \'\') AS `uri`'
			]
		)->where(
			[
				'`id`' => [4, 5, 6, 37, 5239, 5240]
			]
		)->orderBy('`menuindex`')->cache(self::DURATION_CACHE);

		$mainMenu = [];
		if ($menus = $query->all()) {
			foreach ($menus as $key => $menu) {
				$mainMenu[Url::to($menu->uri)] = $menu->menutitle;
			}
		}

		return $mainMenu;
	}

	/**
	 * О компании для вывода в футере
	 *
	 * @return false|string|null
	 */
	public static function getAbout()
	{
		$query = SiteContent::find()->select(
			[
				'`description`', '`content`'
			]
		)->where(
			[
				'`id`' => 5302
			]
		)->cache(self::DURATION_CACHE);
		return $query->one();
	}

	/**
	 * Правила полей
	 *
	 * @return array|array[]
	 */
	public function rules()
	{
		return [
			[['published', 'pub_date', 'unpub_date', 'parent', 'isfolder', 'richtext', 'template', 'menuindex', 'searchable', 'cacheable', 'createdby', 'createdon', 'editedby', 'editedon', 'deleted', 'deletedon', 'deletedby', 'publishedon', 'publishedby', 'donthit', 'privateweb', 'privatemgr', 'content_dispo', 'hidemenu', 'content_type', 'uri_override', 'hide_children_in_tree', 'show_in_tree'], 'integer'],
			[['introtext', 'content', 'uri', 'properties'], 'string'],
			[['type'], 'string', 'max' => 20],
			[['contentType'], 'string', 'max' => 50],
			[['pagetitle', 'longtitle', 'description', 'alias', 'link_attributes', 'menutitle'], 'string', 'max' => 255],
			[['class_key', 'context_key'], 'string', 'max' => 100],
		];
	}

	/**
	 * Заголовки полей
	 *
	 * @return array|string[]
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'type' => 'Type',
			'contentType' => 'Content Type',
			'pagetitle' => 'Заголовок',
			'longtitle' => 'Расширенный заголовок',
			'description' => 'Описание',
			'alias' => 'Псевдоним',
			'link_attributes' => 'Атрибуты ссылки',
			'published' => 'Опубликован',
			'pub_date' => 'Pub Date',
			'unpub_date' => 'Unpub Date',
			'parent' => 'Родительский ресурс',
			'isfolder' => 'Контейнер',
			'introtext' => 'Аннотация (введение)',
			'content' => 'Содержимое',
			'richtext' => 'Использовать HTML-редактор',
			'template' => 'Template',
			'menuindex' => 'Menuindex',
			'searchable' => 'Доступен для поиска',
			'cacheable' => 'Кэшируемый',
			'createdby' => 'Createdby',
			'createdon' => 'Createdon',
			'editedby' => 'Editedby',
			'editedon' => 'Editedon',
			'deleted' => 'Удалён',
			'deletedon' => 'Deletedon',
			'deletedby' => 'Deletedby',
			'publishedon' => 'Publishedon',
			'publishedby' => 'Publishedby',
			'menutitle' => 'Пункт меню',
			'donthit' => 'Donthit',
			'privateweb' => 'Privateweb',
			'privatemgr' => 'Privatemgr',
			'content_dispo' => 'Content Dispo',
			'hidemenu' => 'Скрыть из меню',
			'class_key' => 'Class Key',
			'context_key' => 'Context Key',
			'content_type' => 'Content Type',
			'uri' => 'Uri',
			'uri_override' => 'Uri Override',
			'hide_children_in_tree' => 'Hide Children In Tree',
			'show_in_tree' => 'Show In Tree',
			'properties' => 'Properties',
		];
	}

	/**
	 * Получение контента по ссылке и ее псевдониму
	 *
	 * @param bool $uri      полная ссылка от корня
	 * @param bool $alias    псевдоним ссылки
	 * @param bool $duration время кэширования
	 *
	 * @return array|\yii\db\ActiveRecord|null
	 */
	public static function findContentByUrlAndAlias($uri = false, $alias = false, $duration = false)
	{
		if (empty($uri) || empty($alias)) {
			return false;
		}

		$query = SiteContent::find()->select(
			[
				'id',
				'longtitle',
				'content',
				'description',
				'introtext'
			]
		)->where(
			[
				'alias' => $alias
			]
		)->andWhere(
			[
				'like', 'uri', $uri
			]
		)->andWhere(
			[
				'published' => static::PUBLISHED
			]
		);

		if ($duration) {
			$query->cache($duration);
		}

		return $query->one();
	}

	/**
	 * Получить контент для главной страницы
	 * @param false $duration необходимость кэширования
	 *
	 * @return array|\yii\db\ActiveRecord|null
	 */
	public static function findContentByMain($idMain = 1, $duration = false)
	{
		$query = SiteContent::find()->select(
			[
				'id',
				'longtitle',
				'content',
				'description',
				'introtext'
			]
		)->where(
			[
				'id' => $idMain
			]
		)->andWhere(
			[
				'published' => static::PUBLISHED
			]
		);

		if ($duration) {
			$query->cache($duration);
		}

		return $query->one();
	}

	/**
	 * Соединение с таблицей
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getTmplvarContentvalues()
	{
		return $this->hasMany(SiteTmplvarContentvalues::className(),
			[
				'contentid' => 'id'
			]
		);
	}
}
