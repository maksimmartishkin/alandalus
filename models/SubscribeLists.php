<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscribe_lists".
 *
 * @property int $id
 * @property string|null $email Email подписавшегося
 * @property int|null $confirm Подтверждение подписки
 * @property string|null $token Токен подписавшегося для подтверждения
 * @property string|null $create_dt Дата создания записи
 * @property string|null $update_dt Дата обновления записи
 */
class SubscribeLists extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscribe_lists';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['confirm'], 'integer'],
            [['create_dt', 'update_dt'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['token'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email подписавшегося',
            'confirm' => 'Подтверждение подписки',
            'token' => 'Токен подписавшегося для подтверждения',
            'create_dt' => 'Дата создания записи',
            'update_dt' => 'Дата обновления записи',
        ];
    }
}
