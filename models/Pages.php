<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $title Заголовок
 * @property string $description Описание
 * @property string $annotation Аннотация
 * @property string $title_menu Заголовок меню
 * @property string $link Ссылка на страницу
 * @property string $content Контент
 * @property string $create Дата создания
 * @property string $update Дата создания
 * @property string $public Опубликовано
 * @property string $hide_menu Скрыть из меню
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['description', 'annotation', 'content', 'public', 'hide_menu'], 'string'],
            [['create', 'update'], 'safe'],
            [['public'], 'integer'],
            [['title', 'title_menu'], 'string', 'max' => 150],
            [['link'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'annotation' => 'Аннотация',
            'title_menu' => 'Заголовок меню',
            'link' => 'Ссылка на страницу',
            'content' => 'Контент',
            'create' => 'Дата создания',
            'update' => 'Дата создания',
            'public' => 'Опубликовано',
            'hide_menu' => 'Скрыть из меню',
        ];
    }

	/**
	 *
	 * Боковое меню
	 * @param null $link
	 *
	 * @return array
	 */
    public static function getSideMenu($link = null, $sort = null, $razdel = null)
	{
//		$query = new \yii\db\Query();
//		$query->select(['title_menu', 'link']);
//		if(!empty($razdel)) {
//			if($razdel == 2) {
////				$sql .= ', SUBSTRING_INDEX(`link`,"/",1) main_link, SUBSTRING_INDEX(`link`,"/",2) link1';
//				$query->addSelect(['SUBSTRING_INDEX(`link`,"/",1) main_link', 'SUBSTRING_INDEX(`link`,"/",2) link1']);
//				//$query->
//			}
//		}
//		$query->from('pages')
//		->where(['LIKE', 'link', $link])->andFilterWhere(['<>', 'title_menu', ''])->andFilterWhere(['hide_menu'=>'0']);
//		//->limit(10);
////		$menu = $query->all();
////		echo '<pre>';print_r($menu);die();
//		//$query->select('`title_menu`, `link`, SUBSTRING_INDEX(`link`,"/",1) main_link, SUBSTRING_INDEX(`link`,"/",2) link1, SUBSTRING_INDEX(`link`,"/",3) link2, SUBSTRING_INDEX(`link`,"/",4) link3');
//////		$sql .= (['LIKE', 'link', $link])->andFilterWhere(['<>', 'title_menu', ''])->andFilterWhere(['hide_menu'=>'0']);
////		$sql .= " FROM pages WHERE link LIKE :link AND title_menu <> '' AND hide_menu = '0'";
////		//(['LIKE', 'link', $link])->andFilterWhere(['<>', 'title_menu', ''])->andFilterWhere(['hide_menu'=>'0']);
////		if(!empty($sort)) {
////			$sql .= " ORDER BY {$sort}";
////		}
////		$query = Pages::findBySql($sql, [':link' => $link.'%']);
//////		var_dump($query->createCommand()->getRawSql());die();
////		$menu = $query->all();
////		echo '<pre>';print_r($menu);die();
//
////		$query->select('title_menu, link');
//		if(!empty($razdel)) {
//			if($razdel == 2) {
//				$sql = 'SUBSTRING_INDEX(link,"/","1") main_link, SUBSTRING_INDEX(`link`,"/",2) link1';
////				$query->select('');
//				//$query->
//			}
//		}
////		$query = Pages::findBySql($sql);
////		//$query->select('`title_menu`, `link`, SUBSTRING_INDEX(`link`,"/",1) main_link, SUBSTRING_INDEX(`link`,"/",2) link1, SUBSTRING_INDEX(`link`,"/",3) link2, SUBSTRING_INDEX(`link`,"/",4) link3');
//////		$sql .= (['LIKE', 'link', $link])->andFilterWhere(['<>', 'title_menu', ''])->andFilterWhere(['hide_menu'=>'0']);
//		if(!empty($sort)) {
//			$query->orderBy($sort);
//		}
////		var_dump($query->createCommand()->getRawSql());die();
//		$menu = $query->all();
////		echo '<pre>';print_r($menu);die();
//		$side_menu = [];
//		$main_menu = [];
//		foreach ($menu as $key => $item) {
//			$ar_links = explode('/', $item['link1']);
//			rsort($ar_links);
//			if($item['main_link'] != $item['link']) {
////				$main_menu[$item['main_link']]['title'] = $item['title_menu'];
////				$main_menu[$item['main_link']]['link'] = $item['link'];
////			} else {
//				$main_menu[$item['main_link']][$ar_links[0]]['title'] = $item['title_menu'];
//				$main_menu[$item['main_link']][$ar_links[0]]['link'] = $item['link'];
//				/*			$main_menu[$item['main_link']]['link'] = [];*/
//			}
////			var_dump($key, $ar_links, $item);
//			//die();
//		}
////		sort($main_menu[$link]);
////		var_dump($main_menu[$link]);die();
//		return $main_menu;



		$query = Pages::find()->where(['LIKE', 'link', $link])->andFilterWhere(['not', 'title_menu', null])->andFilterWhere(['hide_menu'=>'0']);
		if(!empty($sort)) {
			$query->orderBy('title');
		}
//		$rawSql = $query->createCommand()->getRawSql();
//		var_dump($rawSql);die();
		$menu = $query->all();
		$side_menu = [];
		foreach ($menu as $key => $item) {
			$ar_links = explode('/', $item->link);
//			list($razdel1, $razdel2, $razdel3) = explode('/', $item->link);
			if(count($ar_links) == 1) {
				list($razdel1) = explode('/', $item->link);
				$side_menu[$razdel1]['link'] = $item->link;
				$side_menu[$razdel1]['title'] = $item->title_menu;
				$side_menu[$razdel1]['child'] = [];
			} elseif(count($ar_links) == 2) {
				list($razdel1, $razdel2) = explode('/', $item->link);
				$side_menu[$razdel1]['child'][$razdel2]['link'] = $item->link;
				$side_menu[$razdel1]['child'][$razdel2]['title'] = $item->title_menu;
				$side_menu[$razdel1]['child'][$razdel2]['child'] = [];
			} elseif(count($ar_links) == 3) {
				list($razdel1, $razdel2, $razdel3) = explode('/', $item->link);
				$side_menu[$razdel1]['child'][$razdel2]['child'][$razdel3]['link'] = $item->link;
				$side_menu[$razdel1]['child'][$razdel2]['child'][$razdel3]['title'] = $item->title_menu;
				$side_menu[$razdel1]['child'][$razdel2]['child'][$razdel3]['child'] = [];
			} elseif(count($ar_links) == 4) {
				list($razdel1, $razdel2, $razdel3, $razdel4) = explode('/', $item->link);
				$side_menu[$razdel1]['child'][$razdel2]['child'][$razdel3]['child'][$razdel4]['link'] = $item->link;
				$side_menu[$razdel1]['child'][$razdel2]['child'][$razdel3]['child'][$razdel4]['title'] = $item->title_menu;
			}
//			foreach ($ar_links as $key_link => $ar_link) {
//				var_dump($ar_links, count($ar_links), $side_menu);die();
//				$side_menu[$key][$ar_link][$key_link]['link'] = $item->link;
//				$side_menu[$key][$ar_link][$key_link]['title'] = $item->title_menu;
//			}
//			$side_menu[$ar_links[0]]['link'] = $item->link;
//			$side_menu[$ar_links[0]]['title'] = $item->title_menu;
//			echo '<pre>';print_r($side_menu);die();
//
//			if(!empty($ar_links[1])) {
//				$side_menu[$ar_links[0]][$ar_links[1]][$key]['link'] = $item->title_menu;
//			}
//			if(!empty($ar_links[2])) {
//				$side_menu[$ar_links[0]][$ar_links[1]][$ar_links[2]][$key]['link'] = $item->title_menu;
//			}
//			if(!empty($ar_links[3])) {
//				$side_menu[$ar_links[0]][$ar_links[1]][$ar_links[2]][$ar_links[3]][$key]['link'] = $item->title_menu;
//			}
			//else {
// else {
//					if(!empty($ar_links[1])) {
//						$side_menu[$key]['link'][$ar_links[0]][$ar_links[1]] = $item['title_menu'];
//					} else {
//						$side_menu[$key]['link'][$ar_links[0]] = $item['title_menu'];
//					}
//				}
//			}
//			$side_menu[$key]['title_menu'] = $item['title_menu'];
//			foreach (explode('/', $item['link']) as $link) {
//
//			}
//			var_dump($item->title_menu);//die();
		}
//		sort($side_menu);
//		echo '<pre>';print_r($side_menu);die();
//		var_dump($side_menu);die();
		return $side_menu;
	}

	/**
	 * Список интересных фактов для раздела Об Андалусии
	 * @return Pages[]|PagesSearch[]|array|\yii\db\ActiveRecord[]
	 */
	public function findFacts()
	{
		return Pages::find()->select('title, annotation, link')->where(['like', 'link', 'andalucia/faktyi/', 'public' => '1'])->orderBy('title')->all();
	}

	public function findInfoList()
	{
		$info = Pages::find()->select('title, annotation, link')->where(['like', 'link', 'poleznaya-informacziya/', 'public' => '1'])->orderBy('title')->all();
		$info_list = [];
		foreach ($info as $key => $item) {
			if(count(explode('/', $item->link)) > 2) {
				continue;
			}
			//var_dump(explode('/', $item->link));
			$info_list[$key] = $item;
		}
		return $info_list;
	}

	/**
	 * Словарь (список) терминов для раздела Полезная информация
	 * @return Pages[]|PagesSearch[]|array|\yii\db\ActiveRecord[]
	 */
	public function findSlovarTerminov()
	{
		return Pages::find()->select('title, annotation, link')->where(['like', 'link', 'poleznaya-informacziya/slovar-terminov/', 'public' => '1'])->orderBy('title')->all();
	}

	public function findAccii()
	{
		$query = Pages::find()->select('title_menu, description, link')->where(['like', 'link', 'akczii/', 'public' => '1'])->orderBy('title_menu DESC');
//		var_dump($query->createCommand()->getRawSql());die();
		return $query->all();
	}
}
