<?php

namespace app\models;

/**
 * This is the model class for table "last_publ_posts".
 *
 * @property int         $id
 * @property int|null    $content_id  site_content.id
 * @property int|null    $publishedon Дата из site_content.publishedon или site_content.editedon
 * @property string|null $create_dt
 * @property string|null $title       Заголовок
 * @property string|null $images      Картинка
 * @property string|null $content     Контент
 * @property string|null $uri         URL ссылки
 * @property string|null $update_dt
 */
class LastPublPosts extends \yii\db\ActiveRecord
{
	/**
	 * Имя таблицы
	 *
	 * @return string
	 */
	public static function tableName()
	{
		return 'last_publ_posts';
	}

	/**
	 * Получение последних публикаций
	 *
	 * @param false|int $limit  лимит выборки
	 * @param false|int $offset смещение
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function getLastPublPost($limit = 4, $offset = 0)
	{
		$query = LastPublPosts::find()->select([
			'title',
			'images',
			'content',
			'uri'
		])->orderBy('publishedon DESC');

		if ($offset) {
			$query->offset($offset);
		}

		if ($limit) {
			$query->limit($limit);
		}

		return $query->all();
	}

	public static function getLastPublPostByYesterday($limit = 10, $offset = 0)
	{
		$yesterday = (new \DateTime())->modify('- 1 day')->format('Y-m-d');
		$query = LastPublPosts::find()->select([
			'title',
			'images',
			'content',
			'uri'
		])->where([
			'FROM_UNIXTIME(publishedon, "%Y-%m-%d")' => $yesterday
		])->orderBy('publishedon DESC');

		if ($offset) {
			$query->offset($offset);
		}

		if ($limit) {
			$query->limit($limit);
		}

		return $query->all();
	}

	/**
	 * Обрезание строки до нужного размера
	 *
	 * @param string $string строки
	 * @param int    $length длина, на сколько нужно обрезать строку
	 *
	 * @return false|mixed|string
	 */
	public static function cutPublPost($string = '', $length = 100)
	{
		$strlen = mb_strlen($string);
		if ($strlen <= $length) {
			return $string;
		}

		/** обрезаем и работаем со всеми кодировками и указываем исходную кодировку */
		$string2 = mb_substr($string, 0, $length, 'UTF-8');
		/** определение позиции последнего пробела в урезанной строке. Именно по нему и разделяем слова */
		$position = mb_strrpos($string2, ' ', 0, 'UTF-8');
		/** если нет пробелов */
		if ($position === false) {
			/** ищем позицию первого в исходной строке */
			$position = mb_strpos($string, ' ', 0, 'UTF-8');
		}

		/** Обрезаем переменную по позиции */
		$string = mb_substr($string, 0, $position, 'UTF-8');
		return $string . '...';

	}

	/**
	 * Правила валидации
	 *
	 * @return array|array[]
	 */
	public function rules()
	{
		return [
			[
				[
					'content_id',
					'publishedon'
				],
				'integer'
			],
			[
				[
					'title',
					'images'
				],
				'string',
				'max' => 255
			],
			[
				[
					'create_dt',
					'update_dt',
					'content',
					'uri'
				],
				'safe'
			],
		];
	}

	/**
	 * Заголовки
	 *
	 * @return array|string[]
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'content_id' => 'Content ID',
			'publishedon' => 'Publishedon',
			'create_dt' => 'Create Dt',
			'update_dt' => 'Update Dt',
		];
	}
}
