<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\RentCarForm;
use app\models\ContactMainForm;
use app\models\YachtRentalForm;
use app\models\RentRealEstateForm;
use app\models\ExcursionGuidesForm;

/**
 * Виджет для выбора типа формы и модели для их обработки
 * Class FormMainWidget
 *
 * @package app\components
 */
class FormMainWidget extends Widget
{
	/**
	 * Тип формы
	 *
	 * @var string
	 */
	private static $typeForm;

	/**
	 * Имя кнопки
	 *
	 * @var string
	 */
	private static $nameButton;

	/**
	 * Заголовок формы
	 *
	 * @var string
	 */
	private static $titleForm;

	/**
	 * Тема формы для отправки
	 *
	 * @var string
	 */
	private static $formSubject;

	/**
	 * Инициализация виджета
	 *
	 * @param array $config
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function widget($config = [])
	{
		$output = [];
		if (isset($config['dataConfig'])) {
			parse_str(html_entity_decode($config['dataConfig']), $output);
			static::$typeForm = !empty($output['?type']) ? $output['?type'] : '1';
			static::$nameButton = !empty($output['name_button']) ? $output['name_button'] : 'Обратная связь';
			static::$titleForm = !empty($output['title_form']) ? $output['title_form'] : 'Обратная связь';
			static::$formSubject = !empty($config['formSubject']) ? $config['formSubject'] : '';
			unset($config['dataConfig']);
			unset($config['formSubject']);
		}

		return parent::widget($config);
	}

	/**
	 * Определение какую формы использовать
	 *
	 * @return string
	 */
	public function run()
	{
		switch (static::$typeForm) {
			case '2':
				/**
				 * Забронировать недвижимость
				 */ $model = new RentRealEstateForm();
				if ($model->load(Yii::$app->request->post()) && $model->rent(Yii::$app->params['adminEmail'])) {
					Yii::$app->session->setFlash('contactFormSubmitted');
				}

				return $this->render('form-2', ['model' => $model, 'nameButton' => static::$nameButton, 'titleForm' => static::$titleForm, 'formSubject' => static::$formSubject,]);
				break;
			case '3':
				/**
				 * Заказать экскурсию
				 */ $model = new ExcursionGuidesForm();
				if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
					Yii::$app->session->setFlash('contactFormSubmitted');
				}

				return $this->render('form-3', ['model' => $model, 'nameButton' => static::$nameButton, 'titleForm' => static::$titleForm, 'formSubject' => static::$formSubject,]);
				break;
			case '4':
				/** Аренда яхт */
				$model = new YachtRentalForm();
				if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
					Yii::$app->session->setFlash('contactFormSubmitted');
				}

				return $this->render('form-4', ['model' => $model, 'nameButton' => static::$nameButton, 'titleForm' => static::$titleForm, 'formSubject' => static::$formSubject,]);
				break;
			case '5':
				/** Аренда автомобиля */
				$model = new RentCarForm();
				if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
					Yii::$app->session->setFlash('contactFormSubmitted');
				}

				return $this->render('form-5', ['model' => $model, 'nameButton' => static::$nameButton, 'titleForm' => static::$titleForm, 'formSubject' => static::$formSubject,]);
				break;
			case '1':
				/** Обратная связь */
				//				$model = new ContactMainForm();
				//				if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
				//					Yii::$app->session->setFlash('contactFormSubmitted');
				//				}
				//
				//				return $this->render('form-1', [
				//					'model' => $model,
				//					'nameButton' => static::$nameButton,
				//					'titleForm' => static::$titleForm,
				//					'formSubject' => static::$formSubject,
				//				]);
				//				break;
			default:
				/**
				 * Обратная связь
				 */ $model = new ContactMainForm();
				if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
					Yii::$app->session->setFlash('contactFormSubmitted');
				}

				return $this->render('form-1', ['model' => $model, 'nameButton' => static::$nameButton, 'titleForm' => static::$titleForm, 'formSubject' => static::$formSubject,]);
				break;
		}

	}
}