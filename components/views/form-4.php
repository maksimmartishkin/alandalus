<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

	<div class="modal-forms-button button-fixed">
		<div class="" data-toggle="modal" data-target="#formsYachtRetailModal">
			<button type="button" class="button button-primary button-primary-custom">
				<?= !empty($nameButton) ? trim($nameButton) : ''; ?>
			</button>
		</div>
	</div>

	<div class="modal fade" id="formsYachtRetailModal" tabindex="-1" role="dialog"
		 aria-labelledby="formsYachtRetailModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title text-center"
						id="formsYachtRetailModalLabel"><?= !empty($titleForm) ? $titleForm : ''; ?></h2>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php $form = ActiveForm::begin([
						'id' => 'yacht-rental-form',
						'enableAjaxValidation' => false,
						'enableClientValidation' => true,
					]); ?>
					<div class="row">
						<div class="col-sm-12">
							<?= $form->field($model, 'name', [
								'template' => '
									<div class="form-wrap form-wrap-xs">
										{label}
										{input}
										<span class="form-validation">{error}</span>
									</div>
								'
							])->textInput(
								[
									'class' => 'form-input form-input-custom form-control'
								]
							)->label($model->attributeLabels()['name'],
								[
									'class' => 'form-label'
								]
							); ?>
						</div>
						<div class="col-sm-12">
							<?= $form->field($model, 'email', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->input('email',
								[
									'class' => 'form-input form-input-custom form-control',
								]
							)->label($model->attributeLabels()['email'],
								[
									'class' => 'form-label'
								]
							); ?>
						</div>
						<?= $form->field($model, 'subject', [
						])->hiddenInput(
							[
								'value' => !empty($formSubject) ? $formSubject : $this->title,
							]
						)->label(false); ?>

						<div class="col-sm-6">
							<?= $form->field($model, 'countGuest', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->input('number',
								[
									'class' => 'form-input form-input-custom form-control',
									'min' => 1
								]
							)->label($model->attributeLabels()['countGuest'],
								[
									'class' => 'form-label'
								]
							); ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'date', [
								'template' => '
						<div class="form-wrap form-wrap-xs input-group js-datetimepicker-form">
							{label}
							{input}
							<span class="input-group-addon date-calendar-custom js-date-calendar-custom">
								<i class="fa fa-calendar text-black" aria-hidden="true"></i>
							</span>							
							<span class="form-validation">{error}</span>
						</div>
					'
							])->input('text',
								[
									'class' => 'form-input form-input-custom form-control ',
								]
							)->label($model->attributeLabels()['date'],
								[
									'class' => 'form-label'
								]
							); ?>
						</div>

						<div class="col-sm-12">
							<?= $form->field($model, 'typeVessel', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->dropDownList(
								[
									1 => 'Катер',
									2 => 'Моторное',
									3 => 'Парусное',
									4 => 'Катамаран',
									5 => 'Любое',
								],
								[
									'prompt' => 'Вид судна',
									'class' => 'form-input form-input-custom form-control',
								]
							)->label(false); ?>
						</div>
						<div class="col-sm-12">
							<?= $form->field($model, 'region', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->dropDownList(
								[
									1 => 'Коста дель Соль',
									2 => 'Барселона',
								],
								[
									'prompt' => 'Регион',
									'class' => 'form-input form-input-custom form-control',
								]
							)->label(false); ?>
						</div>
						<div class="col-sm-12">
							<?= $form->field($model, 'body', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->textarea(
								[
									'rows' => 6,
									'class' => 'form-input form-input-custom form-control',
									'style' => 'height:120px;',
								]
							)->label($model->attributeLabels()['body'],
								[
									'class' => 'form-label'
								]
							); ?>

						</div>
						<div class="col-sm-7 text-center">
							<?= $form->field($model, 'reCaptchav2')->widget(
								\himiklab\yii2\recaptcha\ReCaptcha::className(),
								[
									'siteKey' => RECAPTCHA_SITE_KEY_V2
								]
							)->label(false); ?>

						</div>
						<div class="col-sm-5 text-center">
							<div class="form-button form-button-block-custom">
								<?= Html::submitButton('Отправить',
									[
										'class' => 'button button-width-110 button-primary button-primary-custom js-form-button-request-yacht-rental',
										'name' => 'contact-button'
									]
								) ?>
							</div>
						</div>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
<?php
$urlRequest = Url::to(['/yachts-rental-window2']);
$this->registerJs(<<<JS
    $('#yacht-rental-form').on('beforeSubmit', function(event) { 
		var formData = $(this).serialize();
        $.ajax({
            url: '$urlRequest',
            type: "post",
            data: formData,
            beforeSend: function() {
				$('.js-form-button-request-yacht-rental').html('<i class="fa fa-spinner fa-2x fa-spin fa-fw" aria-hidden="true"></i>').attr('disabled', true);
            },
            success: function (data) {
                if (data.success) {
                	$('#formsYachtRetailModal').find('.modal-body').html(data.success).addClass('text-success text-center');
                }
            }
        });
    }).on('submit', function(e){
		e.preventDefault();
	});
JS
);
?>