<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

	<div class="modal-forms-button button-fixed">
		<div class="" data-toggle="modal" data-target="#formsMainModal">
			<button type="button" class="button button-primary button-primary-custom">
				<?= !empty($nameButton) ? trim($nameButton) : ''; ?>
			</button>
		</div>
	</div>

	<div class="modal fade" id="formsMainModal" tabindex="-1" role="dialog" aria-labelledby="formsMainModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title text-center"
						id="formsMainModalLabel"><?= !empty($titleForm) ? $titleForm : ''; ?></h2>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php $form = ActiveForm::begin([
						'id' => 'contact-form',
						'enableAjaxValidation' => false,
						'enableClientValidation' => true,
					]); ?>
					<div class="row">
						<div class="col-sm-12">
							<?= $form->field($model, 'name', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->textInput(
								[
									'class' => 'form-input form-input-custom form-control',
								]
							)->label($model->attributeLabels()['name'],
								[
									'class' => 'form-label'
								]
							); ?>
						</div>
						<div class="col-sm-12">
							<?= $form->field($model, 'email', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->input('email',
								[
									'class' => 'form-input form-input-custom form-control',
								]
							)->label($model->attributeLabels()['email'],
								[
									'class' => 'form-label'
								]
							); ?>
						</div>
						<?= $form->field($model, 'subject', [
						])->hiddenInput(
							[
								'value' => !empty($formSubject) ? $formSubject : $this->title,
							]
						)->label(false); ?>

						<div class="col-sm-12">
							<?= $form->field($model, 'body', [
								'template' => '
						<div class="form-wrap form-wrap-xs">
							{label}
							{input}
							<span class="form-validation">{error}</span>
						</div>
					'
							])->textarea(
								[
									'rows' => 6,
									'class' => 'form-input form-input-custom form-control',
									'style' => 'height:120px;',
								]
							)->label($model->attributeLabels()['body'],
								[
									'class' => 'form-label'
								]
							); ?>

						</div>
						<div class="col-sm-7 text-center">
							<?= $form->field($model, 'reCaptchav2')->widget(
								\himiklab\yii2\recaptcha\ReCaptcha::className(),
								[
									'siteKey' => RECAPTCHA_SITE_KEY_V2
								]
							)->label(false); ?>

						</div>
						<div class="col-sm-5 text-center">
							<div class="form-button form-button-block-custom">
								<?= Html::submitButton('Отправить',
									[
										'class' => 'button button-width-110 button-primary button-primary-custom js-form-button-request-main',
										'name' => 'contact-button'
									]
								) ?>
							</div>
						</div>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
<?php
$urlRequest = Url::to(['/form-main-question']);
$this->registerJs(<<<JS
    $('#contact-form').on('beforeSubmit', function(event) { 
		var formData = $(this).serialize();
        $.ajax({
            url: '$urlRequest',
            type: "post",
            data: formData,
            beforeSend: function() {
				$('.js-form-button-request-main').html('<i class="fa fa-spinner fa-2x fa-spin fa-fw" aria-hidden="true"></i>').attr('disabled', true);
            },
            success: function (data) {
                if (data.success) {
                	$('#formsMainModal').find('.modal-body').html(data.success).addClass('text-success text-center');
                }
            }
        });
    }).on('submit', function(e){
		e.preventDefault();
	});
JS
);
?>